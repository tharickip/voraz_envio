//
//  LoginExtension.swift
//  agroshowiOS
//
//  Created by Lucas de Brito Reis on 03/07/17.
//  Copyright © 2017 Minerva Aplicativos LTDA ME. All rights reserved.
//
import UIKit
import SnapKit
import Alamofire
import SkyFloatingLabelTextField
import SwiftyJSON
import LTMorphingLabel
import CoreData
import SwiftOverlays
import SCLAlertView
import NotificationBannerSwift

extension Login {
    
    //Botao Entre - login e conexão com a api da Agroshow
    func fazerLogin() {
        
        self.botaoEntre.isEnabled = false
        self.estaEmpilhando = false
        self.numeroPaginas = 0
        self.listaPedidos.removeAll()
        SwiftOverlays.removeAllBlockingOverlays()
        self.clearContext()
        self.clearCampanhas(entityName: self.name.campanhaVendas)
        self.clearComparaCampanhas()
        self.clearCampanhaSelecionada()
        self.listaCampanhas.removeAll()

        
        
        
        guard let email = loginTextField.text, let senha = senhaTextField.text else { return }
        
        //self.statusDownload.text  = "Fazendo conexao com o servidor"
        //iniciaAnimacaoDeEspera()
        SwiftOverlays.removeAllBlockingOverlays()
        let text = "Conectando..."
        SwiftOverlays.showBlockingWaitOverlayWithText(text)
        
        
        Alamofire.request(self.urlBase + "login?email=\(email)&senha=\(senha)", method: .post)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    self.botaoEntre.isEnabled = true
                    let json = JSON(value)
                    
                    _ = self.atribuilogin(json)
                    
                    let dadosLogin = self.lerBancoDeDados(self.name.dadosLogin) as! [DadosLogin]
                    
                    if dadosLogin.count > 0 {
                        if let authtoken = dadosLogin[0].authToken {
                            //self.baixarPedidos(authToken: authtoken)
                            self.baixarPedidosPaginacao(authToken: authtoken)
                        }
                    }
                case .failure(let error):
                    self.desempilhaErros(erro: error, response: response){
                        self.botaoEntre.isEnabled = true
                    }
                    
                }
        }
    }
    
    func baixarPedidosPaginacao(authToken: String) {
        
        let headers = ["X-AUTH-TOKEN": "\(authToken)"]
        SwiftOverlays.removeAllBlockingOverlays()
        let text = "Baixando lista de pedidos"
        SwiftOverlays.showBlockingWaitOverlayWithText(text)
        
        Alamofire.request(self.urlBase+"pedidos", method: .get, headers: headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success(let value):

                    let json = JSON(value)
                    let totalPages = json["meta"]["totalPages"].intValue
                    for pagina in 1...totalPages {
                        self.paginacao(authToken: authToken, pagina: pagina, totalPages: totalPages, headers: headers)
                    }
                    
                case .failure(let erro):
                    
                    self.desempilhaErros(erro: erro, response: response){}
                }
        }
    }
    
    
    
    
    
    override func paginacao(authToken: String, pagina: Int, totalPages: Int, headers: Dictionary<String, String>) {
        //self.statusDownload.text = "Listando página \(self.numeroPaginas + 1)/\(totalPages)"
        
        
        
        Alamofire.request(self.urlBase+"pedidos?token=\(authToken)&p=\(pagina)", method: .get)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    
                    let json = JSON(value)
                    for (_, resultado) in json["pedidos"] {
                        
                        let id = resultado["id"].stringValue
                        self.listaPedidos.append(id)
                    }
                    
                    self.numeroPaginas = self.numeroPaginas + 1
                    
                    SwiftOverlays.removeAllBlockingOverlays()
                    let text = "Listando página \(self.numeroPaginas + 1)/\(totalPages)"
                    SwiftOverlays.showBlockingWaitOverlayWithText(text)
                    
                    if self.numeroPaginas > totalPages {
                        self.numeroPaginas = 0
                        SwiftOverlays.removeAllBlockingOverlays()
                        self.baixarPedidosPaginacao(authToken: authToken)
                    } else {
                        if self.numeroPaginas == totalPages {
                            self.removeAllOverlays()
                            SwiftOverlays.removeAllBlockingOverlays()
                            self.percorrerJsonPedidos(authToken: authToken, headers: headers)
                        }
                    }
                    
                case .failure(let erro):
                    
                    self.desempilhaErros(erro: erro, response: response){
                        self.numeroPaginas = totalPages + 1
                    }
                }
        }
    }
    
    
    
    
    
    func percorrerJsonPedidos(authToken: String, headers: Dictionary<String, String>) {
        
        //self.statusDownload.text = "Baixando detalhes dos pedidos"
        SwiftOverlays.removeAllBlockingOverlays()
        let text = "Baixando detalhes dos pedidos"
        SwiftOverlays.showBlockingWaitOverlayWithText(text)
        
        if self.listaPedidos.isEmpty {
            //SwiftOverlays.removeAllBlockingOverlays()
            self.baixarCampanhasOffline(authToken: authToken)
        } else {
            
            for id in self.listaPedidos {
                
                //let id = resultado["id"].stringValue
                
                self.getPedido(self.urlBase, id: id, authToken: authToken, headers: headers, {_ in
                    
                    //let pedidos = self.lerBancoDeDados(self.name.pedidos)
                    let pedidos = self.lerPedidos()
                    
                    
                    //self.statusDownload.text = "Baixando pedidos \(pedidos.count)/\(self.listaPedidos.count)"
                    
                    SwiftOverlays.removeAllBlockingOverlays()
                    let text = "Baixando pedidos \(pedidos.count)/\(self.listaPedidos.count)"
                    SwiftOverlays.showBlockingWaitOverlayWithText(text)
                    DatabaseController.saveContext()
                    
                    if pedidos.count > self.listaPedidos.count {
                        SwiftOverlays.removeAllBlockingOverlays()
                        let text = "Limpando banco de dados"
                        SwiftOverlays.showBlockingWaitOverlayWithText(text)
                        DatabaseController.saveContext()
                        self.clearContext()
                        self.fazerLogin()
                    }
                    
                    
                    if pedidos.count == self.listaPedidos.count {
                        
                        //                    self.numeroPaginas = 0
                        //                    self.listaPedidos.removeAll()
                        
                        //self.encerraAnimacaoDeEspera()
                        SwiftOverlays.removeAllBlockingOverlays()
                        let text = "Baixando pedidos \(pedidos.count)/\(self.listaPedidos.count)"
                        SwiftOverlays.showBlockingWaitOverlayWithText(text)
                        DatabaseController.saveContext()
                        
                        self.baixarCampanhasOffline(authToken: authToken)
                        
                        
                    }
                })
            }
        }
    }
    
    
    
    override func baixarComparaCampanhas(_ urlBase:String, authToken token: String, headers: Dictionary<String, String>, _ completionHandler: @escaping () -> Void) {
        
        let text = "Baixando Comparação entre Campanhas"
        SwiftOverlays.removeAllBlockingOverlays()
        self.removeAllOverlays()
        SwiftOverlays.showBlockingWaitOverlayWithText(text)
        
        Alamofire.request(urlBase+"dashboard/widgets/campanhas-evolucao", method: .get, headers: headers)
            .validate()
            .responseJSON { (response) in
                switch response.result {
                case .success(let valor):
                    
                    completionHandler()
                    
                    let json = JSON(valor)
                    
                    self.clearComparaCampanhas()
                    for (_, resultado) in json["campanhas"] {
                        self.atribuiComparaCampanhas(resultado)
                    }
                    
                    //Aqui vai o presentViewController
                    let feed = Feed()
                    self.present(UINavigationController(rootViewController: feed), animated: false, completion: {
                        self.removeAllOverlays()
                        SwiftOverlays.removeAllBlockingOverlays()
                        self.loginTextField.text = ""
                        self.senhaTextField.text = ""
                    })
                    
                    
                case .failure(let erro):
                    self.desempilhaErros(erro: erro, response: response){}
                }
        }
    }
    
    
}
