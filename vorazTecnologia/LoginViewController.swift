//
//  LoginViewController.swift
//  agroshow
//
//  Created by Lucas de Brito Reis on 17/05/17.
//  Copyright © 2017 MinervaAplicativos. All rights reserved.
//

import UIKit
import SnapKit
import Alamofire
import FormToolbar
import SkyFloatingLabelTextField
import SwiftyJSON
//import LTMorphingLabel
import CoreData
import SwiftOverlays
import SCLAlertView

class Login: BaseViewController, UITextFieldDelegate, UIScrollViewDelegate {
    
    // Inicializacao das variaveis
    let scrollBox: UIScrollView = {
        let view = UIScrollView()
        view.backgroundColor = .white
        return view
    }()
    
    let statusBar: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(r: 128, g: 30, b: 72)
        return view
    }()
    
    let logo: UIImageView = {
        let imageview = UIImageView()
        imageview.image = UIImage(named: "Launch")
        imageview.contentMode = .scaleAspectFit
        imageview.translatesAutoresizingMaskIntoConstraints = false
        return imageview
    }()
    
    let loginTextField: SkyFloatingLabelTextField = {
        let textField = SkyFloatingLabelTextField()
        //textField.text = "gerente001@agroshow.biz"
        textField.title = "E-mail"
        textField.selectedLineColor = UIColor(r: 128, g: 30, b: 72)
        textField.selectedTitleColor = UIColor(r: 128, g: 30, b: 72)
        textField.placeholder = "E-mail"
        textField.font = UIFont.systemFont(ofSize: 16)
        //textField.keyboardAppearance =  UIKeyboardAppearance.default
        textField.autocapitalizationType = .none
        //textField.becomeFirstResponder()
        textField.returnKeyType = UIReturnKeyType.done
        textField.autocorrectionType = UITextAutocorrectionType.no
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.addTarget(self, action: #selector(mostrarBotaoEntre), for: UIControlEvents.editingChanged)
        return textField
    }()
    
    let senhaTextField: SkyFloatingLabelTextField = {
        let textField = SkyFloatingLabelTextField()
        //textField.text = "2bolachaMaria"
        textField.title = "Senha"
        textField.isSecureTextEntry = true
        textField.selectedLineColor = UIColor(r: 128, g: 30, b: 72)
        textField.selectedTitleColor = UIColor(r: 128, g: 30, b: 72)
        textField.placeholder = "Senha"
        textField.font = UIFont.systemFont(ofSize: 16)
        textField.textColor = UIColor.black
        //textField.keyboardAppearance =  UIKeyboardAppearance.default
        textField.autocapitalizationType = .none
        //textField.becomeFirstResponder()
        textField.returnKeyType = UIReturnKeyType.done
        textField.autocorrectionType = UITextAutocorrectionType.no
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    
    let botaoEntre: UIButton = {
        let botao = UIButton(type: .custom)
        botao.backgroundColor = .gray
        botao.setTitle("Entre", for: .normal)
        botao.setTitleColor(UIColor.white, for: .normal)
        botao.addTarget(self, action: #selector(fazerLogin), for: .touchUpInside)
        botao.translatesAutoresizingMaskIntoConstraints = false
        botao.isUserInteractionEnabled = false
        return botao
    }()
    
    
    let box: UIView = {
        let efeito = UIBlurEffect(style: UIBlurEffectStyle.regular)
        let view = UIVisualEffectView(effect: efeito)
        view.backgroundColor = UIColor(white: 0.9, alpha: 0.8)
        view.layer.cornerRadius = 10
        view.layer.masksToBounds = true
        return view
    }()
    
    let tituloEmpresa: UILabel = {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 100, height: 50))
        //label.textColor = UIColor.white
        label.textAlignment = NSTextAlignment.center
        label.font = UIFont.boldSystemFont(ofSize: 30)
        label.text = "Voraz"
        return label
    }()
    
    let sloganEmpresa: UILabel = {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 100, height: 50))
        //label.textColor = UIColor.white
        label.textAlignment = NSTextAlignment.center
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.text = "Soluções em Software"
        return label
    }()
    
    //    let statusDownload: LTMorphingLabel = {
    //        let label = LTMorphingLabel(frame: CGRect(x: 0, y: 0, width: 100, height: 50))
    //        //label.morphingEffect = LTMorphingEffect.scale
    //        label.morphingEffect = LTMorphingEffect.evaporate
    //        label.numberOfLines = 0
    //        label.textAlignment = NSTextAlignment.center
    //        label.font = UIFont.systemFont(ofSize: 14)
    //        label.translatesAutoresizingMaskIntoConstraints = false
    //        label.textColor = UIColor.black
    //        return label
    //    }()
    
    let statusLoad: UIView = {
        let view = UIView()
        return view
    }()
    
    //    let activityIndicatorView = NVActivityIndicatorView(frame: CGRect.zero, type: .lineScale, color: UIColor(r: 128, g: 30, b: 72), padding: CGFloat(0))
    //
    var toolbar = FormToolbar()
    
    var tecladoOn = false
    var alturaTeclado: CGFloat = 0
    var isTeclado = 0
    var logoHeight: CGFloat = 0
    
    var statusbarHeight: CGFloat = 0
    
    //funcao ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.registerTeclado()
        self.hideKeyboardWhenTappedAround()
        
        // Configura o delegate dos texfields para a viewController
        loginTextField.delegate = self
        senhaTextField.delegate = self
        self.setupViews()
        //self.persistenciaSessao()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.persistenciaSessao()
    }
    
    
    //SetupViews - adiciona subviews e configura constraints
    func setupViews() {
        
        // View - configurar a view
        view.backgroundColor = UIColor.white
        UIApplication.shared.statusBarStyle = .lightContent
        
        // Adicionar views
        view.addSubview(scrollBox)
        scrollBox.addSubview(logo)
        view.addSubview(statusBar)
        scrollBox.addSubview(loginTextField)
        scrollBox.addSubview(senhaTextField)
        scrollBox.addSubview(botaoEntre)
        
        
        SwiftOverlays.removeAllBlockingOverlays()
        let text = "Verificando Sessão Anterior"
        SwiftOverlays.showBlockingWaitOverlayWithText(text)
        
        scrollBox.snp.makeConstraints { (make) in
            make.top.left.width.right.bottom.equalToSuperview()
        }
        
        statusbarHeight = view.frame.height == 812 ? 40 : 20
        statusBar.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.height.equalTo(statusbarHeight)
            make.width.equalToSuperview()
            make.centerX.equalToSuperview()
        }
        
        //logoHeight = view.frame.height/5
        logoHeight = 150
        let logoWidth = logo.resize(newHeight: logoHeight)
        
        logo.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(statusbarHeight + 30)
            make.centerX.equalToSuperview()
            make.height.equalTo(logoHeight)
            make.width.equalTo(logoWidth)
        }
        
        loginTextField.snp.makeConstraints { (make) in
            make.top.equalTo(logo.snp.bottom).offset(20)
            //make.top.lessThanOrEqualTo(logo.snp.bottom).offset(40)
            make.width.equalTo(view.frame.width-80)
            make.centerX.equalToSuperview()
        }
        
        senhaTextField.snp.makeConstraints { (make) in
            make.top.equalTo(loginTextField.snp.bottom).offset(view.frame.height/30)
            make.width.equalTo(view.frame.width-80)
            make.centerX.equalToSuperview()
        }
        
        botaoEntre.snp.makeConstraints { (make) in
            make.top.equalTo(senhaTextField.snp.bottom).offset(view.frame.height/25)
            make.width.equalTo(view.frame.width-80)
            make.centerX.equalToSuperview()
            make.height.equalTo(view.frame.height/15)
            botaoEntre.layer.cornerRadius = view.frame.height/30
            //make.bottom.equalToSuperview().offset(-logoHeight-40)
            make.bottom.equalToSuperview().offset(view.frame.height < 600 ? -150 : -50)
            
        }
        
        //        //Permite alternar entre os textFields via setas teclado
        //        self.toolbar = FormToolbar(inputs: [loginTextField, senhaTextField])
        //        toolbar.doneButtonTitle = "Fechar"
        //        toolbar.direction = .upDown
    }
    
    func registerTeclado(){
        scrollBox.delegate = self
        scrollBox.isScrollEnabled = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }
    
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboardView))
        //tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboardView() {
        view.endEditing(true)
    }
    
    
    func keyboardWillShow(notification: NSNotification) {
        
        if view.frame.height < 600 {
        scrollBox.contentOffset.x = 0
        scrollBox.contentOffset.y = statusbarHeight+85
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
            scrollBox.contentOffset = CGPoint(x: 0,y: 0)
    }
    
    
    func mostrarBotaoEntre(){
        if let temArroba = loginTextField.text?.contains("@") {
            if temArroba {
                self.botaoEntre.isUserInteractionEnabled = true
                self.botaoEntre.backgroundColor = UIColor(r: 128, g: 30, b: 72)//UIColor(r: 54, g: 142, b: 58)
            } else {
                self.botaoEntre.isUserInteractionEnabled = false
                self.botaoEntre.backgroundColor = .gray
            }
        }
    }
}
