//
//  DadosDashBoard.swift
//  vorazTecnologia
//
//  Created by Lucas de Brito Reis on 15/08/17.
//  Copyright © 2017 Minerva Aplicativos LTDA ME. All rights reserved.
//

import UIKit
import CoreData
import SwiftOverlays
import Alamofire
import SwiftyJSON
import NotificationBannerSwift

extension BaseViewController {
    
    func baixarCampanhasOffline(authToken: String) {
        
        //print("baixarCampanhasOffline")
        
        self.listaCampanhas.removeAll()
        self.numeroPaginasCampanhas = 0
        self.clearComparaCampanhas()
        self.clearCampanhasX()
        
        
        let text = "Baixando lista de campanhas"
        SwiftOverlays.removeAllBlockingOverlays()
        self.removeAllOverlays()
        SwiftOverlays.showBlockingWaitOverlayWithText(text)
        
        let headers = ["X-AUTH-TOKEN": "\(authToken)"]
        
        Alamofire.request(self.urlBase+"campanhas-venda", method: .get, headers: headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    
                    let json = JSON(value)
                    let totalPages = json["meta"]["totalPages"].intValue
                    
                    if totalPages > 0 {
                        
                        for pagina in 1...totalPages {
                            self.paginacaoCampanhas(authToken: authToken, pagina: pagina, totalPages: totalPages, headers: headers)
                        }
                        
                    }
                    
                case .failure(let erro):
                    self.desempilhaErros(erro: erro, response: response){}
                    
                    
                }
        }
    }
    
    func paginacaoCampanhas(authToken: String, pagina: Int, totalPages: Int, headers: Dictionary<String, String>) {
        
        //print("paginacaoCampanhas")
        
        SwiftOverlays.removeAllBlockingOverlays()
        self.removeAllOverlays()
        let text = "Verificando novas campanhas"
        SwiftOverlays.showBlockingWaitOverlayWithText(text)
        
        
        Alamofire.request(self.urlBase+"campanhas-venda?token=\(authToken)&p=\(pagina)", method: .get)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    
                    
                    
                    let json = JSON(value)
                    //salvo json na memória
                    
                    for (_, resultado) in json["campanhaVendas"] {
                        self.atribuiCampanhaVendas(resultado)
                        let id = resultado["id"].stringValue
                        self.listaCampanhas.append(id)
                    }
                    
                    self.numeroPaginasCampanhas = self.numeroPaginasCampanhas + 1
                    
                    SwiftOverlays.removeAllBlockingOverlays()
                    self.removeAllOverlays()
                    let text = "Verificando novas campanhas: página \(self.numeroPaginasCampanhas)/\(totalPages)"
                    SwiftOverlays.showBlockingWaitOverlayWithText(text)
                    
                    if self.numeroPaginasCampanhas > totalPages {
                        self.numeroPaginasCampanhas = 0
                        self.listaCampanhas.removeAll()
                        //self.removeAllOverlays()
                        self.baixarCampanhasOffline(authToken: authToken)
                    } else {
                        if self.numeroPaginasCampanhas == totalPages {
                            
                            //self.removeAllOverlays()
                            //SwiftOverlays.removeAllBlockingOverlays()
                            self.percorrerVendasContadores(authToken: authToken, headers: headers)
                        }
                    }
                    
                case .failure(let erro):
                    
                    self.desempilhaErros(erro: erro, response: response){
                        self.numeroPaginasCampanhas = totalPages + 1
                    }
                }
        }
    }
    
    func percorrerVendasContadores(authToken: String, headers: Dictionary<String, String>) {
        
        //print("percorrerCampanhasEspecies")
        
        let text = "Baixando detalhes das Campanhas: Vendas"
        SwiftOverlays.removeAllBlockingOverlays()
        self.removeAllOverlays()
        SwiftOverlays.showBlockingWaitOverlayWithText(text)
        
        if self.listaCampanhas.count > 0 {
            
            //leio somente os notsync
            for id in self.listaCampanhas {
                
                //atualizo os status
                
                self.baixarVendasContadores(self.urlBase, id: id, authToken: authToken, headers: headers, {_ in
                    
                    let vendasContadores = self.lerVendasContadores()
                    
                    SwiftOverlays.removeAllBlockingOverlays()
                    self.removeAllOverlays()
                    let text = "Atualizando detalhes das campanhas: Vendas \(vendasContadores.count)/\(self.listaCampanhas.count)"
                    SwiftOverlays.showBlockingWaitOverlayWithText(text)
                    
                    //DatabaseController.saveContext()
                    
                    if vendasContadores.count > self.listaCampanhas.count {
                        
                        //self.statusDownload.text = "Limpando banco de dados"
                        self.clearCampanhas(entityName: self.name.VendasEspecies)
                        DatabaseController.saveContext()
                        self.baixarCampanhasOffline(authToken: authToken)
                    } else {

                        if vendasContadores.count == self.listaCampanhas.count {
                            self.percorrerCampanhasEspecies(authToken: authToken, headers: headers)
                            
                        }
                    }
                })
            }
        }
    }
    
    
    func percorrerCampanhasEspecies(authToken: String, headers: Dictionary<String, String>) {
        
        //print("percorrerCampanhasEspecies")
        
        let text = "Baixando detalhes das Campanhas: \nEspecies"
        SwiftOverlays.removeAllBlockingOverlays()
        self.removeAllOverlays()
        SwiftOverlays.showBlockingWaitOverlayWithText(text)
        
        if self.listaCampanhas.count > 0 {
            
            //leio somente os notsync
            for id in self.listaCampanhas {
                
                //atualizo os status
                
                self.baixarEspecies(self.urlBase, id: id, authToken: authToken, headers: headers, {_ in
                    
                    let vendasEspecie = self.lerVendasEspecies()
                    
                    SwiftOverlays.removeAllBlockingOverlays()
                    self.removeAllOverlays()
                    let text = "Atualizando campanhas - Especies: \(vendasEspecie.count)/\(self.listaCampanhas.count)"
                    SwiftOverlays.showBlockingWaitOverlayWithText(text)
                    
                    //DatabaseController.saveContext()
                    
                    if vendasEspecie.count > self.listaCampanhas.count {
                        
                        //self.statusDownload.text = "Limpando banco de dados"
                        self.clearCampanhas(entityName: self.name.VendasEspecies)
                        DatabaseController.saveContext()
                        self.baixarCampanhasOffline(authToken: authToken)
                    } else {
                        
                        //print("vendasEspecie.count = \(vendasEspecie.count)")
                        //print("self.listaCampanhas.count = \(self.listaCampanhas.count)")
                        
                        
                        if vendasEspecie.count == self.listaCampanhas.count {
                            //limpa a memoria
                            //self.listaCampanhas.removeAll()
                            //self.numeroPaginasCampanhas = 0
                            
                            //                            self.removeAllOverlays()
                            //                            SwiftOverlays.removeAllBlockingOverlays()
                            //                            DatabaseController.saveContext()
                            
                            self.percorrerEntregasEvolucao(authToken: authToken, headers: headers)
                            
                            
                        }
                    }
                })
            }
        }
    }
    
    
    func percorrerEntregasEvolucao(authToken: String, headers: Dictionary<String, String>) {
        
        //print("percorrerEntregasEvolucao")
        
        let text = "Baixando detalhes das Campanhas: \nEvolução das Entregas"
        SwiftOverlays.removeAllBlockingOverlays()
        self.removeAllOverlays()
        SwiftOverlays.showBlockingWaitOverlayWithText(text)
        
        if self.listaCampanhas.count > 0 {
            
            //leio somente os notsync
            for id in self.listaCampanhas {
                
                //atualizo os status
                
                self.baixarEvolucao(self.urlBase, id: id, authToken: authToken, headers: headers, {_ in
                    
                    let entregasEvolucao = self.lerEntregasEvolucao()
                    
                    SwiftOverlays.removeAllBlockingOverlays()
                    self.removeAllOverlays()
                    let text = "Atualizando campanhas - Evolução das Entregas: \n\(entregasEvolucao.count)/\(self.listaCampanhas.count)"
                    SwiftOverlays.showBlockingWaitOverlayWithText(text)
                    
                    //DatabaseController.saveContext()
                    
                    if entregasEvolucao.count > self.listaCampanhas.count {
                        //self.statusDownload.text = "Limpando banco de dados"
                        self.clearCampanhas(entityName: self.name.EntregasEvolucao)
                        DatabaseController.saveContext()
                        self.baixarCampanhasOffline(authToken: authToken)
                    } else {
                        
                        if entregasEvolucao.count == self.listaCampanhas.count {
                            self.percorrerCampanhasVendasForecast(authToken: authToken, headers: headers)
                        }
                    }
                })
            }
        }
    }
    
    
    
    func percorrerCampanhasVendasForecast(authToken: String, headers: Dictionary<String, String>) {
        //print("percorrerCampanhasVendasForecast")
        
        let text = "Baixando detalhes das Campanhas: \nVendas Projetadas"
        SwiftOverlays.removeAllBlockingOverlays()
        self.removeAllOverlays()
        SwiftOverlays.showBlockingWaitOverlayWithText(text)
        
        if self.listaCampanhas.count > 0 {
            
            //leio somente os notsync
            for id in self.listaCampanhas {
                
                //atualizo os status
                
                self.baixarVendasForecast(self.urlBase, id: id, authToken: authToken, headers: headers, {_ in
                    
                    let vendasForecast = self.lerVendasForecast()
                    
                    
                    SwiftOverlays.removeAllBlockingOverlays()
                    self.removeAllOverlays()
                    let text = "Atualizando campanhas: \(vendasForecast.count)/\(self.listaCampanhas.count)"
                    SwiftOverlays.showBlockingWaitOverlayWithText(text)
                    
                    //DatabaseController.saveContext()
                    
                    if vendasForecast.count > self.listaCampanhas.count {
                        //self.statusDownload.text = "Limpando banco de dados"
                        self.clearCampanhas(entityName: self.name.VendasForecast)
                        DatabaseController.saveContext()
                        self.baixarCampanhasOffline(authToken: authToken)
                    } else {
                        
                        if vendasForecast.count == self.listaCampanhas.count {
                            
                            self.baixarComparaCampanhas(self.urlBase, authToken: authToken, headers: headers, {
                                
                                //limpa a memoria
                                self.listaCampanhas.removeAll()
                                self.numeroPaginasCampanhas = 0
                                
                                self.removeAllOverlays()
                                SwiftOverlays.removeAllBlockingOverlays()
                                DatabaseController.saveContext()
                                
                            })
                        }
                    }
                })
            }
        }
    }
    
    func campanhaFiltrada(campanhaId: String) -> CampanhaVendas? {
        
        let requestCampanhas  = NSFetchRequest<NSFetchRequestResult>(entityName: self.name.campanhaVendas)
        requestCampanhas.returnsObjectsAsFaults = false
        requestCampanhas.predicate = NSPredicate(format: "id == %@", campanhaId)
        
        do {
            let campanhas = try(DatabaseController.getContext().fetch(requestCampanhas)) as! [CampanhaVendas]
            if campanhas.count > 0 {
                /* remover duplicatas */
                return campanhas[0]
            } else {
                return nil
            }
            
        } catch let erro {
            print("erro ao carregar \(self.name.campanhaVendas): \(erro)")
        }
        
        return nil
    }
    
    
    func lerCampanhas() -> [CampanhaVendas] {
        let requestCampanhas  = NSFetchRequest<NSFetchRequestResult>(entityName: self.name.campanhaVendas)
        requestCampanhas.returnsObjectsAsFaults = false
        
        //let sort = NSSortDescriptor(key: "id", ascending: false)
        //requestCampanhas.sortDescriptors = [sort]
        
        do {
            let campanhas = try(DatabaseController.getContext().fetch(requestCampanhas)) as! [CampanhaVendas]
            return campanhas
            
        } catch let erro {
            print("erro ao carregar \(self.name.campanhaVendas): \(erro)")
        }
        return [CampanhaVendas]()
    }
    
    func lerBancoDeDadosSemId(_ entityName: String) -> [NSManagedObject] {
        
        var entidade = [NSManagedObject]()
        
        let request  = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        request.returnsObjectsAsFaults = false

        
        do {
            entidade = try(DatabaseController.getContext().fetch(request)) as! [NSManagedObject]
        } catch let erro {
            print("erro ao carregar \(entityName): \(erro)")
        }
        
        return entidade
    }
    
    
    func baixarVendasContadores(_ urlBase:String, id: String, authToken token: String, headers: Dictionary<String, String>, _ completionHandler: @escaping () -> Void) {
        
        let text = "Baixando detalhes das campanhas: Vendas"
        SwiftOverlays.removeAllBlockingOverlays()
        self.removeAllOverlays()
        SwiftOverlays.showBlockingWaitOverlayWithText(text)
        
        Alamofire.request(urlBase+"dashboard/widgets/vendas-contadores?campanha_id=\(id)", method: .get, headers: headers)
            .validate()
            .responseJSON { (response) in
                switch response.result {
                case .success(let valor):
                    let json = JSON(valor)
                    let contadores = self.atribuiVendasContadores(json)
                    
                    let campanha = self.campanhaFiltrada(campanhaId: id)
                    campanha?.vendasContadores = contadores
                    
                    
                    campanha?.cd_vendasContadoresOK = true
                    DatabaseController.saveContext()
                    
                    completionHandler()
                    
                case .failure(let erro):
                    
                    self.desempilhaErros(erro: erro, response: response){}
                }
        }
    }
    
    func baixarEspecies(_ urlBase:String, id: String, authToken token: String, headers: Dictionary<String, String>, _ completionHandler: @escaping () -> Void) {
        
        let text = "Baixando lista de campanhas: Espécies"
        SwiftOverlays.removeAllBlockingOverlays()
        self.removeAllOverlays()
        SwiftOverlays.showBlockingWaitOverlayWithText(text)
        
        Alamofire.request(urlBase+"dashboard/widgets/vendas-especies?campanha_id=\(id)", method: .get, headers: headers)
            .validate()
            .responseJSON { (response) in
                switch response.result {
                case .success(let valor):
                    let json = JSON(valor)
                    
                    let campanha = self.campanhaFiltrada(campanhaId: id)
                    
                    var vetorEspecies: [VendasEspecies] = []
                    
                    for (_, JsonEspecies) in json["especies"] {
                        let especie = self.atribuiVendasEspecies(JsonEspecies)
                        vetorEspecies.append(especie)
                    }
                        campanha?.setValue(NSSet(array: vetorEspecies), forKey: "vendasEspecies")

                    campanha?.cd_vendasEspecieOK = true
                    DatabaseController.saveContext()
                    
                    completionHandler()
                    
                case .failure(let erro):
                    self.desempilhaErros(erro: erro, response: response){}
                }
        }
    }
    
    func baixarEvolucao(_ urlBase:String, id: String, authToken token: String, headers: Dictionary<String, String>, _ completionHandler: @escaping () -> Void) {
        
        let text = "Baixando lista de campanhas: Evolução Campanhas"
        SwiftOverlays.removeAllBlockingOverlays()
        self.removeAllOverlays()
        SwiftOverlays.showBlockingWaitOverlayWithText(text)
        
        Alamofire.request(urlBase+"dashboard/widgets/entregas-evolucao?campanha_id=\(id)", method: .get, headers: headers)
            .validate()
            .responseJSON { (response) in
                switch response.result {
                case .success(let valor):
                    let json = JSON(valor)
                    
                    let campanha = self.campanhaFiltrada(campanhaId: id)
                    
                    var vetorEntregasEvolucao: [EntregasEvolucao] = []
                    
                    for (_, variedades) in json["variedades"] {
                        vetorEntregasEvolucao.append(self.atribuiEntregasEvolucao(variedades))
                    }
                    campanha?.cd_entregasEvolucaoOK = true
                    
                    campanha?.setValue(NSSet(array: vetorEntregasEvolucao), forKey: "entregasEvolucao")
                    
                    DatabaseController.saveContext()
                    
                    
                    
                    completionHandler()
                    
                case .failure(let erro):
                    self.desempilhaErros(erro: erro, response: response){}
                }
        }
    }
    
    func baixarVendasForecast(_ urlBase:String, id: String, authToken token: String, headers: Dictionary<String, String>, _ completionHandler: @escaping () -> Void) {
        
        let text = "Baixando lista de campanhas: Vendas Previstas"
        SwiftOverlays.removeAllBlockingOverlays()
        self.removeAllOverlays()
        SwiftOverlays.showBlockingWaitOverlayWithText(text)
        
        
        Alamofire.request(urlBase+"dashboard/widgets/vendas-forecast?campanha_id=\(id)", method: .get, headers: headers)
            .validate()
            .responseJSON { (response) in
                switch response.result {
                case .success(let valor):
                    let json = JSON(valor)
                    
                    let campanha = self.campanhaFiltrada(campanhaId: id)
                    
                    var vetorVendasForecast: [VendasForecast] = []
                    
                    for (_, variedades) in json["variedades"] {
                        vetorVendasForecast.append(self.atribuiVendasForecast(variedades))
                    }
                    campanha?.cd_vendasForecastOK = true
                    campanha?.setValue(NSSet(array: vetorVendasForecast), forKey: "vendasForecast")
                    
                    DatabaseController.saveContext()
                    
                    completionHandler()
                    
                case .failure(let erro):
                    self.desempilhaErros(erro: erro, response: response){}
                }
        }
    }
    
    
    func baixarComparaCampanhas(_ urlBase:String, authToken token: String, headers: Dictionary<String, String>, _ completionHandler: @escaping () -> Void) {
        
        //print("baixar Compara campanhas")
        
        let text = "Baixando Comparação entre Campanhas"
        SwiftOverlays.removeAllBlockingOverlays()
        self.removeAllOverlays()
        SwiftOverlays.showBlockingWaitOverlayWithText(text)
        
        Alamofire.request(urlBase+"dashboard/widgets/campanhas-evolucao", method: .get, headers: headers)
            .validate()
            .responseJSON { (response) in
                switch response.result {
                case .success(let valor):
                    
                    let json = JSON(valor)
                    
                    self.clearComparaCampanhas()
                    for (_, resultado) in json["campanhas"] {
                        self.atribuiComparaCampanhas(resultado)
                    }
                    
                    
                    //****************todos os pedidos sincronizados
                    let title = "Pedidos e Campanhas"
                    let subtitle = "Pedidos e campanhas atualizados com sucesso"
                    let banner = NotificationBanner(title: title, subtitle: subtitle, style: .success)
                    banner.show()
                    completionHandler()
                    
                    
                    
                case .failure(let erro):
                    self.desempilhaErros(erro: erro, response: response){}
                }
        }
    }
    
    
    
    func clearCampanhasX() {
        
        let requestCampanhasVendas = NSFetchRequest<NSFetchRequestResult>(entityName: self.name.campanhaVendas)
        requestCampanhasVendas.returnsObjectsAsFaults = false
        
        let resultdadosCampanhas = try? DatabaseController.getContext().fetch(requestCampanhasVendas)
        let coreCampanhasVendas = resultdadosCampanhas as! [CampanhaVendas]
        
        
        for objectCampanhasVendas in coreCampanhasVendas {
            DatabaseController.getContext().delete(objectCampanhasVendas)
        }
        
        DatabaseController.saveContext()
    }
    
    
    func clearCampanhas(entityName: String) {
        
        let requestCampanhasVendas = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        requestCampanhasVendas.returnsObjectsAsFaults = false
        
        let resultdadosCampanhas = try? DatabaseController.getContext().fetch(requestCampanhasVendas)
        let coreCampanhasVendas = resultdadosCampanhas as! [NSManagedObject] //[CampanhaVendas]
        
        
        for objectCampanhasVendas in coreCampanhasVendas {
            DatabaseController.getContext().delete(objectCampanhasVendas)
        }
        
        DatabaseController.saveContext()
    }
    
    func clearCampanhaSelecionada() {
        
        let requestCampanhaSelecionada = NSFetchRequest<NSFetchRequestResult>(entityName: self.name.CampanhaSelecionada)
        requestCampanhaSelecionada.returnsObjectsAsFaults = false
        
        let resultCampanhaSelecionada = try? DatabaseController.getContext().fetch(requestCampanhaSelecionada)
        let coreCampanhaSelecionada = resultCampanhaSelecionada as! [CampanhaSelecionada]
        
        
        for objectCampanhaSelecionada in coreCampanhaSelecionada {
            DatabaseController.getContext().delete(objectCampanhaSelecionada)
        }
        
        DatabaseController.saveContext()
    }
    
    
    
    
    func clearComparaCampanhas() {
        
        let requestComparaCampanhas = NSFetchRequest<NSFetchRequestResult>(entityName: self.name.ComparaCampanhas)
        requestComparaCampanhas.returnsObjectsAsFaults = false
        
        let resultComparaCampanhas = try? DatabaseController.getContext().fetch(requestComparaCampanhas)
        let coreComparaCampanhas = resultComparaCampanhas as! [ComparaCampanhas]
        
        
        for objectComparaCampanhas in coreComparaCampanhas {
            DatabaseController.getContext().delete(objectComparaCampanhas)
        }
        
        DatabaseController.saveContext()
    }
  
    
}
