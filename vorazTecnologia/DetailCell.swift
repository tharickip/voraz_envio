//
//  DetailCell.swift
//  agroshowiOS
//
//  Created by Lucas de Brito Reis on 15/06/17.
//  Copyright © 2017 Minerva Aplicativos LTDA ME. All rights reserved.
//

import UIKit
import SnapKit

class DetailCell: UITableViewCell {
    
    
    let valorUnitario: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.center
        label.text = ""
        return label
    }()
    
    let botao: UIButton = {
        let botao = UIButton()
        botao.backgroundColor = UIColor.red
        return botao
    }()
    
    let descricaoProduto: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.center
        label.textColor = UIColor.black
        label.text = ""
        return label
    }()
    
    let desconto: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.center
        label.textColor = UIColor.red
        label.text = ""
        return label
    }()
    
    let total: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.center
        label.textColor = UIColor(r: 54, g: 142, b: 58)
        label.text = ""
        return label
    }()
    
    
    
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setupViews() {
        self.backgroundColor = UIColor.white
        
        // Adiciona views
        self.addSubview(valorUnitario)
        self.addSubview(descricaoProduto)
        self.addSubview(desconto)
        self.addSubview(total)
        
        valorUnitario.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(10)
            make.width.equalTo(80)
            //make.height.equalToSuperview()
        }
        
        total.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.right.equalToSuperview().offset(-10)
            make.width.equalTo(100)
            //make.height.equalToSuperview()
        }
        
        descricaoProduto.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(10)
            make.left.equalTo(valorUnitario.snp.right)
            make.right.equalTo(total.snp.left)
        }
        
        desconto.snp.makeConstraints { (make) in
            make.top.equalTo(descricaoProduto.snp.bottom).offset(10)
            make.left.equalTo(valorUnitario.snp.right)
            make.right.equalTo(total.snp.left)
            make.bottom.equalToSuperview().offset(-10)
        }
    }
    
    func config(pedido: Pedidos, indexPath: IndexPath) {
        
        let itens = pedido.itens?.allObjects
        let item = itens?[indexPath.row] as! Itens
        
        if let percentualDesconto = item.percentualDesconto {
            if let descontoUnitario = item.descontoUnitario {
                desconto.text = "(\(String(describing: (Double(percentualDesconto)?.arredonda())!)))% (R$ \(String(describing: (Double(descontoUnitario)?.arredonda())!)))"
            }
        }
        
        if let produtoNome = item.produto?.nome {
            descricaoProduto.text = produtoNome
        }
        
        if let quantidadePedido = item.quantidadePedido {
            if let precoUnitario = item.valorUnitario {
                valorUnitario.text = "\(String(describing: quantidadePedido))x R$ \(String(describing: (Double(precoUnitario)?.arredonda())!))"
            }
        }
        
        if let valorTotal = pedido.valorTotal {
            total.text = " R$ \((Double(valorTotal)?.arredonda())!)"
        }
        
        
        
    }
    
}
