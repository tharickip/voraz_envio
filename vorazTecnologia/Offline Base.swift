//
//  ExtensionFeed.swift
//  vorazTecnologia
//
//  Created by Lucas de Brito Reis on 31/07/17.
//  Copyright © 2017 Minerva Aplicativos LTDA ME. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import SCLAlertView
import SwiftOverlays
import NotificationBannerSwift

extension BaseViewController {
    
    func liberarOffline(_ urlBase: String, id: String, indexPath: IndexPath, authToken: String, observacao: String,  _ completion: (@escaping () -> Void)){
        
        let urlRequisicao = urlBase + "pedidos/\(id)/desbloquear"
        
        //let observacao = "observacao teste"
        let headers = ["X-AUTH-TOKEN": "\(authToken)", "observacoes": observacao]
        
        Alamofire.request(urlRequisicao, method: .post, headers: headers)
            .responseData { (response) in
                
                switch response.result {
                case .success(let valor):
                    
                    let mensagem = self.retornaMensagem(JSON(valor))
                    
                    if mensagem != "" {
                        
                        if let pedido = self.pedidofiltrado(pedidoId: id) {
                            pedido.cd_isSync = true
                            pedido.cd_requisicaoDeuCerto = false
                            pedido.cd_mensagemErro = mensagem
                            DatabaseController.saveContext()
                            completion()
                        }
                        
                        //self.mostrarAlerta(mensagem)
                        
                    } else {
                        
                        self.getPedidoOffline(urlBase, id: id, authToken: authToken, headers: headers, { _ in
                            completion()
                        })
                        
                        
                        
                    }
                case .failure:
                    if let pedido = self.pedidofiltrado(pedidoId: id) {
                        pedido.cd_requisicaoDeuCerto = false
                        DatabaseController.saveContext()
                        completion()
                    }
                }
        }
    }
    
    
    func pedirRevisaoOffline(_ urlBase: String, id: String, indexPath: IndexPath, authToken: String, observacao: String,  _ completionHandler: (@escaping (_ parametro: Any) -> Void)){
        
        
        
        let parametros = ["revisao": ["observacao": observacao, "pedido": [ "id": id ]]]
        let urlRequisicao = urlBase + "pedidos/\(id)/revisao"
        let headersRequisicao = ["X-AUTH-TOKEN": "\(authToken)", "observacoes": observacao]
        let headers = ["X-AUTH-TOKEN": "\(authToken)"]
        
        Alamofire.request(urlRequisicao, method: .post, parameters: parametros, encoding: JSONEncoding.default, headers: headersRequisicao)
            .responseJSON { (response) in
                
                switch response.result {
                case .success(let valor):
                    
                    let mensagem = self.retornaMensagem(JSON(valor))
                    
                    if mensagem != "" {
                        if let pedido = self.pedidofiltrado(pedidoId: id) {
                            pedido.cd_isSync = true
                            pedido.cd_requisicaoDeuCerto = false
                            pedido.cd_mensagemErro = mensagem
                            DatabaseController.saveContext()
                            completionHandler(Any.self)
                        }
                        //self.mostrarAlerta(mensagem)
                    } else {
                        
                        self.getPedidoOffline(urlBase, id: id, authToken: authToken, headers: headers, { _ in
                            completionHandler(Any.self)
                        })
                        
                    }
                case .failure:
                    if let pedido = self.pedidofiltrado(pedidoId: id) {
                        pedido.cd_requisicaoDeuCerto = false
                        DatabaseController.saveContext()
                        completionHandler(Any.self)
                    }
                }
        }
        
    }
    
    func bloquearOffline(_ urlBase: String, id: String, indexPath: IndexPath, authToken: String, observacao: String, motivoBloqueioId: Int, _ completionHandler: (@escaping (_ parametro: Any) -> Void)) {
        
        
        
        let urlRequisicao = urlBase + "pedidos/\(id)/bloquear"
        let headers: HTTPHeaders = [
            "X-AUTH-TOKEN": "\(authToken)",
            "Content-Type": "application/json"
        ]
        
        let parametros = [
            "params": [
                "motivoBloqueioId": motivoBloqueioId,
                "observacao": "\(observacao)"
            ]
        ]
        
        Alamofire.request(urlRequisicao, method: .post, parameters: parametros, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { response in
                
                if let statusCode = response.response?.statusCode {
                    if statusCode == 200 {
                        
                        self.getPedidoOffline(urlBase, id: id, authToken: authToken, headers: headers, { _ in
                            completionHandler(Any.self)
                        })
                    } else {
                        
                        switch response.result {
                        case .success(let valor):
                            
                            var mensagem = self.retornaMensagem(JSON(valor))
                            
                            if mensagem == "" {
                                mensagem = "Erro não identificado"
                            }
                            
                            if let pedido = self.pedidofiltrado(pedidoId: id) {
                                pedido.cd_isSync = true
                                pedido.cd_requisicaoDeuCerto = false
                                pedido.cd_mensagemErro = mensagem
                                DatabaseController.saveContext()
                            }
                            //self.mostrarAlerta(mensagem)
                            completionHandler(Any.self)
                            
                        case .failure:
                            if let pedido = self.pedidofiltrado(pedidoId: id) {
                                pedido.cd_requisicaoDeuCerto = false
                                DatabaseController.saveContext()
                                completionHandler(Any.self)
                            }
                        }
                    }
                } else {
                    if let pedido = self.pedidofiltrado(pedidoId: id) {
                        pedido.cd_requisicaoDeuCerto = false
                        DatabaseController.saveContext()
                        completionHandler(Any.self)
                    }
                }
                
                
                
                
        }
    }
    
    
    func getPedidoOffline(_ urlBase:String, id: String, authToken token: String, headers: Dictionary<String, String>, _ completionHandler: @escaping () -> Void) {
        
        Alamofire.request(urlBase+"pedidos/\(id)", method: .get, headers: headers)
            .validate()
            .responseJSON { (response) in
                switch response.result {
                case .success(let valor):
                    let json  = JSON(valor)
                    
                    
                    let mensagem = self.retornaMensagem(JSON(valor))
                    
                    if mensagem != "" {
                        if let pedido = self.pedidofiltrado(pedidoId: id) {
                            pedido.cd_isSync = true
                            pedido.cd_requisicaoDeuCerto = false
                            pedido.cd_mensagemErro = mensagem
                            DatabaseController.saveContext()
                        }
                    } else {
                        
                        if self.pedidofiltrado(pedidoId: id) != nil {
                            self.atualizaPedido(pedidoId: id, json: json["pedido"])
                            
                            completionHandler()
                        } else {
                            self.atribuiPedido(json["pedido"])
                            
                            DatabaseController.saveContext()
                            completionHandler()
                        }
                    }
                case .failure(let erro):
                    let message = self.falhaConexao(erro, response: response)
                    if let pedido = self.pedidofiltrado(pedidoId: id) {
                        pedido.cd_requisicaoDeuCerto = false
                        pedido.cd_mensagemErro = message
                        DatabaseController.saveContext()
                    }
                }
        }
    }
    
    /************************* outros **********************/
    
    func persistenciaSessao(){
        
        
        let dadosLogin = self.lerBancoDeDados(name.dadosLogin) as! [DadosLogin]
        
        if dadosLogin.count > 0 {
            //tem banco de dado
            
            
            
            SwiftOverlays.removeAllBlockingOverlays()
            let text = "Verificando Sessão Anterior"
            SwiftOverlays.showBlockingWaitOverlayWithText(text)
            
            if let authToken = dadosLogin[0].authToken {
                
                
                let feed = Feed()
                // self.present(UINavigationController(rootViewController: feed), animated: true, completion: nil)
                self.present(UINavigationController(rootViewController: feed), animated: false, completion: {
                    
                    self.verificaToken(authToken: authToken, {
                    
                        
                        //self.removeAllOverlays()
                        //SwiftOverlays.removeAllBlockingOverlays()
                        //feed.showWaitOverlay()
                        feed.sincroniza()
                    })
                })
            }
            
        } else {
            self.removeAllOverlays()
        }
    }
    
    
    func salvaRequisicaoliberar(_ urlBase: String, id: String, indexPath: IndexPath, observacao: String,  authToken: String,_ completionHandler: @escaping () -> Void) {
        
        let pedido = self.pedidofiltrado(pedidoId: id)
        pedido?.cd_tipoRequisicao   = self.tipoRequisicao.liberar
        pedido?.cd_observacao       = observacao
        pedido?.cd_motivoBloqueioId = String(0)
        pedido?.cd_isSync           = false
        pedido?.cd_indexPath        = Int32(indexPath.row)
        pedido?.status              = "LIBERADO"
        DatabaseController.saveContext()
        
        completionHandler()
    }
    
    
    func salvaRequisicaoPedirRevisao(_ urlBase: String, id: String, indexPath: IndexPath, observacao: String, authToken: String, _ completionHandler: @escaping () -> Void) {
        
        let pedido = self.pedidofiltrado(pedidoId: id)
        pedido?.cd_tipoRequisicao   = self.tipoRequisicao.pedirRevisao
        pedido?.cd_observacao       = observacao
        pedido?.cd_motivoBloqueioId = String(0)
        pedido?.cd_isSync           = false
        pedido?.cd_indexPath        = Int32(indexPath.row)
        pedido?.status              = "A_REVISAR"
        DatabaseController.saveContext()
        completionHandler()
    }
    
    
    func salvaRequisicaobloquear(_ urlBase: String, id: String, indexPath: IndexPath, observacao: String, motivoBloqueioId: Int, authToken: String, _ completionHandler: @escaping () -> Void) {
        
        let pedido = self.pedidofiltrado(pedidoId: id)
        pedido?.cd_tipoRequisicao   = self.tipoRequisicao.bloquear
        pedido?.cd_observacao       = observacao
        pedido?.cd_motivoBloqueioId = String(motivoBloqueioId)
        pedido?.cd_isSync           = false
        pedido?.cd_indexPath        = Int32(indexPath.row)
        pedido?.status              = "BLOQUEADO"
        DatabaseController.saveContext()
        completionHandler()
    }
    
    
    func offline(_ response:  HTTPURLResponse? , _ completionHandler: @escaping () -> Void) {
        
        SwiftOverlays.removeAllBlockingOverlays()
        print("offline")
        let title = ""
        let subtitle = "Você está trabalhando offline"
        let banner = NotificationBanner(title: title, subtitle: subtitle, style: .warning)
        
        if let httpStatusCode = response?.statusCode {
            if httpStatusCode == 401 {
                // o token expirou - nao autorizado
                //ABRO UM POP UP E FACO TUDO DE NOVO
                let dadosLogin = self.lerBancoDeDados(self.name.dadosLogin) as! [DadosLogin]
                if dadosLogin.count > 0 {
                    if let email = dadosLogin[0].usuario?.email {
                        DatabaseController.saveContext()
                        self.tokenExpirou(email)
                    }
                }
            } else {
                print("response: ", response?.statusCode, " ", response?.description)
                completionHandler()
                banner.show()
            }
        } else {
            print("no response")
            completionHandler()
            banner.show()
        }
        
    }
    
    
    
    func verificaToken(authToken token: String, _ completionHandler: @escaping () -> Void) {
        
        SwiftOverlays.removeAllBlockingOverlays()
        let text = "Verificando Sessão Anterior"
        SwiftOverlays.showBlockingWaitOverlayWithText(text)
        print("verificaToken \(token)")
        
        let headers = ["X-AUTH-TOKEN": "\(token)"]
        
        Alamofire.request(self.urlBase+"pedidos", method: .get, headers: headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success:
                    print("verificaToken success")
                    completionHandler()
                    
                case .failure:
                    print("verificaToken failure")
                    self.offline(response.response, {
                        completionHandler()
                    })
                    break
                }
        }
    }
    
    func tokenExpirou(_ email: String) {
        
        let titulo      = "Autenticação"
        let placeHolder = "Senha"
        let nomeBotao   = "OK"
        
        print("token expirou")
        
        // Add a text field
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false,
            showCircularIcon: true
            //contentViewColor: UIColor(r: 54, g: 142, b: 58)
        )
        
        let alert = SCLAlertView(appearance: appearance)
        let textoPreenchido = alert.addTextField(placeHolder)
        textoPreenchido.spellCheckingType = .no
        textoPreenchido.autocorrectionType = .no
        textoPreenchido.autocapitalizationType = .none
        textoPreenchido.isSecureTextEntry = true
        
        alert.addButton("Enviar") {
            if let textoCampo = textoPreenchido.text {
                if textoCampo != "" {
                    
                    let senha = textoCampo
                    self.novoToken(email: email, senha: senha)
                    
                } else {
                    let mensagem = "O campo não pode ser vazio"
                    //self.mostrarAlerta(mensagem, nomeBotao: nomeBotao, style: .error)
                    self.mostrarAlerta(mensagem, title: titulo, nomeBotao: nomeBotao, style: .error){
                        self.tokenExpirou(email)
                    }
                }
                
            }
        }
        alert.addButton("Cancelar") {
            //            let mensagem = "Você está trabalhando offline"
            //            self.mostrarAlerta(mensagem, nomeBotao: nomeBotao, style: .error)
            
            let title = ""
            let subtitle = "Você está trabalhando offline"
            let banner = NotificationBanner(title: title, subtitle: subtitle, style: .warning)
            banner.show()
            
            //alert.showSuccess(titulo, subTitle: descricao)
        }
        alert.showInfo(titulo, subTitle: email)
    }
    
    func novoToken(email: String, senha: String) {
        
        SwiftOverlays.removeAllBlockingOverlays()
        let text = "Validando usuário"
        SwiftOverlays.showBlockingWaitOverlayWithText(text)
        
        Alamofire.request(self.urlBase + "login?email=\(email)&senha=\(senha)", method: .post)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    
                    let json = JSON(value)
                    
                    _ = self.atribuilogin(json)
                    
                    let dadosLogin = self.lerBancoDeDados(self.name.dadosLogin) as! [DadosLogin]
                    
                    if dadosLogin.count > 0 {
                        if let authtoken = dadosLogin[0].authToken {
                            self.baixarPedidosOffline(authToken: authtoken)
                        }
                    }
                case .failure(let error):
                    
                    self.desempilhaErros(erro: error, response: response){
                        self.tokenExpirou(email)
                    }
                    
                    
                    
                }
        }
    }
    
    
    func desempilhaErros(erro: Error, response: (DataResponse<Any>), completion: () -> ()){
        
        print("desempilhaErros")
        
        if !self.estaEmpilhando {
            print("esta empilhando if")
            
            let message = self.falhaConexao(erro, response: response)
            SwiftOverlays.removeAllBlockingOverlays()
            self.removeAllOverlays()
            self.mostrarAlerta(message, nomeBotao: "OK", style: .error)
            self.estaEmpilhando = true
        }
        
        
        completion()
        
    }
    
    
    
    
    
    
    
}

