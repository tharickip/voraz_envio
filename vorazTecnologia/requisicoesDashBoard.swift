//
//  requisicoesDashBoard.swift
//  vorazTecnologia
//
//  Created by Lucas de Brito Reis on 14/08/17.
//  Copyright © 2017 Minerva Aplicativos LTDA ME. All rights reserved.
//

import UIKit
import CoreData
import SwiftOverlays
import Alamofire
import SwiftyJSON
import NotificationBannerSwift


extension DashBoard {
    
    func baixarCampanhas(authToken: String) {
        
        self.listaCampanhas.removeAll()
        self.novasCampanhas.removeAll()
        self.numeroPaginasCampanhas = 0
        
        
        let text = "Baixando lista de Campanhas"
        SwiftOverlays.removeAllBlockingOverlays()
        self.removeAllOverlays()
        SwiftOverlays.showBlockingWaitOverlayWithText(text)
        
        let headers = ["X-AUTH-TOKEN": "\(authToken)"]
        //self.statusDownload.text = "Baixando lista de pedidos"
        Alamofire.request(self.urlBase+"/campanhas-venda", method: .get, headers: headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    
                    let json = JSON(value)
                    let totalPages = json["meta"]["totalPages"].intValue
                    
                    if totalPages > 0 {
                        
                        for pagina in 1...totalPages {
                            self.paginacaoCampanhas(authToken: authToken, pagina: pagina, totalPages: totalPages, headers: headers)
                        }
                        
                    }
                    
                case .failure(let erro):
                    SwiftOverlays.removeAllBlockingOverlays()
                    let message = self.falhaConexao(erro, response: response)
                    
                    self.mostrarAlerta(message, nomeBotao: "OK", style: .error)
                    
                    
                }
        }
    }
    
    
    func percorrerJsonCampanhas(authToken: String, headers: Dictionary<String, String>) {
        
        //self.statusDownload.text = "Baixando detalhes dos pedidos"
        let text = "Baixando detalhes das novas campanhas"
        SwiftOverlays.removeAllBlockingOverlays()
        self.removeAllOverlays()
        SwiftOverlays.showBlockingWaitOverlayWithText(text)
        
        
        
        
        //crio um vetor com [is_sync]
        //primeiro verifico se está no bd
        //depois verifico se mudou o status
        var idNotSync: [String] = []
        
        
        
        
        if self.listaCampanhas.count > 0 {
            
            for idTemp in self.listaCampanhas {
                
                let pedidoTemp = self.pedidofiltrado(pedidoId: idTemp)
                if pedidoTemp == nil {
                    //nao está listado no banco de dados
                    idNotSync.append(idTemp)
                } else {
                    //esta no banco de dados
                    //verifico se o status mudou
                    if pedidoTemp?.status! != self.statusPedidos[idTemp] {
                        idNotSync.append(idTemp)
                    }
                }
            }
        }
        
        //**********************
        //faco um laco que coloca true pra status que nao mudaram
        
        if idNotSync.count == 0 {
            SwiftOverlays.removeAllBlockingOverlays()
            self.removeAllOverlays()
            
            //****************todos os pedidos sincronizados
            let title = "Campanhas"
            let subtitle = "Lista de campanhas atualizada com sucesso"
            let banner = NotificationBanner(title: title, subtitle: subtitle, style: .success)
            banner.show()
        } else {
            
            //leio somente os notsync
            for id in idNotSync {
                
                //atualizo os status
                
                self.getPedido(self.urlBase, id: id, authToken: authToken, headers: headers, {_ in
                    
                    //let pedidos = self.lerBancoDeDados(self.name.pedidos)
                    let pedidos = self.lerNotSync(isSync: true)
                    
                    SwiftOverlays.removeAllBlockingOverlays()
                    self.removeAllOverlays()
                    let text = "Atualizando pedidos: \(pedidos.count)/\(self.listaPedidos.count)"
                    SwiftOverlays.showBlockingWaitOverlayWithText(text)
                    
                    //self.statusDownload.text = "Baixando pedidos \(pedidos.count)/\(self.listaPedidos.count)"
                    DatabaseController.saveContext()
                    
                    if pedidos.count > self.listaPedidos.count {
                        //self.statusDownload.text = "Limpando banco de dados"
                        self.clearContext()
                        DatabaseController.saveContext()
                        self.baixarPedidosOffline(authToken: authToken)
                    } else {
                        
                        if pedidos.count == self.listaPedidos.count {
                            //limpa a memoria
                            self.listaPedidos.removeAll()
                            self.statusPedidos.removeAll()
                            self.numeroPaginas = 0
                            
                            self.removeAllOverlays()
                            SwiftOverlays.removeAllBlockingOverlays()
                            DatabaseController.saveContext()
                            
                            //****************todos os pedidos sincronizados
                            let title = "Campanhas"
                            let subtitle = "Lista de campanhas atualizada com sucesso"
                            let banner = NotificationBanner(title: title, subtitle: subtitle, style: .success)
                            banner.show()
                        }
                    }
                })
            }
        }
    }
    
    
    /***************************************************************************/
    
    
    
    func paginacaoCampanhas(authToken: String, pagina: Int, totalPages: Int, headers: Dictionary<String, String>) {
        
        SwiftOverlays.removeAllBlockingOverlays()
        self.removeAllOverlays()
        let text = "Verificando novas campanhas"
        SwiftOverlays.showBlockingWaitOverlayWithText(text)
        
        Alamofire.request(self.urlBase+"pedidos?token=\(authToken)&p=\(pagina)", method: .get)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    
                    
                    
                    let json = JSON(value)
                    //salvo json na memória
                    
                    for (_, resultado) in json["pedidos"] {
                        
                        let id = resultado["id"].stringValue
                        self.listaPedidos.append(id)
                        self.statusPedidos[id] = resultado["status"].stringValue
                    }
                    
                    self.numeroPaginas = self.numeroPaginas + 1
                    
                    SwiftOverlays.removeAllBlockingOverlays()
                    self.removeAllOverlays()
                    let text = "Verificando novas campanhas página \(self.numeroPaginas)/\(totalPages)"
                    SwiftOverlays.showBlockingWaitOverlayWithText(text)
                    
                    if self.numeroPaginas > totalPages {
                        self.numeroPaginas = 0
                        self.listaPedidos.removeAll()
                        self.statusPedidos.removeAll()
                        self.removeAllOverlays()
                        self.baixarPedidosOffline(authToken: authToken)
                    } else {
                        if self.numeroPaginas == totalPages {
                            
                            self.removeAllOverlays()
                            SwiftOverlays.removeAllBlockingOverlays()
                            self.percorrerJsonCampanhas(authToken: authToken, headers: headers)
                        }
                    }
                    
                case .failure(let erro):
                    
                    self.numeroPaginas = totalPages + 1
                    SwiftOverlays.removeAllBlockingOverlays()
                    self.removeAllOverlays()
                    let message = self.falhaConexao(erro, response: response)
                    self.mostrarAlerta(message, nomeBotao: "OK", style: .error)
                }
        }
    }
    

    func campanhafiltrada(pedidoId: String) -> Pedidos? {
        
        let requestPedidos  = NSFetchRequest<NSFetchRequestResult>(entityName: self.name.pedidos)
        requestPedidos.returnsObjectsAsFaults = false
        requestPedidos.predicate = NSPredicate(format: "id == %@", pedidoId)
        
        do {
            let pedidos = try(DatabaseController.getContext().fetch(requestPedidos)) as! [Pedidos]
            if pedidos.count > 0 {
                /* remover duplicatas */
                return pedidos[0]
            } else {
                return nil
            }
            
        } catch let erro {
            print("erro ao carregar \(self.name.pedidos): \(erro)")
        }
        
        return nil
    }
    
    
    
    
    
}
