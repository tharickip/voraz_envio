//
//  PedidoNovo.swift
//  agroshowiOS
//
//  Created by Lucas de Brito Reis on 14/06/17.
//  Copyright © 2017 Minerva Aplicativos LTDA ME. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import CoreData


class Name: NSObject {
    
    let campanha                = "Campanha"
    let cliente                 = "Cliente"
    let dadosLogin              = "DadosLogin"
    let itens                   = "Itens"
    let organizacao             = "Organizacao"
    let pedidos                 = "Pedidos"
    let pessoa                  = "Pessoa"
    let produto                 = "Produto"
    let proximoAvaliador        = "ProximoAvaliador"
    let responsavel             = "Responsavel"
    let usuario                 = "Usuario"
    let requisicao              = "Requisicao"
    let campanhaVendas          = "CampanhaVendas"
    let VendasContadores        = "VendasContadores"
    let VendasEspecies          = "VendasEspecies"
    let VendasForecast          = "VendasForecast"
    let VolumeCampanhas         = "VolumeCampanhas"
    let EntregasEvolucao        = "EntregasEvolucao"
    let ComparaCampanhas        = "ComparaCampanhas"
    let CampanhaSelecionada     = "CampanhaSelecionada"
    
    let cd_vendasContadoresOK   = "cd_vendasContadoresOK"
    let cd_entregasEvolucaoOK   = "cd_entregasEvolucaoOK"
    let cd_vendasEspecieOK      = "cd_vendasEspecieOK"
    let cd_vendasForecastOK     = "cd_vendasForecastOK"
    
    
}

class TipoRequisicao: NSObject {
    
    let liberar         = "liberar"
    let pedirRevisao    = "revisar"
    let bloquear        = "bloquear"
}
