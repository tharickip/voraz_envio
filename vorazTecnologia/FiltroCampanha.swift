//
//  FiltroCampanha.swift
//  vorazTecnologia
//
//  Created by Lucas de Brito Reis on 24/08/17.
//  Copyright © 2017 Minerva Aplicativos LTDA ME. All rights reserved.
//

import UIKit
import SideMenu
import Alamofire
import SwiftyJSON
import LTMorphingLabel
import CoreData
import SCLAlertView
import DGElasticPullToRefresh
import NotificationBannerSwift
import SwiftOverlays

private let reuseFiltroIdentifier = "reuseFiltroIdentifier"

class FiltroCampanha: BaseViewController, UITableViewDataSource, UITableViewDelegate {
    
    let tabela: UITableView = {
        let tableview = UITableView()
        tableview.register(UITableViewCell.self, forCellReuseIdentifier: reuseFiltroIdentifier)
        return tableview
    }()
    
    let backButton: UIButton = {
        let botao = UIButton(type: .custom)
        botao.setImage(UIImage(named: "Backicon"), for: .normal)
        botao.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        return botao
    }()
    
    let tituloView: UILabel = {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 100, height: 50))
        label.textColor = UIColor.white
        label.textAlignment = NSTextAlignment.center
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.text = "Selecionar Campanha"
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Configurando o delegate e data source da tableView
        self.tabela.dataSource = self
        self.tabela.delegate = self
        
        //Confirgurar Views
        setupViews()
        backButton.addTarget(self, action: #selector(voltarDashBoard), for: .touchUpInside)
    }
    
    func setupViews() {
        
        
        let campanhas = self.lerCampanhas()
        
        let alturaTabela: CGFloat = CGFloat(campanhas.count) * 50
        
        if alturaTabela < view.frame.height {
            self.tabela.isScrollEnabled = false
        }
        
        view.backgroundColor = UIColor.lightGray
        
        view.addSubview(self.tabela)
        tabela.snp.makeConstraints { (make) in
            make.top.left.width.equalToSuperview()
            make.height.equalTo(alturaTabela)
        }
        
        //configura a view
        self.tabela.backgroundColor = UIColor.white
        UIApplication.shared.statusBarStyle = .lightContent
        
        guard let _ = self.navigationController,
            let barraDeNavegacao = self.navigationController?.navigationBar else {
                return
        }
        
        //configuracoes da barra de navegacao
        barraDeNavegacao.isTranslucent = false
        barraDeNavegacao.barTintColor = UIColor(r: 128, g: 30, b: 72)
        barraDeNavegacao.tintColor = UIColor.white
        let menuItem = UIBarButtonItem(customView: backButton)
        //let contagemItem = UIBarButtonItem(customView: contagemPedidosView)
        //contagemItem.isEnabled = false
        
        
        self.navigationItem.setLeftBarButton(menuItem, animated: true)
        //self.navigationItem.setRightBarButton(contagemItem, animated: true)
        self.navigationItem.titleView = tituloView
    }
    
    //funcao que exibe o menu (ao clicar no hamburguer)
    func voltarDashBoard() {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Table view data source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let campanhas  = self.lerCampanhas()
        return campanhas.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseFiltroIdentifier, for: indexPath)
        
        // Configure the cell...
        let campanhas = self.lerCampanhas()
        cell.textLabel?.text = campanhas[indexPath.row].nome!
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        return 50
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200 // also UITableViewAutomaticDimension can be used
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let campanhas = self.lerCampanhas()
        
        self.clearCampanhas(entityName: self.name.CampanhaSelecionada)
        let campanhaSelecionada = NSEntityDescription.insertNewObject(forEntityName: self.name.CampanhaSelecionada, into: DatabaseController.getContext()) as! CampanhaSelecionada
        if let id  = campanhas[indexPath.row].id {
            campanhaSelecionada.idSelecionado = id
        }
        
        if let nome = campanhas[indexPath.row].nome {
            campanhaSelecionada.nome = nome
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    
    
}

