//
//  Extensions.swift
//  faleComODrRisadinha
//
//  Created by Lucas de Brito Reis on 31/03/17.
//  Copyright © 2017 MinervaAplicativos. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

extension UIColor {
    
    convenience init (r: CGFloat, g: CGFloat, b: CGFloat) {
        self.init(red: r/255, green: g/255, blue: b/255, alpha: 1)
    }
}


extension UIView {
    func addConstraintWithFormat(format: String, views: UIView...) {
        var viewsDictionary = [String:UIView]()
        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            view.translatesAutoresizingMaskIntoConstraints = false
            viewsDictionary[key] = view
        }
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutFormatOptions(), metrics: nil, views: viewsDictionary))
    }
}


extension UIImageView {
    
    func resize(newWidth: CGFloat) -> CGFloat {
        
        guard let image = self.image else {return 0}
        let proporcao = newWidth/image.size.width
        let newHeight = image.size.height * proporcao
        
        return newHeight
    }
    
    func resize(newHeight: CGFloat) -> CGFloat {
        
        guard let image = self.image else {return 0}
        let proporcao = newHeight/image.size.height
        let newWidth = image.size.width * proporcao
        
        return newWidth
    }
    
}

extension Double {
    func arredonda() -> String {
        return String(format: "% .2f", self)
    }
}

extension Double {
    func toString() -> String {
        return String(format: "%.2f",self).replacingOccurrences(of: ".", with: ",")
    }
}




