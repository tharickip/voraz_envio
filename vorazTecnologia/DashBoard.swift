//
//  DashBoardViewController.swift
//
//
//  Created by Lucas de Brito Reis on 21/06/17.

import UIKit
import SnapKit
import AIFlatSwitch
import Charts

class DashBoard: BaseViewController, UIScrollViewDelegate {
    
    let backButton: UIButton = {
        let botao = UIButton(type: .custom)
        botao.setImage(UIImage(named: "Backicon"), for: .normal)
        botao.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        return botao
    }()
    
    let tituloView: UILabel = {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 100, height: 50))
        label.textColor = UIColor.white
        label.textAlignment = NSTextAlignment.center
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.text = "DashBoard"
        return label
    }()
    
    let scrollBox: UIScrollView = {
        let view = UIScrollView()
        view.backgroundColor = UIColor(white: 0.8, alpha: 0.5)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let boxEntregue: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(r: 144, g: 49, b: 170)
        view.layer.cornerRadius = 5
        return view
    }()
    
    let entregue$: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(white: 1.0, alpha: 0.4)
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.left
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.boldSystemFont(ofSize: 30)
        label.text = "$"
        return label
    }()
    
    let entregueLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(white: 1.0, alpha: 0.6)
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.left
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.text = "Entregue"
        return label
    }()
    
    let entregue: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(white: 1.0, alpha: 0.6)
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.right
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.boldSystemFont(ofSize: 30)
        label.text = "12316731672"
        return label
    }()
    
    let boxCarteira: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(r: 67, g: 80, b: 175)
        view.layer.cornerRadius = 5
        return view
    }()
    
    let carteiraLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(white: 1.0, alpha: 0.6)
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.left
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.text = "Carteira"
        return label
    }()
    
    let carteira$: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(white: 1.0, alpha: 0.4)
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.left
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.boldSystemFont(ofSize: 30)
        label.text = "$"
        return label
    }()
    
    let carteira: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(white: 1.0, alpha: 0.6)
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.right
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.boldSystemFont(ofSize: 30)
        label.text = ""
        return label
    }()
    
    let boxTotalVendas: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(r: 66, g: 148, b: 136)
        view.layer.cornerRadius = 5
        return view
    }()
    
    let totalVendas$: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(white: 1.0, alpha: 0.4)
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.left
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.boldSystemFont(ofSize: 30)
        label.text = "$"
        return label
    }()
    
    let totalVendas: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(white: 1.0, alpha: 0.6)
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.right
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.boldSystemFont(ofSize: 30)
        label.text = ""
        return label
    }()
    
    let totalVendasLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(white: 1.0, alpha: 0.6)
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.left
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.text = "Total Vendas"
        return label
    }()
    
    let grafico1: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.cornerRadius = 5
        return view
    }()
    
    let tituloBox1: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.left
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.text = "Evolução das Campanhas"
        return label
    }()
    
    let subTituloBox1: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.left
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14)
        label.text = "Volume Entregue"
        return label
    }()
    
    let grafico2: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.cornerRadius = 5
        return view
    }()
    
    let tituloBox2: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.left
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.text = "Vendas por Espécie"
        return label
    }()
    
    let subTituloBox2: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.left
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14)
        label.text = "Valor"
        return label
    }()
    
    let grafico3: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.cornerRadius = 5
        return view
    }()
    
    let tituloBox3: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.left
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.text = "Evolução das Entregas"
        return label
    }()
    
    let subTituloBox3: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.left
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14)
        label.text = "SAFRA18TESTE"
        return label
    }()
    
    let grafico4: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.cornerRadius = 5
        return view
    }()
    
    let tituloBox4: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.left
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.text = "Evolução das Vendas x Forecast Desafio"
        return label
    }()
    
    let subTituloBox4: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.left
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14)
        label.text = "SAFRA18TESTE"
        return label
    }()
    
    let grafico5: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.cornerRadius = 5
        return view
    }()
    
    let tituloBox5: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.left
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.text = "Vendas por Espécie"
        return label
    }()
    
    let subTituloBox5: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.left
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14)
        label.text = "Volume"
        return label
    }()
    

    
    

    
    
    
    
    let boxScrollView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }()
    
    let calendario: UIButton = {
        let botao = UIButton(type: .custom)
        botao.setImage(UIImage(named: "calendario"), for: .normal)
        botao.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        return botao
    }()
    
    let tituloCampanhaSelecionada: UILabel = {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 100, height: 50))
        label.textColor = UIColor.white
        label.textAlignment = NSTextAlignment.center
        label.font = UIFont.boldSystemFont(ofSize: 16)
        //label.text = "DashBoard"
        return label
    }()
    
    let caixaTexto: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(r: 128, g: 30, b: 72)
        return view
    }()
    
    
    let horizontalVazioView: UIView = {
       let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 5
        return view
    }()
    
    
    let horizontalVazioLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.center
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14)
        label.text = "Não há dados suficientes"
        return label
    }()
    
    let horizontal2VazioView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 5
        return view
    }()
    
    
    let horizontal2VazioLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.center
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14)
        label.text = "Não há dados suficientes"
        return label
    }()
    
    let chartVazioView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 5
        return view
    }()
    
    
    let chartVazioLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.center
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14)
        label.text = "Não há dados suficientes"
        return label
    }()
    
    let chart2VazioView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 5
        return view
    }()
    
    
    let chart2VazioLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.center
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14)
        label.text = "Não há dados suficientes"
        return label
    }()
    
    
    
    //constantes layout
    var Xbox        = CGFloat()
    var YBox        = CGFloat()
    var widthBox    = CGFloat()
    var heightBox   = CGFloat()
    
    let barChartView = BarChartView()
    let chart = PieChartView()
    let chart2 = PieChartView()
    let horizontal = HorizontalBarChartView()
    let horizontal2 = HorizontalBarChartView()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollBox.delegate = self
        configCalendario()
        setupViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupChart()
    }
    
    
    
    func setupViews() {
        
        //constantes layout
        Xbox        = view.frame.height/20
        YBox        = view.frame.width/40
        widthBox    = view.frame.width-(view.frame.width/20)
        heightBox   = view.frame.width/1.1
        
        //barra de navegacao
        if let barranavegacao = self.navigationController?.navigationBar {
            barranavegacao.isTranslucent = false
            barranavegacao.barTintColor = UIColor(r: 128, g: 30, b: 72)//UIColor(r: 54, g: 142, b: 58)
            barranavegacao.tintColor = UIColor.white
            self.navigationItem.titleView = tituloView
            let backItem = UIBarButtonItem(customView: backButton)
            self.navigationItem.setLeftBarButton(backItem, animated: true)
            backButton.addTarget(self, action: #selector(back), for: .touchUpInside)
        }
        
        let calendarioItem = UIBarButtonItem(customView: calendario)
        self.navigationItem.setRightBarButton(calendarioItem, animated: true)
        self.calendario.addTarget(self, action: #selector(filtro), for: .touchUpInside)
        
        self.view.addSubview(caixaTexto)
        caixaTexto.addSubview(tituloCampanhaSelecionada)
        
        
        caixaTexto.snp.makeConstraints { (make) in
            make.top.left.width.equalToSuperview()
            make.height.equalTo(30)
        }
        
        tituloCampanhaSelecionada.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
        }

        
        view.backgroundColor = UIColor.white
        //Adiciona subviews
        view.addSubview(scrollBox)
        scrollBox.addSubview(boxScrollView)
        
        
        /*********** Valores Totais **********/
        boxScrollView.addSubview(boxEntregue)
        boxEntregue.addSubview(entregue$)
        boxEntregue.addSubview(entregue)
        boxEntregue.addSubview(entregueLabel)
        
        boxScrollView.addSubview(boxCarteira)
        boxCarteira.addSubview(carteira$)
        boxCarteira.addSubview(carteira)
        boxCarteira.addSubview(carteiraLabel)
        
        boxScrollView.addSubview(boxTotalVendas)
        boxTotalVendas.addSubview(totalVendas$)
        boxTotalVendas.addSubview(totalVendas)
        boxTotalVendas.addSubview(totalVendasLabel)
        
        
        boxScrollView.addSubview(grafico1)
        grafico1.addSubview(tituloBox1)
        grafico1.addSubview(subTituloBox1)
        
        boxScrollView.addSubview(grafico2)
        grafico2.addSubview(tituloBox2)
        grafico2.addSubview(subTituloBox2)
        
        boxScrollView.addSubview(grafico5)
        grafico5.addSubview(tituloBox5)
        grafico5.addSubview(subTituloBox5)
        
        boxScrollView.addSubview(grafico3)
        grafico3.addSubview(tituloBox3)
        grafico3.addSubview(subTituloBox3)
        
        boxScrollView.addSubview(grafico4)
        grafico4.addSubview(tituloBox4)
        grafico4.addSubview(subTituloBox4)
        

        
        
        
        self.scrollBox.delegate = self
        self.scrollBox.minimumZoomScale = 1.0
        self.scrollBox.maximumZoomScale = 2.0
        self.scrollBox.isUserInteractionEnabled = true
        
        
        scrollBox.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(30)
            make.left.width.bottom.equalToSuperview()
        }
        
        boxScrollView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        /*********** Valores Totais **********/
        boxEntregue.snp.makeConstraints { (make) in
            make.top.equalTo(10)
            make.width.equalTo(widthBox)
            make.left.equalTo(YBox)
            make.height.equalTo(70)
        }
        
        entregue$.snp.makeConstraints { (make) in
            make.top.left.equalToSuperview().offset(10)
        }
        
        entregue.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(10)
            make.right.equalToSuperview().offset(-10)
        }
        
        entregueLabel.snp.makeConstraints { (make) in
            make.top.equalTo(entregue$.snp.bottom)
            make.right.equalToSuperview().offset(-20)
        }
        
        boxCarteira.snp.makeConstraints { (make) in
            make.top.equalTo(boxEntregue.snp.bottom).offset(10)
            make.width.equalTo(widthBox)
            make.left.equalTo(boxEntregue.snp.left)
            make.height.equalTo(boxEntregue.snp.height)
        }
        
        carteira$.snp.makeConstraints { (make) in
            make.top.left.equalToSuperview().offset(10)
        }
        
        carteira.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(10)
            make.right.equalToSuperview().offset(-10)
        }
        carteiraLabel.snp.makeConstraints { (make) in
            make.top.equalTo(carteira$.snp.bottom)
            make.right.equalToSuperview().offset(-20)
        }
        
        
        boxTotalVendas.snp.makeConstraints { (make) in
            make.top.equalTo(boxCarteira.snp.bottom).offset(10)
            make.width.equalTo(widthBox)
            make.left.equalTo(boxEntregue.snp.left)
            make.height.equalTo(boxEntregue.snp.height)
        }
        
        totalVendas$.snp.makeConstraints { (make) in
            make.top.left.equalToSuperview().offset(10)
        }
        
        
        totalVendas.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(10)
            make.right.equalToSuperview().offset(-10)
        }
        
        totalVendasLabel.snp.makeConstraints { (make) in
            make.top.equalTo(totalVendas$.snp.bottom)
            make.right.equalToSuperview().offset(-20)
        }
        
        
        /*********** box1 **********/
        grafico1.snp.makeConstraints { (make) in
            make.top.equalTo(boxTotalVendas.snp.bottom).offset(10)
            //make.top.equalToSuperview().offset(Xbox)
            make.width.equalTo(widthBox)
            make.height.equalTo(heightBox)
            make.left.equalTo(YBox)
        }
        
        tituloBox1.snp.makeConstraints { (make) in
            make.top.left.equalTo(10)
            make.width.equalTo(widthBox-20)
        }
        
        subTituloBox1.snp.makeConstraints { (make) in
            make.top.equalTo(tituloBox1.snp.bottom)
            make.left.equalTo(tituloBox1.snp.left)
            make.width.equalTo(widthBox-20)
        }
        
        /*********** box2 **********/
        grafico2.snp.makeConstraints{ (make) in
            make.top.equalTo(grafico1.snp.bottom).offset(YBox)
            make.width.height.left.equalTo(grafico1)
        }
        
        tituloBox2.snp.makeConstraints { (make) in
            make.top.left.equalTo(10)
            make.width.equalTo(widthBox-20)
        }
        
        subTituloBox2.snp.makeConstraints { (make) in
            make.top.equalTo(tituloBox2.snp.bottom)
            make.left.equalTo(tituloBox2.snp.left)
            make.width.equalTo(widthBox-20)
        }
        
        
        
        grafico5.snp.makeConstraints{ (make) in
            make.top.equalTo(grafico2.snp.bottom).offset(YBox)
            make.width.height.left.equalTo(grafico1)
        }
        
        tituloBox5.snp.makeConstraints { (make) in
            make.top.left.equalTo(10)
            make.width.equalTo(widthBox-20)
        }
        
        subTituloBox5.snp.makeConstraints { (make) in
            make.top.equalTo(tituloBox5.snp.bottom)
            make.left.equalTo(tituloBox5.snp.left)
            make.width.equalTo(widthBox-20)
        }
        
        
        /*********** box3 **********/
        grafico3.snp.makeConstraints{ (make) in
            make.top.equalTo(grafico5.snp.bottom).offset(YBox)
            make.width.left.equalTo(grafico1)
            make.height.equalTo(view.frame.width)
        }
        
        tituloBox3.snp.makeConstraints { (make) in
            make.top.left.equalTo(10)
            make.width.equalTo(widthBox-20)
        }
        
        subTituloBox3.snp.makeConstraints { (make) in
            make.top.equalTo(tituloBox3.snp.bottom)
            make.left.equalTo(tituloBox3.snp.left)
            make.width.equalTo(widthBox-20)
        }
        
        /*********** box4 **********/
        grafico4.snp.makeConstraints{ (make) in
            make.top.equalTo(grafico3.snp.bottom).offset(YBox)
            make.width.left.equalTo(grafico1)
            make.bottom.equalToSuperview().offset(-30)
            make.height.equalTo(view.frame.width)
        }
        
        tituloBox4.snp.makeConstraints { (make) in
            make.top.left.equalTo(10)
            make.width.equalTo(widthBox-20)
        }
        
        subTituloBox4.snp.makeConstraints { (make) in
            make.top.equalTo(tituloBox4.snp.bottom)
            make.left.equalTo(tituloBox4.snp.left)
            make.width.equalTo(widthBox-20)
        }
        
    }
    
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.boxScrollView
    }
    
    
    func back() {
        //self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
}
