//
//  Feed.swift
//  agroshow
//
//  Created by Lucas de Brito Reis on 17/05/17.
//  Copyright © 2017 MinervaAplicativos. All rights reserved.
//

import UIKit
import SideMenu
import Alamofire
import SwiftyJSON
import LTMorphingLabel
import CoreData
import SCLAlertView
import DGElasticPullToRefresh
import NotificationBannerSwift
import SwiftOverlays

private let reuseIdentifier = "reuseIdentifier"

class Feed: BaseViewController, UITableViewDataSource, UITableViewDelegate {
    
    let tabela: UITableView = {
        let tableview = UITableView()
        tableview.register(FeedCell.self, forCellReuseIdentifier: reuseIdentifier)
        return tableview
    }()
    
    let menuBotao: UIButton = {
        let botao = UIButton(type: .custom)
        botao.setImage(UIImage(named: "Hamburguer"), for: .normal)
        botao.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        return botao
    }()
    
    //    let contagemPedidosView: UILabel = {
    //        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 100, height: 50))
    //        label.textColor = UIColor.white
    //        label.textAlignment = NSTextAlignment.center
    //        label.font = UIFont.boldSystemFont(ofSize: 18)
    //        label.text = ""
    //        return label
    //    }()
    
    let tituloView: UILabel = {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 100, height: 50))
        label.textColor = UIColor.white
        label.textAlignment = NSTextAlignment.center
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.text = "Pedidos"
        return label
    }()
    
    /******** Inicio (Variáveis) - Protocolo UILoadRequisicao ********/
    let telaFade: UIView = {
        let efeito = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let view = UIVisualEffectView(effect: efeito)
        view.backgroundColor = UIColor(white: 0.1, alpha: 0.1)
        view.layer.masksToBounds = true
        return view
    }()
    
    let box: UIView = {
        let efeito = UIBlurEffect(style: UIBlurEffectStyle.regular)
        let view = UIVisualEffectView(effect: efeito)
        view.backgroundColor = UIColor(white: 0.9, alpha: 0.8)
        view.layer.cornerRadius = 10
        view.layer.masksToBounds = true
        return view
    }()
    
    let statusDownload: LTMorphingLabel = {
        let label = LTMorphingLabel(frame: CGRect(x: 0, y: 0, width: 100, height: 50))
        label.morphingEffect = LTMorphingEffect.scale
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.center
        label.font = UIFont.systemFont(ofSize: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.black
        return label
    }()
    
    let statusLoad: UIView = {
        let view = UIView()
        return view
    }()
    
    //    let activityIndicatorView = NVActivityIndicatorView(frame: CGRect.zero, type: .lineScale, color: UIColor(r: 54, g: 142, b: 58), padding: CGFloat(0))
    //
    
    var carregandoDados = false
    
    
    let spinner: UIActivityIndicatorView = {
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        spinner.startAnimating()
        //spinner.frame = CGRect(x: 0, y: 0, width: self.tabela.frame.width, height: 44)
        return spinner
    }()
    
    var ePrimeiroLogin = true
    
    /******** Fim (Variáveis) - Protocolo UILoadRequisicao ********/
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Configurando o delegate e data source da tableView
        self.tabela.dataSource = self
        self.tabela.delegate = self
        
        
        //self.lerBancoDeDados()
        
        //Confirgurar Views
        setupViews()
        menuBotao.addTarget(self, action: #selector(mostrarMenu), for: .touchUpInside)
        //atualizarBotao.addTarget(self, action: #selector(sincroniza), for: .touchUpInside)
        setupPullToRefresh()
        //waitScroll()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabela.reloadData()
    }
    
    deinit {
        self.tabela.dg_removePullToRefresh()
    }
    
    func setupPullToRefresh(){
        // Initialize tableView
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = UIColor(red: 78/255.0, green: 221/255.0, blue: 200/255.0, alpha: 1.0)
        self.tabela.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            // Add your logic here
            
            self?.sincroniza()
            // Do not forget to call dg_stopLoading() at the end
            self?.tabela.dg_stopLoading()
            }, loadingView: loadingView)
        self.tabela.dg_setPullToRefreshFillColor(UIColor(red: 57/255.0, green: 67/255.0, blue: 89/255.0, alpha: 1.0))
        self.tabela.dg_setPullToRefreshBackgroundColor(self.tabela.backgroundColor!)
    }
    
    
    
    
    func setupViews() {
        
        view.addSubview(self.tabela)
        tabela.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        //configura a view
        self.tabela.backgroundColor = UIColor.white
        UIApplication.shared.statusBarStyle = .lightContent
        
        guard let navigationController = self.navigationController,
            let barraDeNavegacao = self.navigationController?.navigationBar else {
                return
        }
        
        //configura o menu
        let menu = Menu()
        
        let dadosLogin = self.lerBancoDeDados(self.name.dadosLogin) as! [DadosLogin]
        
        if dadosLogin.count > 0 {
            menu.emailUsuario.text = dadosLogin[0].usuario?.email
            
            if let nomeUsuario = dadosLogin[0].usuario?.nome {
                menu.nomeUsuario.text = nomeUsuario
                if nomeUsuario.characters.count > 0 {
                    menu.letra.text = String(nomeUsuario.characters.prefix(1))
                }
            }
            
        }
        
        //efeito de SideMenu (menu hamburguer)
        let sideMenu = UISideMenuNavigationController(rootViewController: menu)
        sideMenu.leftSide = true
        SideMenuManager.menuLeftNavigationController = sideMenu
        SideMenuManager.menuAddPanGestureToPresent(toView: barraDeNavegacao)
        SideMenuManager.menuAddScreenEdgePanGesturesToPresent(toView: navigationController.view)
        SideMenuManager.menuFadeStatusBar = false
        
        //configuracoes da barra de navegacao
        barraDeNavegacao.isTranslucent = false
        barraDeNavegacao.barTintColor = UIColor(r: 128, g: 30, b: 72) //UIColor(r: 54, g: 142, b: 58)
        barraDeNavegacao.tintColor = UIColor.white
        let menuItem = UIBarButtonItem(customView: menuBotao)
        //let contagemItem = UIBarButtonItem(customView: contagemPedidosView)
        //contagemItem.isEnabled = false
        
        
        self.navigationItem.setLeftBarButton(menuItem, animated: true)
        //self.navigationItem.setRightBarButton(contagemItem, animated: true)
        self.navigationItem.titleView = tituloView
    }
    
    func atualizarDados() {
        
        self.tabela.reloadData()
        let titulo = "Sincronizar"
        let descricao = "Funcionalidade em desenvolvimento"
        
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false,
            showCircularIcon: true
            //contentViewColor: UIColor(r: 54, g: 142, b: 58)
        )
        
        let alert = SCLAlertView(appearance: appearance)
        alert.addButton("OK") {
        }
        alert.showSuccess(titulo, subTitle: descricao)
        
        //self.baixarPedidos(authToken: self.dadosLogin.authToken, headers: [:])
    }
    
    //funcao que exibe o menu (ao clicar no hamburguer)
    func mostrarMenu() {
        
        present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    // MARK: - Table view data source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //let pedidos = lerBancoDeDados(self.name.pedidos)
        let pedidos = lerPedidos()
        
        return pedidos.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier) as! FeedCell
        
        // Configure the cell...
        //let pedidos = lerBancoDeDados(self.name.pedidos) as! [Pedidos]
        let pedidos = lerPedidos()
        
        if indexPath.row < pedidos.count {
            cell.config(pedido: pedidos[indexPath.row])
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200 // also UITableViewAutomaticDimension can be used
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        //let pedidos = self.lerBancoDeDados(self.name.pedidos) as! [Pedidos]
        let pedidos = lerPedidos()
        
        let detail = Detail()
        
        detail.indexPathPedido = indexPath
        detail.pedidoId = pedidos[indexPath.row].id
        
        present(UINavigationController(rootViewController: detail), animated: true, completion: nil)
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    func notificacaoFeed(_ tipoRequisicao: String,  indexPath: IndexPath) {
        
        self.removeAllOverlays()
        SwiftOverlays.removeAllBlockingOverlays()
        
        //let pedidos = lerBancoDeDados(self.name.pedidos) as! [Pedidos]
        let pedidos = lerPedidos()
        
        if indexPath.row < pedidos.count {
            
            if pedidos[indexPath.row].cd_isSync {
                
                if pedidos[indexPath.row].cd_requisicaoDeuCerto {
                    
                    var title: String
                    var subtitle: String
                    
                    switch tipoRequisicao {
                    case self.tipoRequisicao.liberar:
                        title  = "Liberar"
                        subtitle = "Pedido Liberado"
                    case self.tipoRequisicao.pedirRevisao:
                        title  = "Enviar para revisão"
                        subtitle = "Pedido enviado para revisão"
                    case self.tipoRequisicao.bloquear:
                        title  = "Bloquear"
                        subtitle = "Pedido bloqueado"
                    default:
                        title  = ""
                        subtitle = ""
                    }
                    
                    let banner = NotificationBanner(title: title, subtitle: subtitle, style: .success)
                    banner.show()
                    
                }
            }
        }
        self.tabela.reloadData()
        
        
    }
    
    //Impede que o usuário possa tratar um pedido com status bloqueado
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        //let pedidos = lerBancoDeDados(self.name.pedidos) as! [Pedidos]
        let pedidos = lerPedidos()
        
        if indexPath.row < pedidos.count  {
            
            if pedidos[indexPath.row].status == "BLOQUEADO" {
                return false
            }
        }
        return true
    }
    
    
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        //let pedidos = lerBancoDeDados(self.name.pedidos) as! [Pedidos]
        let pedidos = lerPedidos()
        let dadosLogin = lerBancoDeDados(self.name.dadosLogin) as! [DadosLogin]
        
        let liberar = UITableViewRowAction(style: .normal, title: "Liberar") { (action, indexPath) in
            
            if pedidos.count > 0 {
                
                if indexPath.row < pedidos.count {
                
                let descricao = ""
                self.preencherObservacao("Liberar", descricao: descricao, placeHolder: "justificativa") { (observacao) in
                    
                    self.liberar(self.urlBase, id: pedidos[indexPath.row].id!, indexPath: indexPath, authToken: (dadosLogin[0].authToken)!, observacao: observacao, {_ in
                        
                        self.notificacaoFeed(self.tipoRequisicao.liberar, indexPath: indexPath)
                    })
                }
            }
            }
            
        }
        
        let revisao = UITableViewRowAction(style: .normal, title: "Revisão") { (action, indexPath) in
            
            if pedidos.count > 0 {
                if indexPath.row < pedidos.count {
                    
                    let descricao = ""
                    self.preencherObservacao("Enviar para Revisão", descricao: descricao, placeHolder: "observação") { (observacao) in
                        
                        self.pedirRevisao(self.urlBase, id: pedidos[indexPath.row].id!, indexPath: indexPath, authToken: (dadosLogin[0].authToken)!, observacao: observacao , {_ in
                            
                            self.notificacaoFeed(self.tipoRequisicao.pedirRevisao, indexPath: indexPath)
                        })
                        
                    }
                }
            }
        }
        
        let bloquear = UITableViewRowAction(style: .normal, title: "Bloquear") { (action, indexPath) in
            
            if pedidos.count > 0 {
                if indexPath.row < pedidos.count {
                    
                    let descricao = ""
                    
                    self.preencherObservacao("Bloquear", descricao: descricao, placeHolder: "observação") { (observacao) in
                        
                        self.motivoBloqueio({ (motivoBloqueioId) in
                            
                            
                            self.bloquear(self.urlBase, id: pedidos[indexPath.row].id!, indexPath: indexPath, authToken: (dadosLogin[0].authToken)!, observacao: observacao, motivoBloqueioId: motivoBloqueioId, {_ in
                                
                                self.notificacaoFeed(self.tipoRequisicao.bloquear, indexPath: indexPath)
                            })
                            
                        })}
                    
                }
            }
            
        }
        
        liberar.backgroundColor     = UIColor(r: 105, g: 191, b: 105)
        revisao.backgroundColor     = UIColor(r: 178, g: 87, b: 9)
        bloquear.backgroundColor    = UIColor(r: 178, g:   0, b:   0)
        
        return [bloquear, revisao, liberar]
    }
    
    
    
    override func baixarComparaCampanhas(_ urlBase:String, authToken token: String, headers: Dictionary<String, String>, _ completionHandler: @escaping () -> Void) {
        
        
        let text = "Baixando Comparação entre Campanhas"
        SwiftOverlays.removeAllBlockingOverlays()
        self.removeAllOverlays()
        SwiftOverlays.showBlockingWaitOverlayWithText(text)
        
        
        Alamofire.request(urlBase+"dashboard/widgets/campanhas-evolucao", method: .get, headers: headers)
            .validate()
            .responseJSON { (response) in
                switch response.result {
                case .success(let valor):
                    
                    completionHandler()
                    
                    let json = JSON(valor)
                    
                    self.clearComparaCampanhas()
                    for (_, resultado) in json["campanhas"] {
                        self.atribuiComparaCampanhas(resultado)
                    }
                    
                    SwiftOverlays.removeAllBlockingOverlays()
                    self.removeAllOverlays()
                    
                    
                    
                    completionHandler()
                    
                    
                case .failure(let erro):
                    self.desempilhaErros(erro: erro, response: response){}
                }
        }
    }
    
    override func baixarCampanhasOffline(authToken: String) {
        
        
        //print("override func baixarCampanhasOffline(authToken: String)")
        
        
        super.baixarCampanhasOffline(authToken: authToken)
        self.tabela.reloadData()
    }
    
    
}
