//
//  ExtensionPedido.swift
//  agroshowiOS
//
//  Created by Lucas de Brito Reis on 03/07/17.
//  Copyright © 2017 Minerva Aplicativos LTDA ME. All rights reserved.
//
import UIKit
import CoreData
import Alamofire
import SwiftyJSON
import SCLAlertView
import SwiftOverlays
import NotificationBannerSwift

extension BaseViewController {
    
    func liberar(_ urlBase: String, id: String, indexPath: IndexPath, authToken: String, observacao: String,  _ completion: (@escaping () -> Void)){
        
        let urlRequisicao = urlBase + "pedidos/\(id)/desbloquear"
        
        //let observacao = "observacao teste"
        let headers = ["X-AUTH-TOKEN": "\(authToken)", "observacoes": observacao]
        
        SwiftOverlays.removeAllBlockingOverlays()
        let text = "Carregando..."
        SwiftOverlays.showBlockingWaitOverlayWithText(text)
        
        
        Alamofire.request(urlRequisicao, method: .post, headers: headers)
            .responseData { (response) in
                
                
                switch response.result {
                case .success(let valor):
                    
                    let mensagem = self.retornaMensagem(JSON(valor))
                    
                    if mensagem != "" {
                        
                        if let pedido = self.pedidofiltrado(pedidoId: id) {
                            pedido.cd_requisicaoDeuCerto = false
                            pedido.cd_mensagemErro = mensagem
                            DatabaseController.saveContext()
                        }
                        
                        
                        self.mostrarAlerta(mensagem, nomeBotao: "OK", style: .error)
                        
                    } else {
                        
                        self.getPedido(urlBase, id: id, authToken: authToken, headers: headers, { _ in
                            completion()
                        })
                        
                        
                        
                    }
                case .failure:
                    
                    self.offline(response.response, {
                        
                        self.salvaRequisicaoliberar(urlBase, id: id, indexPath: indexPath, observacao: observacao, authToken: authToken, {
                            
                            self.mostrarAlerta("Requisição Salva", nomeBotao: "OK", style: .success)
                            completion()
                        })
                    })
                    
                }
        }
    }
    
    
    func pedirRevisao(_ urlBase: String, id: String, indexPath: IndexPath, authToken: String, observacao: String,  _ completionHandler: (@escaping (_ parametro: Any) -> Void)){
        
        
        
        let parametros = ["revisao": ["observacao": observacao, "pedido": [ "id": id ]]]
        let urlRequisicao = urlBase + "pedidos/\(id)/revisao"
        let headersRequisicao = ["X-AUTH-TOKEN": "\(authToken)", "observacoes": observacao]
        let headers = ["X-AUTH-TOKEN": "\(authToken)"]
        
        SwiftOverlays.removeAllBlockingOverlays()
        let text = "Carregando..."
        SwiftOverlays.showBlockingWaitOverlayWithText(text)
        
        
        Alamofire.request(urlRequisicao, method: .post, parameters: parametros, encoding: JSONEncoding.default, headers: headersRequisicao)
            .responseJSON { (response) in
                
                switch response.result {
                case .success(let valor):
                    
                    let mensagem = self.retornaMensagem(JSON(valor))
                    
                    if mensagem != "" {
                        if let pedido = self.pedidofiltrado(pedidoId: id) {
                            pedido.cd_requisicaoDeuCerto = false
                            pedido.cd_mensagemErro = mensagem
                            DatabaseController.saveContext()
                        }
                        self.mostrarAlerta(mensagem, nomeBotao: "OK", style: .error)
                    } else {
                        
                        self.getPedido(urlBase, id: id, authToken: authToken, headers: headers, { _ in
                            completionHandler(Any.self)
                        })
                        
                    }
                case .failure:

                    self.offline(response.response, {
                        
                        self.salvaRequisicaoPedirRevisao(urlBase, id: id, indexPath: indexPath, observacao: observacao, authToken: authToken, {
                            self.mostrarAlerta("Requisição Salva", nomeBotao: "OK", style: .success)
                            completionHandler(Any.self)
                        })
                    })
                }
        }
        
    }
    
    func bloquear(_ urlBase: String, id: String, indexPath: IndexPath, authToken: String, observacao: String, motivoBloqueioId: Int, _ completionHandler: (@escaping (_ parametro: Any) -> Void)) {
        
        
        
        let urlRequisicao = urlBase + "pedidos/\(id)/bloquear"
        let headers: HTTPHeaders = [
            "X-AUTH-TOKEN": "\(authToken)",
            "Content-Type": "application/json"
        ]
        
        let parametros = [
            "params": [
                "motivoBloqueioId": motivoBloqueioId,
                "observacao": "\(observacao)"
            ]
        ]
        SwiftOverlays.removeAllBlockingOverlays()
        let text = "Carregando..."
        SwiftOverlays.showBlockingWaitOverlayWithText(text)
        
        
        Alamofire.request(urlRequisicao, method: .post, parameters: parametros, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { response in
                
                if let statusCode = response.response?.statusCode {
                    if statusCode == 200 {
                        
                        self.getPedido(urlBase, id: id, authToken: authToken, headers: headers, { _ in
                            completionHandler(Any.self)
                        })
                    } else {
                        switch response.result {
                        case .success(let valor):
                            
                            var mensagem = self.retornaMensagem(JSON(valor))
                            
                            if mensagem == "" {
                                mensagem = "Erro não identificado"
                            }
                            
                            if let pedido = self.pedidofiltrado(pedidoId: id) {
                                pedido.cd_requisicaoDeuCerto = false
                                pedido.cd_mensagemErro = mensagem
                                DatabaseController.saveContext()
                            }
                            //self.mostrarAlerta(mensagem)
                            self.mostrarAlerta(mensagem, nomeBotao: "OK", style: .error)
                            completionHandler(Any.self)
                            
                        case .failure:
                            self.offline(response.response, {
                                self.salvaRequisicaobloquear(urlBase, id: id, indexPath: indexPath, observacao: observacao, motivoBloqueioId: motivoBloqueioId, authToken: authToken, {
                                    self.mostrarAlerta("Requisição Salva", nomeBotao: "OK", style: .success)
                                    completionHandler(Any.self)
                                })
                            })
                            
                            
                        }
                    }
                } else {
                    self.offline(response.response, {
                        self.salvaRequisicaobloquear(urlBase, id: id, indexPath: indexPath, observacao: observacao, motivoBloqueioId: motivoBloqueioId, authToken: authToken, {
                            self.mostrarAlerta("Requisição Salva", nomeBotao: "OK", style: .success)
                            completionHandler(Any.self)
                        })
                    })
                    
                    
                    
                }
                
                
                
                
        }
    }
    
    
    func corStatus(_ status: String) -> UIColor {
        switch status {
        case "PENDENTE":                return UIColor(r: 255, g: 118, b:   0)
        case "A_REVISAR":               return UIColor(r: 178, g:  87, b:   9)
        case "AGUARDANDO_LIBERACAO":    return UIColor(r:  47, g:   9, b: 178)
        case "BLOQUEADO":               return UIColor(r: 178, g:   0, b:   0)
        case "FATURADO_PARCIAL":        return UIColor(r:  20, g: 133, b: 204)
        case "A_FATURAR":               return UIColor(r: 204, g: 105, b:  20)
        case "AGUARDAR_FATURAMENTO":    return UIColor(r: 255, g: 215, b:   0)
        case "FATURADO":                return UIColor(r:  17, g: 178, b:  25)
        case "LIBERADO":                return UIColor(r:   0, g: 255, b:  13)
        case "REGISTRADO":              return .lightGray
        default:                        return UIColor(r: 198, g: 198, b: 198)
        }
    }
    
    func getPedido(_ urlBase:String, id: String, authToken token: String, headers: Dictionary<String, String>, _ completionHandler: @escaping () -> Void) {
        

        Alamofire.request(urlBase+"pedidos/\(id)", method: .get, headers: headers)
            .validate()
            .responseJSON { (response) in
                switch response.result {
                case .success(let valor):
                    let json  = JSON(valor)
                    
                    
                    let mensagem = self.retornaMensagem(JSON(valor))
                    
                    if mensagem != "" {
                        //SwiftOverlays.removeAllBlockingOverlays()
                        //self.removeAllOverlays()
                        //self.mostrarAlerta(mensagem)
                        self.mostrarAlerta(mensagem, nomeBotao: "OK", style: .error)
                    } else {

                        if self.pedidofiltrado(pedidoId: id) != nil {
                            self.atualizaPedido(pedidoId: id, json: json["pedido"])
                            completionHandler()
                            self.removeAllOverlays()
                        } else {
                            self.atribuiPedido(json["pedido"])
                            
                            DatabaseController.saveContext()
                            completionHandler()
                            self.removeAllOverlays()
                        }
                    }
                case .failure(let erro):
                    
                    self.desempilhaErros(erro: erro, response: response){}
                }
        }
    }
    
    func preencherObservacao(_ titulo: String, descricao: String, placeHolder: String, _ completion: (@escaping (_ texto: String) -> Void)) {
        
        
        
        // Add a text field
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false,
            showCircularIcon: true
            //contentViewColor: UIColor(r: 54, g: 142, b: 58)
        )
        
        let alert = SCLAlertView(appearance: appearance)
        let textoPreenchido = alert.addTextField(placeHolder)
        textoPreenchido.spellCheckingType = .no
        textoPreenchido.autocorrectionType = .no
        
        alert.addButton("Enviar") {
            if let textoCampo = textoPreenchido.text {
                if textoCampo != "" {
                    completion(textoCampo)
                } else {
                    let aviso = "O campo não pode ser vazio"
                    self.preencherObservacao(titulo, descricao: aviso, placeHolder: placeHolder, { (digitado) in
                        completion(digitado)
                    })
                }
                
            }
        }
        alert.addButton("Cancelar") {
        }
        alert.showInfo(titulo, subTitle: descricao)
        //alert.showSuccess(titulo, subTitle: descricao)
        
        
        
    }
    
    func motivoBloqueio(_ completion: (@escaping (_ motivoBloqueioId: Int) -> Void)) {
        
        let titulo      = "Motivo do Bloqueio"
        let descricao   = "Selecione o motivo do bloqueio"
        
        // Add a text field
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false,
            showCircularIcon: true
            //contentViewColor: UIColor(r: 54, g: 142, b: 58)
        )
        
        
        //Fazer isso com um GET
        
        
        
        let alert = SCLAlertView(appearance: appearance)
        alert.addButton("COTA") {
            let motivoBloqueioId = 5
            completion(motivoBloqueioId)
        }
        alert.addButton("DESCONTO") {
            let motivoBloqueioId = 1
            completion(motivoBloqueioId)
        }
        alert.addButton("Cancelar") {
        }
        //alert.showSuccess(titulo, subTitle: descricao)
        alert.showError(titulo, subTitle: descricao)
        
    }

    
    func mostrarAlerta(_ mensagem: String, title: String, nomeBotao: String, style: SCLAlertViewStyle, completion: @escaping () -> Void) {
        
        self.removeAllOverlays()
        SwiftOverlays.removeAllBlockingOverlays()
        
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false,
            showCircularIcon: true
            //contentViewColor: UIColor(r: 54, g: 142, b: 58)
        )
        
        let alert = SCLAlertView(appearance: appearance)
        alert.addButton(nomeBotao) {
            completion()
        }
        
        switch style {
        case .success:
            alert.showSuccess(title, subTitle: mensagem)
        case .error:
            
            if !self.estaEmpilhando {
                alert.showError(title, subTitle: mensagem)
                self.estaEmpilhando = true
            }
            
        default:
            if !self.estaEmpilhando {
                alert.showError(title, subTitle: mensagem)
                self.estaEmpilhando = true
            }

        }
    }
    
    
    func mostrarAlerta(_ mensagem: String, nomeBotao: String, style: SCLAlertViewStyle) {
        
        
        
        self.removeAllOverlays()
        SwiftOverlays.removeAllBlockingOverlays()
        
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false,
            showCircularIcon: true
            //contentViewColor: UIColor(r: 54, g: 142, b: 58)
        )
        
        let alert = SCLAlertView(appearance: appearance)
        alert.addButton(nomeBotao) {
            
        }
        
        switch style {
        case .success:
            alert.showSuccess("", subTitle: mensagem)
        case .error:
            
            if !self.estaEmpilhando {
                alert.showError("Erro", subTitle: mensagem)
                self.estaEmpilhando = true
            }
        default:
            if !self.estaEmpilhando {
                alert.showError("Erro", subTitle: mensagem)
                self.estaEmpilhando = true
            }
        }
    }
    
    func baixarPedidosOffline(authToken: String) {
        
        self.listaPedidos.removeAll()
        self.statusPedidos.removeAll()
        self.numeroPaginas = 0
        self.estaEmpilhando = false
        
        
        let text = "Baixando lista de pedidos"
        SwiftOverlays.removeAllBlockingOverlays()
        self.removeAllOverlays()
        SwiftOverlays.showBlockingWaitOverlayWithText(text)
        
        let headers = ["X-AUTH-TOKEN": "\(authToken)"]
        //self.statusDownload.text = "Baixando lista de pedidos"
        Alamofire.request(self.urlBase+"pedidos", method: .get, headers: headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    
                    let json = JSON(value)
                    let totalPages = json["meta"]["totalPages"].intValue
                    
                    if totalPages > 0 {
                        
                        for pagina in 1...totalPages {
                            self.paginacao(authToken: authToken, pagina: pagina, totalPages: totalPages, headers: headers)
                        }
                    }
                    
                case .failure(let erro):
                    
                    self.desempilhaErros(erro: erro, response: response){}
                    
                }
        }
    }
    
    
    func percorrerJsonPedidosOffline(authToken: String, headers: Dictionary<String, String>) {
        
        let text = "Baixando detalhes dos novos pedidos"
        SwiftOverlays.removeAllBlockingOverlays()
        self.removeAllOverlays()
        SwiftOverlays.showBlockingWaitOverlayWithText(text)

        var idNotSync: [String] = []
        
        if self.listaPedidos.count > 0 {
            
            for idTemp in self.listaPedidos {
                
                let pedidoTemp = self.pedidofiltrado(pedidoId: idTemp)
                if pedidoTemp == nil {
                    //nao está listado no banco de dados
                    idNotSync.append(idTemp)
                } else {
                    //esta no banco de dados
                    //verifico se o status mudou
                    if pedidoTemp?.status! != self.statusPedidos[idTemp] {
                        idNotSync.append(idTemp)
                    }
                }
            }
        }
        
        //**********************
        //faco um laco que coloca true pra status que nao mudaram
        
        if idNotSync.count == 0 {
            //SwiftOverlays.removeAllBlockingOverlays()
            //self.removeAllOverlays()
            
            //limpa a memoria
            self.listaPedidos.removeAll()
            self.statusPedidos.removeAll()
            self.numeroPaginas = 0
            
            self.baixarCampanhasOffline(authToken: authToken)
        } else {
            
            //leio somente os notsync
            for id in idNotSync {
                
                //atualizo os status
                
                self.getPedido(self.urlBase, id: id, authToken: authToken, headers: headers, {_ in
                    
                    //let pedidos = self.lerBancoDeDados(self.name.pedidos)
                    let pedidos = self.lerNotSync(isSync: true)
                    
                    SwiftOverlays.removeAllBlockingOverlays()
                    self.removeAllOverlays()
                    let text = "Atualizando pedidos: \(pedidos.count)/\(self.listaPedidos.count)"
                    SwiftOverlays.showBlockingWaitOverlayWithText(text)
                    
                    //self.statusDownload.text = "Baixando pedidos \(pedidos.count)/\(self.listaPedidos.count)"
                    DatabaseController.saveContext()
                    
                    if pedidos.count > self.listaPedidos.count {
                        //self.statusDownload.text = "Limpando banco de dados"
                        self.clearContext()
                        DatabaseController.saveContext()
                        //self.baixarPedidosOffline(authToken: authToken)
                        
                    } else {
                        
                        if pedidos.count == self.listaPedidos.count {
                            
                            self.baixarCampanhasOffline(authToken: authToken)
                            
                            
                            //limpa a memoria
                            self.listaPedidos.removeAll()
                            self.statusPedidos.removeAll()
                            self.numeroPaginas = 0
                            
                            self.removeAllOverlays()
                            SwiftOverlays.removeAllBlockingOverlays()
                            DatabaseController.saveContext()

                        }
                    }
                })
            }
        }
    }
    
    /***************************************************************************/
    
    
    
    func paginacao(authToken: String, pagina: Int, totalPages: Int, headers: Dictionary<String, String>) {
        
        SwiftOverlays.removeAllBlockingOverlays()
        self.removeAllOverlays()
        let text = "Verificando novos pedidos"
        SwiftOverlays.showBlockingWaitOverlayWithText(text)
        
        Alamofire.request(self.urlBase+"pedidos?token=\(authToken)&p=\(pagina)", method: .get)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success(let value):

                    let json = JSON(value)
                    for (_, resultado) in json["pedidos"] {
                        
                        let id = resultado["id"].stringValue
                        self.listaPedidos.append(id)
                        self.statusPedidos[id] = resultado["status"].stringValue
                    }
                    
                    self.numeroPaginas = self.numeroPaginas + 1
                    
                    SwiftOverlays.removeAllBlockingOverlays()
                    self.removeAllOverlays()
                    let text = "Verificando novos pedidos página \(self.numeroPaginas)/\(totalPages)"
                    SwiftOverlays.showBlockingWaitOverlayWithText(text)
                    
                    if self.numeroPaginas > totalPages {
                        self.numeroPaginas = 0
                        self.listaPedidos.removeAll()
                        self.statusPedidos.removeAll()
                        //self.removeAllOverlays()
                        self.baixarPedidosOffline(authToken: authToken)
                    } else {
                        
                        if self.numeroPaginas == totalPages {
                            
                            //self.removeAllOverlays()
                            //SwiftOverlays.removeAllBlockingOverlays()
                            self.percorrerJsonPedidosOffline(authToken: authToken, headers: headers)
                        }
                    }
                    
                case .failure(let erro):
                    self.desempilhaErros(erro: erro, response: response){
                        self.numeroPaginas = totalPages + 1
                    }
                }
        }
    }
    
    
}
