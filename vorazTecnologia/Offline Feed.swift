//
//  Offline Feed.swift
//  vorazTecnologia
//
//  Created by Lucas de Brito Reis on 05/08/17.
//  Copyright © 2017 Minerva Aplicativos LTDA ME. All rights reserved.
//


import UIKit
import SideMenu
import Alamofire
import SwiftyJSON
import LTMorphingLabel
import CoreData
import SCLAlertView
import DGElasticPullToRefresh
import SwiftOverlays
import NotificationBannerSwift

extension Feed {
    
    func sincroniza() {
        
        SwiftOverlays.removeAllBlockingOverlays()
        
        let text = "Verificando solicitações pendentes"
        SwiftOverlays.showBlockingWaitOverlayWithText(text)
        self.estaEmpilhando = false
        
        let dadosLogin = self.lerBancoDeDados(self.name.dadosLogin) as! [DadosLogin]
        let pedidosNotSync = self.lerNotSync(isSync: false)
        
        if dadosLogin.count > 0 {
            
            
            if pedidosNotSync.count > 0 {
                
                var contador = 0
                
                for pedido in pedidosNotSync {
                    
                    guard let observacao = pedido.cd_observacao,
                        let _motivoBloqueioId = pedido.cd_motivoBloqueioId,
                        let motivoBloqueioId = Int(_motivoBloqueioId)
                        else {
                            
                            SwiftOverlays.removeAllBlockingOverlays()
                            
                            return }
                    
                    //para cada pedido eu vefico o tipo de requisicao e entao faço a requisição
                    //como que eu guardo sucesso
                    let indexPath = IndexPath(row: Int(pedido.cd_indexPath), section: 0)
                    
                    
                    if let tipoRequisicao = pedido.cd_tipoRequisicao {
                        switch tipoRequisicao {
                        case self.tipoRequisicao.liberar:
                            
                            
                            self.liberarOffline(self.urlBase, id: pedido.id!, indexPath: indexPath, authToken: (dadosLogin[0].authToken)!, observacao: observacao, {_ in
                                
                                self.tabela.reloadData()
                                contador = contador + 1
                                if contador >= pedidosNotSync.count {
                                    self.mostraResultados()
                                }
                            })
                            
                            break
                        case self.tipoRequisicao.pedirRevisao:
                            
                            
                            self.pedirRevisaoOffline(self.urlBase, id: pedido.id!, indexPath: indexPath, authToken: (dadosLogin[0].authToken)!, observacao: observacao , {_ in
                                
                                self.tabela.reloadData()
                                contador = contador + 1
                                if contador >= pedidosNotSync.count {
                                    self.mostraResultados()
                                }
                            })
                            
                            break
                        case self.tipoRequisicao.bloquear:
                            
                            self.bloquearOffline(self.urlBase, id: pedido.id!, indexPath: indexPath, authToken: (dadosLogin[0].authToken)!, observacao: observacao, motivoBloqueioId: motivoBloqueioId, {_ in
                                
                                
                                
                                self.tabela.reloadData()
                                contador = contador + 1
                                if contador >= pedidosNotSync.count {
                                    self.mostraResultados()
                                }
                            })
                            break
                        default:
                            SwiftOverlays.removeAllBlockingOverlays()
                            break
                        }
                    }
                    
                }
            } else {
                
                
                self.mostraResultados()
                SwiftOverlays.removeAllBlockingOverlays()
                
            }
        } else {
            
            SwiftOverlays.removeAllBlockingOverlays()
        }
    }
    
    
    func mostraResultados() {
        self.tabela.reloadData()
        
        let requestPedidos  = NSFetchRequest<NSFetchRequestResult>(entityName: self.name.pedidos)
        requestPedidos.returnsObjectsAsFaults = false
        requestPedidos.predicate = NSPredicate(format: "cd_requisicaoDeuCerto == %@", false as CVarArg)
        
        do {
            let dadosLogin = self.lerBancoDeDados(self.name.dadosLogin) as! [DadosLogin]
            if dadosLogin.count > 0 {
                
                let title = "Não Sincronizados"
                var mensagem = "\n"
                let pedidos = try(DatabaseController.getContext().fetch(requestPedidos)) as! [Pedidos]
                
                
                if pedidos.count > 0 {
                    
                    
                    
                    for pedido in pedidos {
                        if let numero = pedido.numero {
                            if let msgErro = pedido.cd_mensagemErro {
                                if msgErro == ""{
                                    mensagem.append("Pedido \(numero)\n\n")
                                } else {
                                    mensagem.append("Pedido \(numero): \(msgErro)\n\n")
                                }
                            }
                        }
                    }
                    self.mostrarAlerta(mensagem, title: title, nomeBotao: "OK", style: .error, completion: {
                        
                        
                        SwiftOverlays.removeAllBlockingOverlays()
                        let text = "Verificando a existencia de novos pedidos"
                        SwiftOverlays.showBlockingWaitOverlayWithText(text)
                        
                        self.baixarPedidosOffline(authToken: dadosLogin[0].authToken!)
                    })
                    
                } else {
                    SwiftOverlays.removeAllBlockingOverlays()
                    let text = "Verificando a existencia de novos pedidos"
                    SwiftOverlays.showBlockingWaitOverlayWithText(text)
                    self.baixarPedidosOffline(authToken: dadosLogin[0].authToken!)
                }
            } else {
                
                SwiftOverlays.removeAllBlockingOverlays()
            }

        } catch {
            SwiftOverlays.removeAllBlockingOverlays()
        }
    }
    
    
    
    
    
}
