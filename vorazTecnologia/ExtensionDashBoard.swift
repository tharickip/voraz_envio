//
//  ExtensionDashBoard.swift
//
//
//  Created by Lucas de Brito Reis on 30/07/17.
//
//

import CoreData
import SnapKit
import Charts

extension DashBoard {
    
    func configCalendario() {
        
        grafico1.addSubview(self.barChartView)
        self.barChartView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(60)
            make.height.equalToSuperview().offset(-60)
            make.width.equalToSuperview().offset(-20)
            make.left.equalToSuperview().offset(0)
        }
        
        grafico2.addSubview(self.chart)
        chart.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(60)
            make.height.equalToSuperview().offset(-60)
            make.width.equalToSuperview().offset(-20)
            make.left.equalToSuperview().offset(0)
        }
        
        grafico5.addSubview(self.chart2)
        chart2.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(60)
            make.height.equalToSuperview().offset(-60)
            make.width.equalToSuperview().offset(-20)
            make.left.equalToSuperview().offset(0)
        }
        
        
        grafico3.addSubview(self.horizontal2)
        self.horizontal2.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(60)
            make.height.equalToSuperview().offset(-60)
            make.width.equalToSuperview().offset(-50)
            make.left.equalToSuperview().offset(0)
        }
        
        
        grafico4.addSubview(self.horizontal)
        self.horizontal.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(60)
            make.height.equalToSuperview().offset(-60)
            make.width.equalToSuperview().offset(-20)
            make.left.equalToSuperview().offset(0)
        }
    }
    
    
    func filtro(){
        let filtro = UINavigationController(rootViewController: FiltroCampanha())
        self.present(filtro, animated: true, completion: nil)
    }
    
    
    func setupChart() {
        
        var id = ""
        
        let campanhaSelecionada = self.lerBancoDeDadosSemId(self.name.CampanhaSelecionada) as! [CampanhaSelecionada]
        let campanhas = self.lerBancoDeDados(self.name.campanhaVendas) as! [CampanhaVendas]
        
        guard let idDefault = campanhas.last?.id else { return }
        id = idDefault
        self.tituloCampanhaSelecionada.text = campanhas.last?.nome!
        
        if campanhaSelecionada.count > 0 {
            if let idCampanhaSelecionada = campanhaSelecionada[0].idSelecionado {
                id  = idCampanhaSelecionada
                if let nome = campanhaSelecionada[0].nome {
                    self.tituloCampanhaSelecionada.text = nome
                }
            }
        }
        
        let comparaCampanhas = self.lerBancoDeDadosSemId(self.name.ComparaCampanhas) as! [ComparaCampanhas]
        barras(comparaCampanhas: comparaCampanhas)
        
        
        let campanha = self.campanhaFiltrada(campanhaId: id)
        if let especies = campanha?.vendasEspecies?.allObjects as? [VendasEspecies] {
            pizza(especies: especies)
            pizza2(especies: especies)
        }
        
        if let entregasEvolucao = campanha?.entregasEvolucao?.allObjects as? [EntregasEvolucao] {
            barrasHorizontal(entregasEvolucao: entregasEvolucao)
        }
        
        if let vendasForecast = campanha?.vendasForecast?.allObjects as? [VendasForecast] {
            barrasHorizontal(vendasForecast: vendasForecast)
        }
        
        if let vendasContadores = campanha?.vendasContadores {
            
            entregue.text       = "\(vendasContadores.valorEntregue)"
            carteira.text       = "\(vendasContadores.valorCarteira)"
            totalVendas.text    = "\(vendasContadores.valorVenda)"
        }
    }
    
    func barras(comparaCampanhas: [ComparaCampanhas]){
        
        let vetorCores = [UIColor(r: 192, g: 212, b: 239), UIColor(r: 106, g: 152, b: 195), UIColor(r: 242, g: 164, b: 104), UIColor(r: 247, g: 206, b: 161)]
        
        var dataSets = [IChartDataSet]()
        
        for indice in 0..<comparaCampanhas.count {
            
            let barChartDataval = [indice: comparaCampanhas[indice].volume]
            var barChartValues: [BarChartDataEntry] = []
            
            for (key,val) in barChartDataval {
                
                let xVal = Double(key)
                let yVal = Double(val)
                let dataEntry = BarChartDataEntry(x: xVal, y: yVal)
                barChartValues.append(dataEntry)
            }
            
            let chartDataSet = BarChartDataSet(values: barChartValues, label: comparaCampanhas[indice].nome!)
            chartDataSet.colors = [vetorCores[indice % 4]]
            dataSets.append(chartDataSet)
        }
        
        
        let chartData = BarChartData(dataSets: dataSets)
        self.barChartView.data = chartData
        let description = Description()
        description.text = ""
        self.barChartView.chartDescription = description
        self.barChartView.xAxis.drawLabelsEnabled = false
        self.barChartView.animate(xAxisDuration: 2.0, yAxisDuration: 2.0, easingOption: .easeInBack)
    }
    
    
    func pizza(especies: [VendasEspecies])  {
        
        var entries = [PieChartDataEntry]()
        for especie in especies {
            
            let dataEntry = PieChartDataEntry(value: especie.valor, label: especie.nome!)
            entries.append(dataEntry)
        }
        
        // 3. chart setup
        let set = PieChartDataSet( values: entries, label: "")
        // this is custom extension method. Download the code for more details.
        
        
        let azulClaro       = UIColor(r: 192, g: 212, b: 239)
        let azulEscuro      = UIColor(r: 106, g: 152, b: 195)
        let laranjaEscuro   = UIColor(r: 242, g: 164, b: 104)
        let laranjaClaro    = UIColor(r: 247, g: 206, b: 161)
        
        
        set.colors =  [azulEscuro, laranjaEscuro, azulClaro, laranjaClaro]
        
        if especies.count > 4 {
            for _ in 4..<especies.count {
            let randomB: UInt32 = 155 + arc4random_uniform(100)
            let randomG: UInt32 = 150 + arc4random_uniform(30)
            let corAleatoria = UIColor(r: 192, g: CGFloat(randomG), b: CGFloat(randomB))
            set.colors.append(corAleatoria)
            }
        }
        
        
        
        
        
        let data = PieChartData(dataSet: set)
        chart.data = data
        chart.noDataText = "Não há dados disponíveis"
        // user interaction
        chart.isUserInteractionEnabled = true
        chart.drawEntryLabelsEnabled = false
        
        let description = Description()
        description.text = ""
        chart.chartDescription = description
        //chart.centerText = "Pie Chart"
        chart.holeRadiusPercent = 0
        chart.transparentCircleColor = UIColor.clear
        
        
        if especies.count  > 0 {
            chartVazioLabel.removeFromSuperview()
            chartVazioView.removeFromSuperview()
            
        } else {
            chart.addSubview(chartVazioView)
            chartVazioView.addSubview(chartVazioLabel)
            chartVazioView.snp.makeConstraints({ (make) in
                make.edges.equalToSuperview()
            })
            chartVazioLabel.snp.makeConstraints({ (make) in
                make.center.equalToSuperview()
            })
        }
        
        
        
        
        
    }
    
    func pizza2(especies: [VendasEspecies])  {
        
        var entries = [PieChartDataEntry]()
        for especie in especies {
            
            let dataEntry = PieChartDataEntry(value: especie.volume, label: especie.nome!)
            entries.append(dataEntry)
        }
        
        // 3. chart setup
        let set = PieChartDataSet( values: entries, label: "")
        // this is custom extension method. Download the code for more details.
        
        set.colors =  []
        
        
        for _ in 0..<especies.count {
            let randomR: UInt32 = arc4random_uniform(255)
            let randomB: UInt32 = arc4random_uniform(255)
            let randomG: UInt32 = arc4random_uniform(255)
            let corAleatoria = UIColor(r: CGFloat(randomR), g: CGFloat(randomG), b: CGFloat(randomB))
            set.colors.append(corAleatoria)
            
        }
        
        let data = PieChartData(dataSet: set)
        chart2.data = data
        chart2.noDataText = "Não há dados disponíveis"
        // user interaction
        chart2.isUserInteractionEnabled = true
        chart2.drawEntryLabelsEnabled = false
        
        let description = Description()
        description.text = ""
        chart2.chartDescription = description
        //chart.centerText = "Pie Chart"
        chart2.holeRadiusPercent = 0
        chart2.transparentCircleColor = UIColor.clear
        
        
        if especies.count  > 0 {
            chart2VazioLabel.removeFromSuperview()
            chart2VazioView.removeFromSuperview()
            
        } else {
            chart2.addSubview(chart2VazioView)
            chart2VazioView.addSubview(chart2VazioLabel)
            chart2VazioView.snp.makeConstraints({ (make) in
                make.edges.equalToSuperview()
            })
            chart2VazioLabel.snp.makeConstraints({ (make) in
                make.center.equalToSuperview()
            })
        }
        
    }
    
    func barrasHorizontal(vendasForecast: [VendasForecast]){
        
        let fator: CGFloat = CGFloat(vendasForecast.count) * (vendasForecast.count > 8  ?  0.175 : 0.25)
        let totalFator = view.frame.width * max(fator, 1)
        
        grafico3.snp.updateConstraints{ (make) in
            make.top.equalTo(grafico5.snp.bottom).offset(YBox)
            make.width.left.equalTo(grafico1)
            make.height.equalTo(totalFator)
        }
        
        
        var dataSets = [IChartDataSet]()
        
        var aRealizarValues: [BarChartDataEntry] = []
        var realizadoValues: [BarChartDataEntry] = []
        var nomes:          [String]            = []
        
        
        
        for indice in 0..<vendasForecast.count{
            
            let dataEntryARealizar = BarChartDataEntry(x: Double(indice), y: 50)
            
            aRealizarValues.append(dataEntryARealizar)
            
            let dataEntryRealizado = BarChartDataEntry(x: Double(indice), y: vendasForecast[indice].volumeVendaRealizado)
            realizadoValues.append(dataEntryRealizado)
            
            nomes.append(String(vendasForecast[indice].nome!))
            
        }
        
        let aRealizarDataSet = BarChartDataSet(values: aRealizarValues, label: "A Realizar") //"Carteira"
        let realizadoDataSet = BarChartDataSet(values: realizadoValues, label: "Realizado") //"Entregue"
        
        
        let azulEscuro      = UIColor(r: 106, g: 152, b: 195)
        let laranjaEscuro   = UIColor(r: 242, g: 164, b: 104)
        
        
        
        
        //chartDataSet.colors = [NSUIColor.blue]
        //carteiraDataSet.colors = [corAleatoria]
        //entregueDataSet.colors = [corFraca]
        
        aRealizarDataSet.colors = [laranjaEscuro]
        realizadoDataSet.colors = [azulEscuro]
        
        dataSets.append(aRealizarDataSet)
        dataSets.append(realizadoDataSet)
        
        
        let chartData = BarChartData(dataSets: dataSets)
        chart.noDataText = "Não há dados disponíveis"
        
        self.horizontal.data                            = chartData
        self.horizontal.chartDescription?.text          = ""
        self.horizontal.xAxis.drawLabelsEnabled         = true
        self.horizontal.xAxis.labelPosition             = .bottom
        self.horizontal.drawGridBackgroundEnabled       = true
        self.horizontal.xAxis.valueFormatter            = IndexAxisValueFormatter(values: nomes)
        //self.horizontal.xAxis.centerAxisLabelsEnabled   = true
        self.horizontal.xAxis.granularityEnabled        = true
        self.horizontal.xAxis.granularity               = 1.0
        self.horizontal.xAxis.drawLimitLinesBehindDataEnabled = true
        self.horizontal.xAxis.labelCount = nomes.count
        
        
        self.horizontal.barData?.barWidth  = 0.3
        self.horizontal.groupBars(fromX: -0.5, groupSpace: 0.3, barSpace: 0.05)
        self.horizontal.autoresizesSubviews = true
        
        self.horizontal.animate(xAxisDuration: 0.5, yAxisDuration: 1.0, easingOption: .linear)
        
        if vendasForecast.count  > 0 {
            horizontalVazioLabel.removeFromSuperview()
            horizontalVazioView.removeFromSuperview()
            
        } else {
            horizontal.addSubview(horizontalVazioView)
            horizontalVazioView.addSubview(horizontalVazioLabel)
            horizontalVazioView.snp.makeConstraints({ (make) in
                make.edges.equalToSuperview()
            })
            horizontalVazioLabel.snp.makeConstraints({ (make) in
                make.center.equalToSuperview()
            })
        }
    }
    
    func barrasHorizontal(entregasEvolucao: [EntregasEvolucao]){
        
        let fator: CGFloat = CGFloat(entregasEvolucao.count) * (entregasEvolucao.count > 8  ?  0.175 : 0.25)
        
        let totalFator = view.frame.width * max(fator, 1)
        
        
        grafico4.snp.updateConstraints({ (make) in
            make.top.equalTo(grafico3.snp.bottom).offset(YBox)
            make.width.left.equalTo(grafico1)
            make.bottom.equalToSuperview().offset(-30)
            make.height.equalTo(totalFator)
        })
        
        var dataSets = [IChartDataSet]()
        
        var carteiraValues: [BarChartDataEntry] = []
        var entregueValues: [BarChartDataEntry] = []
        var nomes:          [String]            = []
        
        
        
        for indice in 0..<entregasEvolucao.count{
            
            let dataEntryCarteira = BarChartDataEntry(x: Double(indice), y: 50.0)
            carteiraValues.append(dataEntryCarteira)
            
            let dataEntryEntregue = BarChartDataEntry(x: Double(indice), y: entregasEvolucao[indice].volumeEntregue)
            entregueValues.append(dataEntryEntregue)
            nomes.append(String(entregasEvolucao[indice].nome!))
            
        }
        
        let carteiraDataSet = BarChartDataSet(values: carteiraValues, label: "Carteira") //"Carteira"
        let entregueDataSet = BarChartDataSet(values: entregueValues, label: "Entregue") //"Entregue"
        
        
        let azulEscuro      = UIColor(r: 106, g: 152, b: 195)
        let laranjaEscuro   = UIColor(r: 242, g: 164, b: 104)

        carteiraDataSet.colors = [laranjaEscuro]
        entregueDataSet.colors = [azulEscuro]
        
        dataSets.append(carteiraDataSet)
        dataSets.append(entregueDataSet)
        
        
        let chartData = BarChartData(dataSets: dataSets)
        chart.noDataText = "Não há dados disponíveis"
        
        self.horizontal2.data                           = chartData
        self.horizontal2.chartDescription?.text         = ""
        self.horizontal2.xAxis.drawLabelsEnabled        = true
        self.horizontal2.xAxis.labelPosition            = .bottom
        self.horizontal2.drawGridBackgroundEnabled      = true
        self.horizontal2.xAxis.valueFormatter           = IndexAxisValueFormatter(values: nomes)
        //self.horizontal2.xAxis.centerAxisLabelsEnabled  = true
        self.horizontal2.xAxis.drawGridLinesEnabled     = true
        self.horizontal2.xAxis.granularityEnabled       = true
        self.horizontal2.xAxis.granularity              = 1.0
        //self.horizontal2.xAxis.drawLimitLinesBehindDataEnabled = true
        self.horizontal2.xAxis.labelCount = nomes.count
        self.horizontal2.autoresizesSubviews = true
        
//        //legend
//        let legend = self.horizontal2.legend
//        legend.enabled = true
//        legend.horizontalAlignment = .center
//        legend.verticalAlignment = .center
//        legend.orientation = .horizontal
//        legend.drawInside = false
//        legend.yOffset = 10.0;
//        legend.xOffset = 10.0;
//        legend.yEntrySpace = 10.0;
        
        self.horizontal2.barData?.barWidth  = 0.3
        self.horizontal2.groupBars(fromX: -0.5, groupSpace: 0.3, barSpace: 0.05)
        
        self.horizontal2.animate(xAxisDuration: 1.0, yAxisDuration: 1.0, easingOption: .linear)
        
        if entregasEvolucao.count  > 0 {
            horizontal2VazioLabel.removeFromSuperview()
            horizontal2VazioView.removeFromSuperview()
            
        } else {
            horizontal2.addSubview(horizontal2VazioView)
            horizontal2VazioView.addSubview(horizontal2VazioLabel)
            horizontal2VazioView.snp.makeConstraints({ (make) in
                make.edges.equalToSuperview()
            })
            horizontal2VazioLabel.snp.makeConstraints({ (make) in
                make.center.equalToSuperview()
            })
        }
        
        
    }
}
