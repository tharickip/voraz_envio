//
//  PedidoCell.swift
//  agroshow
//
//  Created by Lucas de Brito Reis on 19/05/17.
//  Copyright © 2017 MinervaAplicativos. All rights reserved.
//

import UIKit
import SnapKit
import AIFlatSwitch

class FeedCell: UITableViewCell {
    
    let boxStatus: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }()
    
    let boxCliente: UIView = {
        let view = UIView()
        view.backgroundColor =  UIColor.white
        return view
    }()
    
    let circulo: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 13
        view.clipsToBounds = true
        return view
    }()
    
    var nomeCliente : UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.left
        label.text = ""
        return label
    }()
    
    var dataCompra: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: UIFont.smallSystemFontSize)
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.left
        label.text = ""
        return label
    }()
    
    
    var statusPedido: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: UIFont.systemFontSize)
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.left
        label.text = ""
        return label
    }()
    
    let numeroLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.left
        label.text = "Número: "
        return label
    }()
    
    var numero: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.left
        label.text = ""
        return label
    }()
    
    let totalLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.left
        label.text = "Total: "
        return label
    }()
    
    var total: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.left
        label.text = ""
        return label
    }()

    
    let flatSwitch: AIFlatSwitch = {
        let flat = AIFlatSwitch(frame: CGRect(x: 0, y: 0, width: 26, height: 26))
        flat.lineWidth = 2.0
        flat.strokeColor = UIColor.white
        flat.trailStrokeColor = UIColor.white
        flat.animatesOnTouch = true
        flat.isEnabled = false
        return flat
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
        //self.backgroundColor = UIColor.blue
    }
    
    func atribuiStatus(_ status: String) {
        switch status {
        case "PENDENTE":
            circulo.backgroundColor = UIColor(r: 255, g: 118, b: 0)
            statusPedido.textColor  = UIColor(r: 255, g: 118, b: 0)
            statusPedido.text       = "Pendente"
            
        case "A_REVISAR":
            circulo.backgroundColor = UIColor(r: 178, g: 87, b: 9)
            statusPedido.textColor  = UIColor(r: 178, g: 87, b: 9)
            statusPedido.text       = "A Revisar"
            
        case "AGUARDANDO_LIBERACAO":
            circulo.backgroundColor = UIColor(r: 47, g: 9, b: 178)
            statusPedido.textColor  = UIColor(r: 47, g: 9, b: 178)
            statusPedido.text       = "Aguardando Liberação"
            
        case "BLOQUEADO":
            circulo.backgroundColor = UIColor(r: 178, g: 0, b: 0)
            statusPedido.textColor  = UIColor(r: 178, g: 0, b: 0)
            statusPedido.text       = "Bloqueado"
            
        case "FATURADO_PARCIAL":
            circulo.backgroundColor = UIColor(r: 20, g: 133, b: 204)
            statusPedido.textColor  = UIColor(r: 20, g: 133, b: 204)
            statusPedido.text       = "Faturado Parcial"
        case "A_FATURAR":
            circulo.backgroundColor = UIColor(r: 204, g: 105, b: 20)
            statusPedido.textColor  = UIColor(r: 204, g: 105, b: 20)
            statusPedido.text       = "A Faturar"
            
        case "AGUARDAR_FATURAMENTO":
            circulo.backgroundColor = UIColor(r: 255, g: 215, b: 0)
            statusPedido.textColor  = UIColor(r: 255, g: 215, b: 0)
            statusPedido.text       = "Aguardar Faturamento"
        case "FATURADO":
            circulo.backgroundColor = UIColor(r: 17, g: 178, b: 25)
            statusPedido.textColor  = UIColor(r: 17, g: 178, b: 25)
            statusPedido.text       = "Faturado"
            
        case "LIBERADO":
            circulo.backgroundColor = UIColor(r: 0, g: 255, b: 13)
            statusPedido.textColor  = UIColor(r: 0, g: 255, b: 13)
            statusPedido.text       = "Liberado"
            
        case "REGISTRADO":
            circulo.backgroundColor = .lightGray
            statusPedido.textColor  = .darkGray
            statusPedido.text       = "Registrado"
            
        default:
            circulo.backgroundColor = .darkGray
            statusPedido.textColor  = .darkGray
            statusPedido.text       = status
        }
    }
    
    func config(pedido: Pedidos) {
        
        nomeCliente.text        = pedido.cliente?.pessoa?.nome
        dataCompra.text         = pedido.dataPedido
        numero.text             = pedido.numero
        flatSwitch.isSelected   = pedido.cd_isSync
        
        if let valorTotal = pedido.valorTotal {
            
            total.text = Double(valorTotal)?.arredonda()
        }

        self.atribuiStatus(pedido.status!)

    }
    
    func setupViews() {
        
        addSubview(boxStatus)
        boxStatus.addSubview(circulo)
        circulo.addSubview(flatSwitch)
        
        addSubview(boxCliente)
        boxCliente.addSubview(dataCompra)
        boxCliente.addSubview(nomeCliente)
        boxCliente.addSubview(numeroLabel)
        boxCliente.addSubview(numero)
        boxCliente.addSubview(totalLabel)
        boxCliente.addSubview(total)
        boxCliente.addSubview(statusPedido)
        
        boxStatus.snp.makeConstraints { (make) -> Void in
            make.left.top.bottom.equalToSuperview()
            make.width.equalTo(70)
        }
        
        circulo.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.equalTo(20)
            make.right.equalTo(-24)
            make.height.width.equalTo(26)
        }
        
        boxCliente.snp.makeConstraints { (make) in
            make.top.right.bottom.equalToSuperview()
            make.left.equalTo(70)
        }
        
        dataCompra.snp.makeConstraints { (make) in
            make.top.equalTo(10)
            make.right.equalTo(-20)
            //make.left.equalTo(boxCliente.snp.centerX).offset(40)
            make.left.equalTo(boxCliente.snp.right).offset(-120)
        }
        
        nomeCliente.snp.makeConstraints { (make) in
            make.top.equalTo(10)
            make.left.equalToSuperview()
            make.right.lessThanOrEqualTo(dataCompra.snp.left)
        }
        
        numeroLabel.snp.makeConstraints { (make) in
            make.top.equalTo(nomeCliente.snp.bottom).offset(5)
            make.left.equalToSuperview()
        }
        
        numero.snp.makeConstraints { (make) in
            make.top.equalTo(nomeCliente.snp.bottom).offset(5)
            make.left.equalTo(numeroLabel.snp.right)
        }
        
        totalLabel.snp.makeConstraints { (make) in
            make.top.equalTo(numeroLabel.snp.bottom).offset(3)
            make.left.equalToSuperview()
            make.bottom.equalToSuperview().offset(-10)
        }
        
        total.snp.makeConstraints { (make) in
            make.top.equalTo(numeroLabel.snp.bottom).offset(3)
            make.left.equalTo(totalLabel.snp.right)
            make.bottom.equalToSuperview().offset(-10)
        }
        
        statusPedido.snp.makeConstraints { (make) in
            make.left.equalTo(dataCompra.snp.left)
            make.top.equalTo(numeroLabel.snp.top)
            make.right.equalTo(-20)
        }
    }
    
}
