//
//  MenuViewController.swift
//  agroshowiOS
//
//  Created by Lucas de Brito Reis on 25/06/17.
//  Copyright © 2017 Minerva Aplicativos LTDA ME. All rights reserved.
//

import UIKit
import SnapKit

private let menuCellIdentifier = "menuCellIdentifier"

class Menu: BaseViewController, UITableViewDataSource, UITableViewDelegate  {
    
    
    let labelName = ["Liberacao de pedido", "Dashboard", "Logout"]
    let imageName = ["Dashboard", "Liberação", "Logout"]
    
    let box: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(r: 150, g: 35, b: 84)//UIColor(r: 54, g: 142, b: 58)
        return view
    }()
    
    let circulo: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(r: 128, g: 30, b: 72)//UIColor(r: 1, g: 96, b: 12)
        return view
    }()
    
    var letra: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 60)
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.center
        label.textColor = UIColor.white
        label.text = ""
        return label
    }()
    
    var nomeUsuario: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.left
        label.textColor = UIColor.white
        label.text = ""
        return label
    }()
    
    var emailUsuario: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 16)
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.left
        label.textColor = UIColor.white
        label.text = ""
        return label
    }()
    
    let tabela: UITableView = {
        let tableview = UITableView()
        return tableview
    }()
    
    let assinatura: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.left
        label.text = ""
        return label
    }()
    
    var espacamento:CGFloat = 135
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Configurando o data source e o delegate da tableview
        tabela.dataSource   = self
        tabela.delegate     = self
        tabela.isScrollEnabled = false
        tabela.register(MenuCell.self, forCellReuseIdentifier: menuCellIdentifier)
        tabela.allowsSelection = true
        
        //configurando as views
        setupViews()
        
    }
    
    // TableView - Data Source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: menuCellIdentifier, for: indexPath) as! MenuCell
        //config the cell
        cell.config(imageName: imageName, labelName: labelName, indexPath: indexPath)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    
    
    
    func setupViews() {
        
        
        //barra de navegacao
        if let barranavegacao = self.navigationController?.navigationBar {
            barranavegacao.isTranslucent = false
            barranavegacao.barTintColor = UIColor(r: 150, g: 35, b: 84)//UIColor(r: 54, g: 142, b: 58)
            barranavegacao.tintColor = UIColor.white
            
            view.backgroundColor = UIColor.lightGray
            barranavegacao.addSubview(box)
            box.addSubview(nomeUsuario)
            box.addSubview(emailUsuario)
            box.addSubview(circulo)
            circulo.addSubview(letra)
            view.addSubview(tabela)
            
            
            box.snp.makeConstraints { (make) in
                make.top.left.right.equalToSuperview()
                make.height.equalTo(180)
            }
            
            circulo.snp.makeConstraints({ (make) in
                make.centerX.equalToSuperview()
                make.centerY.equalToSuperview().offset(-30)
                make.size.equalTo(80)
            })
            
            circulo.layer.cornerRadius = 40
            
            letra.snp.makeConstraints({ (make) in
                make.center.size.equalToSuperview()
            })
            
            nomeUsuario.snp.makeConstraints({ (make) in
                make.top.equalTo(circulo.snp.bottom).offset(10)
                make.left.equalTo(20)
                make.width.equalTo(view.frame.width-70)
            })
            
            emailUsuario.snp.makeConstraints({ (make) in
                make.top.equalTo(nomeUsuario.snp.bottom)
                make.left.equalTo(20)
                make.width.equalTo(view.frame.width-70)
            })
            
            let size = UIScreen.main.bounds
            tabela.snp.makeConstraints({ (make) in
                make.top.equalTo(size.width<size.height ? 135: 145)
                make.left.right.equalToSuperview()
                make.height.equalTo(150)
            })
            
            view.addSubview(assinatura)
            assinatura.snp.makeConstraints({ (make) in
                make.bottom.equalToSuperview()
            })
        }
    }
    
    
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        tabela.snp.updateConstraints { (make) in
            make.top.equalTo(size.width<size.height ? 135: 145)
            make.left.right.equalToSuperview()
            make.height.equalTo(150)

        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch indexPath.row {
        case 0: dismiss(animated: true, completion: nil)
            break
        case 1:
            let navegacao = UINavigationController(rootViewController: DashBoard())
            present(navegacao, animated: true, completion: nil)
            break
        case 2:
            self.clearContext()
            self.clearCampanhas(entityName: self.name.campanhaVendas)
            self.clearComparaCampanhas()
            self.clearCampanhaSelecionada()
            let LoginVC = self.view.window?.rootViewController as? Login
            LoginVC?.dismiss(animated: true, completion: nil)
            
            //self.parent?.dismiss(animated: true, completion: nil)
        default: dismiss(animated: true, completion: nil)
            break
        }
    }
    
}
