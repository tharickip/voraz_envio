//
//  ExtensionBase.swift
//  vorazTecnologia
//
//  Created by Lucas de Brito Reis on 20/07/17.
//  Copyright © 2017 Minerva Aplicativos LTDA ME. All rights reserved.
//

import UIKit
import CoreData
import Alamofire
import SwiftyJSON

extension BaseViewController {
    
    func atribuilogin(_ dicionario: JSON)->DadosLogin {
        //limpa o banco de dados para evitar duplo save
        clearContext()
        
        let dadosLogin = NSEntityDescription.insertNewObject(forEntityName: name.dadosLogin, into: DatabaseController.getContext()) as! DadosLogin
        
        dadosLogin.authToken          = dicionario["authToken"].stringValue
        dadosLogin.usuarioInclusao    = dicionario["acesso"]["usuarioInclusao"].stringValue
        dadosLogin.dataInclusao       = dicionario["acesso"]["dataInclusao"].stringValue
        dadosLogin.idImportado        = dicionario["acesso"]["idImportado"].stringValue
        dadosLogin.permissoesMenu     = dicionario["acesso"]["permissoesMenu"].stringValue
        dadosLogin.organizacaoPadrao  = dicionario["acesso"]["usuarioInclusao"].stringValue
        dadosLogin.permissoesTela     = dicionario["acesso"]["permissoesTela"].stringValue
        dadosLogin.estado             = dicionario["acesso"]["estado"].stringValue
        dadosLogin.id                 = dicionario["acesso"]["id"].stringValue
        dadosLogin.dataAlteracao      = dicionario["acesso"]["dataAlteracao"].stringValue
        dadosLogin.ultimaSessao       = dicionario["acesso"]["ultimaSessao"].stringValue
        dadosLogin.areasGerenciais    = dicionario["acesso"]["areasGerenciais"].stringValue
        dadosLogin.ativo              = dicionario["acesso"]["ativo"].stringValue
        dadosLogin.usuarioAlteracao   = dicionario["acesso"]["usuarioAlteracao"].stringValue
        dadosLogin.grupoPermissao     = dicionario["acesso"]["grupoPermissao"].stringValue
        dadosLogin.organizacao        = atribuiOrganizacao(dicionario["acesso"]["organizacao"])
        dadosLogin.usuario            = atribuiUsuario(dicionario["acesso"]["usuario"])
        
        DatabaseController.saveContext()
        return dadosLogin
    }
    
    
    
    
    func atribuiUsuario(_ dicionario: JSON) -> Usuario {
        let usuario = NSEntityDescription.insertNewObject(forEntityName: name.usuario, into: DatabaseController.getContext()) as! Usuario
        usuario.id                 = dicionario["id"].stringValue
        usuario.dataInclusao       = dicionario["dataInclusao"].stringValue
        usuario.dataAlteracao      = dicionario["dataAlteracao"].stringValue
        usuario.usuarioInclusao    = dicionario["usuarioInclusao"].stringValue
        usuario.usuarioAlteracao   = dicionario["usuarioAlteracao"].stringValue
        usuario.idImportado        = dicionario["idImportado"].stringValue
        usuario.estado             = dicionario["estado"].stringValue
        usuario.nome               = dicionario["nome"].stringValue
        usuario.email              = dicionario["email"].stringValue
        usuario.superior           = dicionario["superior"].stringValue
        usuario.senhaHash          = dicionario["senhaHash"].stringValue
        usuario.descricaoAcesso    = dicionario["descricaoAcesso"].stringValue
        usuario.conta              = dicionario["conta"].stringValue
        usuario.idOrg              = dicionario["idOrg"].stringValue
        usuario.nomeFotoAws        = dicionario["nomeFotoAws"].stringValue
        usuario.emailSecundario    = dicionario["authToken"].stringValue
        usuario.vendedor           = dicionario["vendedor"].boolValue
        usuario.gerente            = dicionario["gerente"].boolValue
        usuario.diretor            = dicionario["diretor"].boolValue
        usuario.presidente         = dicionario["presidente"].boolValue
        usuario.admin              = dicionario["admin"].boolValue
        usuario.ativo              = dicionario["ativo"].boolValue
        DatabaseController.saveContext()
        return usuario
    }
    
    func atribuiOrganizacao(_ dicionario: JSON) -> Organizacao {
        
        let organizacao = NSEntityDescription.insertNewObject(forEntityName: name.organizacao, into: DatabaseController.getContext()) as! Organizacao
        
        organizacao.usuarioInclusao        = dicionario["usuarioInclusao"].stringValue
        organizacao.organizacao            = dicionario["organizacao"].stringValue
        organizacao.dataInclusao           = dicionario["dataInclusao"].stringValue
        organizacao.idImportado            = dicionario["idImportado"].stringValue
        organizacao.idsAreasGerenciais     = dicionario["idsAreasGerenciais"].stringValue
        organizacao.estado                 = dicionario["estado"].stringValue
        organizacao.id                     = dicionario["id"].stringValue
        organizacao.dataAlteracao          = dicionario["dataAlteracao"].stringValue
        organizacao.ativo                  = dicionario["ativo"].stringValue
        organizacao.usuarioAlteracao       = dicionario["usuarioAlteracao"].stringValue
        organizacao.logo                   = dicionario["logo"].stringValue
        organizacao.ehpai                  = dicionario["ehpai"].stringValue
        organizacao.nome                   = dicionario["nome"].stringValue
        organizacao.responsavel            = atribuiResponsavel(dicionario["responsavel"])
        DatabaseController.saveContext()
        return organizacao
    }
    
    func atribuiResponsavel(_ dicionario: JSON) -> Responsavel {
        let responsavel = NSEntityDescription.insertNewObject(forEntityName: name.responsavel, into: DatabaseController.getContext()) as! Responsavel
        responsavel.id     = dicionario["id"].stringValue
        responsavel.nome   = dicionario["nome"].stringValue
        responsavel.email  = dicionario["email"].stringValue
        DatabaseController.saveContext()
        return responsavel
    }
    
    func atribuirequisicao(id pedidoId: String, isSync: Bool, observacao: String, motivoBloqueioId: String) -> Requisicao {
        let requisicao = NSEntityDescription.insertNewObject(forEntityName: name.responsavel, into: DatabaseController.getContext()) as! Requisicao
        requisicao.motivoBloqueioId = motivoBloqueioId
        requisicao.observacao       = observacao
        requisicao.pedidoId         = pedidoId
        requisicao.isSync           = isSync
        DatabaseController.saveContext()
        return requisicao
    }
    
    
    
    func atribuiPedido(_ dicionario: JSON) {
        
        let pedido = NSEntityDescription.insertNewObject(forEntityName: name.pedidos, into: DatabaseController.getContext()) as! Pedidos
        pedido.cd_id                    = dicionario["id"].int64Value
        pedido.id                       = dicionario["id"].stringValue
        pedido.valorFrete               = dicionario["valorFrete"].stringValue
        pedido.dataInclusao             = dicionario["dataInclusao"].stringValue
        pedido.listaPreco               = dicionario["listaPreco"].stringValue
        pedido.cliente                  = atribuiCliente(dicionario["cliente"])
        pedido.enderecoFaturamento      = dicionario["enderecoFaturamento"].stringValue
        pedido.campanha                 = atribuiCampanha(dicionario["campanha"])
        pedido.enderecoEntrega          = dicionario["enderecoEntrega"].stringValue
        pedido.dataEntrega              = dicionario["dataEntrega"].stringValue
        pedido.marcadores               = dicionario["marcadores"].stringValue
        pedido.valorFaturado            = dicionario["valorFaturado"].stringValue
        pedido.tipoFrete                = dicionario["tipoFrete"].stringValue
        pedido.dataPrevisaoRetorno      = dicionario["dataPrevisaoRetorno"].stringValue
        pedido.organizacaoId            = dicionario["organizacaoId"].stringValue
        pedido.impressoes               = dicionario["impressoes"].stringValue
        pedido.vendedor                 = dicionario["vendedor"].stringValue
        pedido.dataPrimeiroVencimento   = dicionario["dataPrimeiroVencimento"].stringValue
        pedido.numeroImportado          = dicionario["numeroImportado"].stringValue
        pedido.areaGerencial            = dicionario["areaGerencial"].stringValue
        pedido.ativo                    = dicionario["ativo"].stringValue
        pedido.dataAlteracao            = dicionario["dataAlteracao"].stringValue
        pedido.quantidadeParcelas       = dicionario["quantidadeParcelas"].stringValue
        pedido.observacaoBloqueio       = dicionario["observacaoBloqueio"].stringValue
        pedido.transportadora           = dicionario["transportadora"].stringValue
        pedido.revisoes                 = dicionario["revisoes"].stringValue
        pedido.numero                   = dicionario["numero"].stringValue
        pedido.observacao               = dicionario["observacao"].stringValue
        pedido.leitoresRevisoes         = dicionario["leitoresRevisoes"].stringValue
        pedido.eventos                  = dicionario["eventos"].stringValue
        pedido.dataUltimoVencimento     = dicionario["dataUltimoVencimento"].stringValue
        pedido.valorDesconto            = dicionario["valorDesconto"].stringValue
        pedido.tipo                     = dicionario["tipo"].stringValue
        pedido.motivoBloqueio           = dicionario["motivoBloqueio"].stringValue
        pedido.usuarioInclusaoPedido    = dicionario["usuarioInclusaoPedido"].stringValue
        pedido.usuarioInclusao          = dicionario["usuarioInclusao"].stringValue
        pedido.valorDescontoCliente     = dicionario["valorDescontoCliente"].stringValue
        pedido.idImportado              = dicionario["idImportado"].stringValue
        pedido.pedidoOrigem             = dicionario["pedidoOrigem"].stringValue
        pedido.dataPedido               = dicionario["dataPedido"].stringValue
        pedido.status                   = dicionario["status"].stringValue
        pedido.estado                   = dicionario["estado"].stringValue
        pedido.proximoAvaliador         = atribuiProximoAvaliador(dicionario["proximoAvaliador"])
        pedido.anexos                   = dicionario["anexos"].stringValue
        pedido.usuarioAlteracao         = dicionario["usuarioAlteracao"].stringValue
        //pedido.temParcelas            = dicionario["temParcelas"].stringValue
        pedido.formaPagamento           = dicionario["formaPagamento"].stringValue
        pedido.valorTotal               = dicionario["valorTotal"].stringValue
        
        pedido.cd_isSync                = true
        pedido.cd_observacao            = ""
        pedido.cd_motivoBloqueioId      = String(0)
        pedido.cd_mensagemErro          = ""
        pedido.cd_requisicaoDeuCerto    = true
        
        var vetorItens: [Itens] = []
        for (_, jsonItens) in dicionario["itens"] {
            let item = self.atribuiItens(jsonItens)
            vetorItens.append(item)
        }
        pedido.setValue(NSSet(array: vetorItens), forKey: "itens")
        
        DatabaseController.saveContext()
    }
    
    
    func atribuiProximoAvaliador(_ dicionario: JSON) -> ProximoAvaliador {
        
        let proximoAvaliador = NSEntityDescription.insertNewObject(forEntityName: name.proximoAvaliador, into: DatabaseController.getContext()) as! ProximoAvaliador
        proximoAvaliador.id                  = dicionario["id"].stringValue
        proximoAvaliador.dataInclusao        = dicionario["dataInclusao"].stringValue
        proximoAvaliador.dataAlteracao       = dicionario["dataAlteracao"].stringValue
        proximoAvaliador.usuarioInclusao     = dicionario["usuarioInclusao"].stringValue
        proximoAvaliador.usuarioAlteracao    = dicionario["usuarioAlteracao"].stringValue
        proximoAvaliador.idImportado         = dicionario["idImportado"].stringValue
        proximoAvaliador.estado              = dicionario["estado"].stringValue
        proximoAvaliador.nome                = dicionario["nome"].stringValue
        proximoAvaliador.email               = dicionario["email"].stringValue
        proximoAvaliador.superior            = dicionario["superior"].stringValue
        proximoAvaliador.senhaHash           = dicionario["senhaHash"].stringValue
        proximoAvaliador.descricaoAcesso     = dicionario["descricaoAcesso"].stringValue
        proximoAvaliador.idOrg               = dicionario["idOrg"].stringValue
        proximoAvaliador.nomeFotoAws         = dicionario["nomeFotoAws"].stringValue
        proximoAvaliador.emailSecundario     = dicionario["emailSecundario"].stringValue
        proximoAvaliador.vendedor            = dicionario["vendedor"].boolValue
        proximoAvaliador.gerente             = dicionario["gerente"].boolValue
        proximoAvaliador.diretor             = dicionario["diretor"].boolValue
        proximoAvaliador.presidente          = dicionario["presidente"].boolValue
        proximoAvaliador.admin               = dicionario["admin"].boolValue
        proximoAvaliador.ativo               = dicionario["ativo"].boolValue
        DatabaseController.saveContext()
        return proximoAvaliador
    }
    
    func atribuiCampanha(_ dicionario: JSON) -> Campanha {
        
        let campanha = NSEntityDescription.insertNewObject(forEntityName: name.campanha, into: DatabaseController.getContext()) as! Campanha
        campanha.id                  = dicionario["id"].stringValue
        campanha.dataInclusao        = dicionario["dataInclusao"].stringValue
        campanha.dataAlteracao       = dicionario["dataAlteracao"].stringValue
        campanha.usuarioInclusao     = dicionario["usuarioInclusao"].stringValue
        campanha.usuarioAlteracao    = dicionario["usuarioAlteracao"].stringValue
        campanha.idImportado         = dicionario["idImportado"].stringValue
        campanha.estado              = dicionario["estado"].stringValue
        campanha.organizacaoId       = dicionario["organizacaoId"].stringValue
        campanha.nome                = dicionario["nome"].stringValue
        campanha.inicio              = dicionario["inicio"].stringValue
        campanha.fim                 = dicionario["fim"].stringValue
        campanha.especies            = dicionario["especies"].stringValue
        campanha.ativo               = dicionario["ativo"].stringValue
        campanha.anoFiscal           = dicionario["anoFiscal"].stringValue
        campanha.datasBase           = dicionario["datasBase"].stringValue
        DatabaseController.saveContext()
        return campanha
    }
    
    func atribuiItens(_ dicionario: JSON) -> Itens {
        
        
        
        let item = NSEntityDescription.insertNewObject(forEntityName: name.itens, into: DatabaseController.getContext()) as! Itens
        item.valorTotal                 = dicionario["valorTotal"].stringValue
        item.movimentacoes              = dicionario["movimentacoes"].stringValue
        item.quantidadeRemessaDeposito  = dicionario["quantidadeRemessaDeposito"].stringValue
        item.peneira                    = dicionario["peneira"].stringValue
        item.quantidadeDevolvida        = dicionario["quantidadeDevolvida"].stringValue
        item.dataInclusao               = dicionario["dataInclusao"].stringValue
        item.grupoPeneira               = dicionario["grupoPeneira"].stringValue
        item.quantidadePedido           = dicionario["quantidadePedido"].stringValue
        item.id                         = dicionario["id"].stringValue
        item.saldo                      = dicionario["saldo"].stringValue
        item.descontoUnitario           = dicionario["descontoUnitario"].stringValue
        item.quantidadeEntregaFutura    = dicionario["quantidadeEntregaFutura"].stringValue
        item.usuarioInclusao            = dicionario["usuarioInclusao"].stringValue
        item.organizacaoId              = dicionario["organizacaoId"].stringValue
        item.percentualDesconto         = dicionario["percentualDesconto"].stringValue
        item.idImportado                = dicionario["idImportado"].stringValue
        item.valorTabela                = dicionario["valorTabela"].stringValue
        item.itemVenda                  = dicionario["itemVenda"].stringValue
        item.status                     = dicionario["status"].stringValue
        item.produto                    = atribuiProduto(dicionario["produto"])
        item.estado                     = dicionario["estado"].stringValue
        item.dataAlteracao              = dicionario["dataAlteracao"].stringValue
        item.ativo                      = dicionario["ativo"].stringValue
        item.usuarioAlteracao           = dicionario["usuarioAlteracao"].stringValue
        item.embalagem                  = dicionario["embalagem"].stringValue
        item.quantidadeFaturada         = dicionario["quantidadeFaturada"].stringValue
        item.pedido                     = dicionario["pedido"].stringValue
        item.valorMaiorTabela           = dicionario["valorMaiorTabela"].stringValue
        item.tratamento                 = dicionario["tratamento"].stringValue
        item.valorUnitario              = dicionario["valorUnitario"].stringValue
        
        DatabaseController.saveContext()
        return item
    }
    
    
    func atribuiProduto(_ dicionario: JSON) -> Produto {
        let produto = NSEntityDescription.insertNewObject(forEntityName: name.produto, into: DatabaseController.getContext()) as! Produto
        produto.id                 = dicionario["id"].stringValue
        produto.dataInclusao       = dicionario["dataInclusao"].stringValue
        produto.dataAlteracao      = dicionario["dataAlteracao"].stringValue
        produto.usuarioInclusao    = dicionario["usuarioInclusao"].stringValue
        produto.usuarioAlteracao   = dicionario["usuarioAlteracao"].stringValue
        produto.idImportado        = dicionario["idImportado"].stringValue
        produto.estado             = dicionario["estado"].stringValue
        produto.organizacaoId      = dicionario["organizacaoId"].stringValue
        produto.nome               = dicionario["nome"].stringValue
        produto.generico           = dicionario["generico"].stringValue
        produto.isGenerico         = dicionario["isGenerico"].boolValue
        produto.descricao          = dicionario["descricao"].stringValue
        produto.geracao            = dicionario["geracao"].stringValue
        DatabaseController.saveContext()
        return produto
    }
    
    func atribuiCliente(_ dicionario: JSON) -> Cliente {
        
        let cliente = NSEntityDescription.insertNewObject(forEntityName: name.cliente, into: DatabaseController.getContext()) as! Cliente
        cliente.vpm                = dicionario["vpm"].stringValue
        cliente.isProspecto        = dicionario["isProspecto"].stringValue
        cliente.dataInclusao       = dicionario["dataInclusao"].stringValue
        cliente.limiteCredito      = dicionario["limiteCredito"].stringValue
        cliente.observacao         = dicionario["observacao"].stringValue
        cliente.formaAceitas       = dicionario["formaAceitas"].stringValue
        cliente.descontoMaximo     = dicionario["descontoMaximo"].stringValue
        cliente.id                 = dicionario["id"].stringValue
        cliente.fazenda            = dicionario["fazenda"].stringValue
        cliente.areasGerenciais    = dicionario["areasGerenciais"].stringValue
        cliente.loja               = dicionario["loja"].stringValue
        cliente.tipo               = dicionario["tipo"].stringValue
        cliente.risco              = dicionario["risco"].stringValue
        cliente.associado          = dicionario["associado"].stringValue
        cliente.grupoCliente       = dicionario["grupoCliente"].stringValue
        cliente.usuarioInclusao    = dicionario["usuarioInclusao"].stringValue
        cliente.organizacaoId      = dicionario["organizacaoId"].stringValue
        cliente.vendedor           = dicionario["vendedor"].stringValue
        cliente.pessoa             = atribuiPessoa(dicionario["pessoa"])
        cliente.classificacao      = dicionario["classificacao"].stringValue
        cliente.idImportado        = dicionario["idImportado"].stringValue
        cliente.estado             = dicionario["estado"].stringValue
        cliente.grupoNegociacao    = dicionario["grupoNegociacao"].stringValue
        cliente.areaGerencial      = dicionario["areaGerencial"].stringValue
        cliente.ativo              = dicionario["ativo"].stringValue
        cliente.usuarioAlteracao   = dicionario["usuarioAlteracao"].stringValue
        cliente.dataAlteracao      = dicionario["dataAlteracao"].stringValue
        cliente.perfilCompra       = dicionario["perfilCompra"].stringValue
        cliente.canal              = dicionario["canal"].stringValue
        cliente.descontoFixo       = dicionario["descontoFixo"].stringValue
        DatabaseController.saveContext()
        return cliente
    }
    
    
    func atribuiPessoa(_ dicionario: JSON) -> Pessoa {
        
        let pessoa = NSEntityDescription.insertNewObject(forEntityName: name.pessoa, into: DatabaseController.getContext()) as! Pessoa
        
        pessoa.email              = dicionario["email"].stringValue
        pessoa.dataNascimento     = dicionario["dataNascimento"].stringValue
        pessoa.usuarioInclusao    = dicionario["usuarioInclusao"].stringValue
        pessoa.redesSociais       = dicionario["redesSociais"].stringValue
        pessoa.organizacaoId      = dicionario["organizacaoId"].stringValue
        pessoa.dataInclusao       = dicionario["dataInclusao"].stringValue
        pessoa.idImportado        = dicionario["idImportado"].stringValue
        pessoa.foneComercial      = dicionario["foneComercial"].stringValue
        pessoa.site               = dicionario["site"].stringValue
        pessoa.estado             = dicionario["estado"].stringValue
        pessoa.nomeFantasia       = dicionario["nomeFantasia"].stringValue
        pessoa.id                 = dicionario["id"].stringValue
        pessoa.dataAlteracao      = dicionario["dataAlteracao"].stringValue
        pessoa.codigoImportado    = dicionario["codigoImportado"].stringValue
        pessoa.ativo              = dicionario["ativo"].stringValue
        pessoa.sexo               = dicionario["sexo"].stringValue
        pessoa.usuarioAlteracao   = dicionario["usuarioAlteracao"].stringValue
        pessoa.enderecos          = dicionario["enderecos"].stringValue
        pessoa.tipoPessoa         = dicionario["tipoPessoa"].stringValue
        pessoa.foneCelular        = dicionario["foneCelular"].stringValue
        pessoa.nome               = dicionario["nome"].stringValue
        pessoa.nomeCompleto       = dicionario["nomeCompleto"].stringValue
        DatabaseController.saveContext()
        return pessoa
    }
    
}
