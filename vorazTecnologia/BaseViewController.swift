//
//  BaseViewController.swift
//  vorazTecnologia
//
//  Created by Lucas de Brito Reis on 20/07/17.
//  Copyright © 2017 Minerva Aplicativos LTDA ME. All rights reserved.
//

import UIKit
import CoreData
import Alamofire
import SwiftyJSON


class BaseViewController: UIViewController {
    
    // Variáveis de dados
    let name = Name()
    let tipoRequisicao = TipoRequisicao()
    let urlBase: String     = "https://api.agroshow.biz/api/"
    //let urlBase: String     = "https://hml2.api.agroshow.biz/api/"
    var listaPedidos: [String] = []
    var numeroPaginas = 0
    var statusPedidos: [String: String] = [:]
    
    
    
    var listaCampanhas: [String] = []
    var numeroPaginasCampanhas = 0
    var estadoCampanhas: [String: String] = [:]
    
    var estaEmpilhando: Bool = false
    

    
    /******************** Funções CoreData ********************/
    func lerBancoDeDados(_ entityName: String) -> [NSManagedObject] {
        
        var entidade = [NSManagedObject]()
        
        let request  = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        request.returnsObjectsAsFaults = false
        
        let sort = NSSortDescriptor(key: "id", ascending: false)
        request.sortDescriptors = [sort]
        
        do {
            entidade = try(DatabaseController.getContext().fetch(request)) as! [NSManagedObject]
        } catch let erro {
            print("erro ao carregar \(entityName): \(erro)")
        }
        
        return entidade
    }
    
    func lerPedidos() -> [Pedidos] {
        
        var entidade = [Pedidos]()
        
        let request  = NSFetchRequest<NSFetchRequestResult>(entityName: self.name.pedidos)
        request.returnsObjectsAsFaults = false
        
        let sort = NSSortDescriptor(key: "cd_id", ascending: false)
        request.sortDescriptors = [sort]
        
        do {
            entidade = try(DatabaseController.getContext().fetch(request)) as! [Pedidos]
        } catch let erro {
            print("erro ao carregar pedidos: \(erro)")
        }
        
        return entidade
    }
    
    func pedidofiltrado(pedidoId: String) -> Pedidos? {
        
        let requestPedidos  = NSFetchRequest<NSFetchRequestResult>(entityName: self.name.pedidos)
        requestPedidos.returnsObjectsAsFaults = false
        requestPedidos.predicate = NSPredicate(format: "id == %@", pedidoId)
        
        do {
            let pedidos = try(DatabaseController.getContext().fetch(requestPedidos)) as! [Pedidos]
            if pedidos.count > 0 {
                /* remover duplicatas */
                return pedidos[0]
            } else {
                return nil
            }
            
        } catch let erro {
            print("erro ao carregar \(self.name.pedidos): \(erro)")
        }
        
        return nil
    }
    
    func atualizaPedido(pedidoId: String, json: JSON) {
        let requestPedidos  = NSFetchRequest<NSFetchRequestResult>(entityName: self.name.pedidos)
        requestPedidos.returnsObjectsAsFaults = false
        requestPedidos.predicate = NSPredicate(format: "id == %@", pedidoId)
        
        do {
            let pedidos = try(DatabaseController.getContext().fetch(requestPedidos)) as! [Pedidos]
            //print("atualiza pedidos: \(pedidos.count)")
            
            if pedidos.count > 0 {
                
                if pedidos.count > 1 {
                    //apagar os pedidos até ter somente 1
                }
                
                //pedidos[0].id                     = json["id"].stringValue
                pedidos[0].valorFrete             = json["valorFrete"].stringValue
                pedidos[0].dataInclusao           = json["dataInclusao"].stringValue
                pedidos[0].listaPreco             = json["listaPreco"].stringValue
                pedidos[0].cliente                = atribuiCliente(json["cliente"])
                pedidos[0].enderecoFaturamento    = json["enderecoFaturamento"].stringValue
                pedidos[0].campanha               = atribuiCampanha(json["campanha"])
                pedidos[0].enderecoEntrega        = json["enderecoEntrega"].stringValue
                pedidos[0].dataEntrega            = json["dataEntrega"].stringValue
                pedidos[0].marcadores             = json["marcadores"].stringValue
                pedidos[0].valorFaturado          = json["valorFaturado"].stringValue
                pedidos[0].tipoFrete              = json["tipoFrete"].stringValue
                pedidos[0].dataPrevisaoRetorno    = json["dataPrevisaoRetorno"].stringValue
                pedidos[0].organizacaoId          = json["organizacaoId"].stringValue
                pedidos[0].impressoes             = json["impressoes"].stringValue
                pedidos[0].vendedor               = json["vendedor"].stringValue
                pedidos[0].dataPrimeiroVencimento = json["dataPrimeiroVencimento"].stringValue
                pedidos[0].numeroImportado        = json["numeroImportado"].stringValue
                pedidos[0].areaGerencial          = json["areaGerencial"].stringValue
                pedidos[0].ativo                  = json["ativo"].stringValue
                pedidos[0].dataAlteracao          = json["dataAlteracao"].stringValue
                pedidos[0].quantidadeParcelas     = json["quantidadeParcelas"].stringValue
                pedidos[0].observacaoBloqueio     = json["observacaoBloqueio"].stringValue
                pedidos[0].transportadora         = json["transportadora"].stringValue
                pedidos[0].revisoes               = json["revisoes"].stringValue
                pedidos[0].numero                 = json["numero"].stringValue
                pedidos[0].observacao             = json["observacao"].stringValue
                pedidos[0].leitoresRevisoes       = json["leitoresRevisoes"].stringValue
                pedidos[0].eventos                = json["eventos"].stringValue
                pedidos[0].dataUltimoVencimento   = json["dataUltimoVencimento"].stringValue
                pedidos[0].valorDesconto          = json["valorDesconto"].stringValue
                pedidos[0].tipo                   = json["tipo"].stringValue
                pedidos[0].motivoBloqueio         = json["motivoBloqueio"].stringValue
                pedidos[0].usuarioInclusaoPedido  = json["usuarioInclusaoPedido"].stringValue
                pedidos[0].usuarioInclusao        = json["usuarioInclusao"].stringValue
                pedidos[0].valorDescontoCliente   = json["valorDescontoCliente"].stringValue
                pedidos[0].idImportado            = json["idImportado"].stringValue
                pedidos[0].pedidoOrigem           = json["pedidoOrigem"].stringValue
                pedidos[0].dataPedido             = json["dataPedido"].stringValue
                pedidos[0].status                 = json["status"].stringValue
                pedidos[0].estado                 = json["estado"].stringValue
                pedidos[0].proximoAvaliador       = atribuiProximoAvaliador(json["proximoAvaliador"])
                pedidos[0].anexos                 = json["anexos"].stringValue
                pedidos[0].usuarioAlteracao       = json["usuarioAlteracao"].stringValue
                pedidos[0].formaPagamento         = json["formaPagamento"].stringValue
                pedidos[0].valorTotal             = json["valorTotal"].stringValue
                
                
                //print("status = \(pedidos[0].status!)")
                pedidos[0].cd_isSync                = true
                pedidos[0].cd_observacao            = ""
                pedidos[0].cd_motivoBloqueioId      = ""
                pedidos[0].cd_mensagemErro          = ""
                pedidos[0].cd_requisicaoDeuCerto    = true
                
                
                var vetorItens: [Itens] = []
                for (_, jsonItens) in json["itens"] {
                    let item = self.atribuiItens(jsonItens)
                    vetorItens.append(item)
                }
                pedidos[0].setValue(NSSet(array: vetorItens), forKey: "itens")
                

                DatabaseController.saveContext()

            }
        } catch let erro {
            print("erro ao carregar \(self.name.pedidos): \(erro)")
        }
        
    }
    
    
    func lerNotSync(isSync: Bool) -> [Pedidos] {
        let requestPedidos  = NSFetchRequest<NSFetchRequestResult>(entityName: self.name.pedidos)
        requestPedidos.returnsObjectsAsFaults = false
        requestPedidos.predicate = NSPredicate(format: "cd_isSync == %@", isSync as CVarArg)
        
        do {
            let pedidos = try(DatabaseController.getContext().fetch(requestPedidos)) as! [Pedidos]
            return pedidos
            
        } catch let erro {
            print("erro ao carregar \(self.name.pedidos): \(erro)")
        }
        return [Pedidos]()
    }
    
    // MARK: Delete Data Records
    
    func clearContext() {
        
        let requestDadosLogin = NSFetchRequest<NSFetchRequestResult>(entityName: self.name.dadosLogin)
        requestDadosLogin.returnsObjectsAsFaults = false
        
        let resultdadosLogin = try? DatabaseController.getContext().fetch(requestDadosLogin)
        let coreDadosLogin = resultdadosLogin as! [DadosLogin]
        
        
        for objectDadosLogin in coreDadosLogin {
            DatabaseController.getContext().delete(objectDadosLogin)
        }
        
        let requestPedidos = NSFetchRequest<NSFetchRequestResult>(entityName: self.name.pedidos)
        requestPedidos.returnsObjectsAsFaults = false
        
        let resultPedidos = try? DatabaseController.getContext().fetch(requestPedidos)
        let corePedidos = resultPedidos as! [Pedidos]
        
        
        for objectPedido in corePedidos {
            DatabaseController.getContext().delete(objectPedido)
        }
    
        DatabaseController.saveContext()
    }
    
    
    
    func retornaMensagem(_ json: JSON) -> String {
        
        var mensagem = ""
        
        if json["erros"]["mensagem"].string != nil {
            mensagem   = json["erros"]["mensagem"].stringValue
        }
        
        return mensagem
    }
    
    func falhaConexao(_ erro: Error, response: DataResponse<Any>) -> String {
        var message = erro.localizedDescription
        
        if let httpStatusCode = response.response?.statusCode {
            
            switch(httpStatusCode) {
            case 400:
                message = "Nome de usuário ou senha não fornecidos."
            case 401:
                message = "Usuário ou senha incorretos"
            case 1005:
                message = "A conexão foi perdida"
            default:
                message = erro.localizedDescription
            }
        } else {
            switch(erro.localizedDescription) {
            case "The Internet connection appears to be offline.":
                message = "A conexão parece estar offline. Verifique sua conexão com a internet."
            case "The request timed out.":
                message = "O pedido excedeu o tempo limite. Verifique sua conexão com a internet"
            default:
                message = erro.localizedDescription
            }
        }
        
        return message
    }

}
