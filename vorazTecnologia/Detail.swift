//
//  Detail.swift
//  agroshowiOS
//
//  Created by Lucas de Brito Reis on 15/06/17.
//  Copyright © 2017 Minerva Aplicativos LTDA ME. All rights reserved.
//

import UIKit
import SnapKit
import Floaty
import LTMorphingLabel
import Alamofire
import SwiftyJSON
import CoreData
import AIFlatSwitch
import NotificationBannerSwift
import SwiftOverlays

private let detailCellIdentifier = "detailCellIdentifier"

class Detail: BaseViewController, UITableViewDataSource, UITableViewDelegate {
    
    let backButton: UIButton = {
        let botao = UIButton(type: .custom)
        botao.setImage(UIImage(named: "Backicon"), for: .normal)
        botao.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        return botao
    }()
    
    let headerView  = UIView()
    let boxTitulo   = UIView()
    let boxStatus   = UIView()
    let boxCliente  = UIView()
    let boxHeader1  = UIView()
    let boxHeader2  = UIView()
    
    let circulo: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 13
        view.backgroundColor = UIColor.yellow
        view.clipsToBounds = true
        return view
    }()
    
    let flatSwitch: AIFlatSwitch = {
        let flat = AIFlatSwitch(frame: CGRect(x: 0, y: 0, width: 26, height: 26))
        flat.lineWidth = 2.0
        flat.strokeColor = UIColor.white
        flat.trailStrokeColor = UIColor.white
        flat.animatesOnTouch = true
        flat.isEnabled = false
        return flat
    }()
    
    let nomeCliente: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.center
        label.text = ""
        return label
    }()
    
    
    let nomeCampanha: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 18)
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.center
        label.text = ""
        return label
    }()
    
    let linhaHeader: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0.2, alpha: 1)
        return view
    }()
    
    var numeroLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.left
        label.text = "Número do Pedido"
        return label
    }()
    
    var numero: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.left
        label.text = ""
        return label
    }()
    
    let boxAvaliador: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(r: 148, g: 139, b: 213)
        //view.layer.cornerRadius = 5
        return view
    }()
    
    
    var proximoAvaliadorLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.left
        label.text = "Proximo Avaliador"
        return label
    }()
    
    var proximoAvaliador: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.left
        label.text = ""
        return label
    }()
    
    var dataDoPedidoLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.left
        label.text = "Data do Pedido: "
        return label
    }()
    
    var dataDoPedido: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.left
        label.text = ""
        return label
    }()
    
    var campanhaLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.left
        label.text = "Campanha"
        return label
    }()
    
    var campanha: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.left
        label.text = ""
        return label
    }()
    
    var tipoLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.left
        label.text = "Tipo"
        return label
    }()
    
    var tipo: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.left
        label.text = ""
        return label
    }()
    
    var vendedor: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 16)
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.left
        label.text = ""
        return label
    }()
    
    var dataDeVencimentoLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.left
        label.text = "Data de Vencimento"
        return label
    }()
    
    var dataDeVencimento: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.left
        label.text = ""
        return label
    }()
    
    var totalGeralLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.left
        label.textColor = UIColor(r: 128, g: 30, b: 72)//UIColor(r: 54, g: 142, b: 58)
        label.text = "Total"
        return label
    }()
    
    var totalGeral: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.center
        label.textColor = UIColor(r: 128, g: 30, b: 72)//UIColor(r: 54, g: 142, b: 58)
        label.text = ""
        return label
    }()
    
    let barratitulo = UIView()
    
    let valorUnitarioLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.left
        label.text = "Valor Unitário"
        return label
    }()
    
    let descontoLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.center
        label.textColor = UIColor.red
        label.text = "Desconto"
        return label
    }()
    
    let totalLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.center
        label.textColor = UIColor(r: 128, g: 30, b: 72)//UIColor(r: 54, g: 142, b: 58)
        label.text = "Total"
        return label
    }()
    
    let linha: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0.2, alpha: 1)
        return view
    }()
    
    let tituloView: UILabel = {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 100, height: 50))
        label.textColor = UIColor.white
        label.textAlignment = NSTextAlignment.center
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.text = "Pedido Detalhado"
        return label
    }()
    
    let tabela: UITableView = {
        let tableview = UITableView()
        tableview.register(DetailCell.self, forCellReuseIdentifier: detailCellIdentifier)
        return tableview
    }()
    
    //let floaty = Floaty()
    let floaty: Floaty = {
        let floaty = Floaty()
        floaty.buttonColor = UIColor(r: 128, g: 30, b: 72)//UIColor(r: 54, g: 142, b: 58)
        floaty.plusColor = UIColor.white
        return floaty
    }()
    
    // Variaveis de dados
    var pedidoId: String?
    var indexPathPedido: IndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabela.register(DetailCell.self, forCellReuseIdentifier: detailCellIdentifier)
        
        //self.lerBancoDeDados(context: getContext())
        //        if let id = pedidoId {
        //            pedido = self.pedidofiltrado(pedidoId: id)
        //        }
        
        // Configurando o delegate e data source da tableView
        tabela.dataSource = self
        tabela.delegate = self
        tabela.allowsSelection = false
        
        
        
        setupViews()
        setupPedido()
    }
    
    func setupPedido() {
        if let pedido = self.pedidofiltrado(pedidoId: pedidoId!) {
            
            let nulo = (pedido.proximoAvaliador?.nome == "") ? true : false
            
            circulo.backgroundColor = corStatus(pedido.status!)
            nomeCliente.text = pedido.cliente?.pessoa?.nome
            nomeCampanha.text = pedido.campanha?.nome
            campanha.text = pedido.campanha?.nome
            numero.text = pedido.numero
            proximoAvaliador.text = pedido.proximoAvaliador?.nome
            proximoAvaliadorLabel.text = nulo ? "" : proximoAvaliadorLabel.text
            dataDoPedido.text = pedido.dataPedido
            campanha.text = pedido.campanha?.nome
            tipo.text = pedido.tipo
            vendedor.text = pedido.vendedor
            dataDeVencimento.text = pedido.dataPrimeiroVencimento
            flatSwitch.isSelected = pedido.cd_isSync
            
            if let valorTotal = pedido.valorTotal {
                totalGeral.text = "R$ \((Double(valorTotal)?.arredonda())!)"
                //totalGeral.text = valorTotal
            }
            
            
            
            
            /*********** box avaliador ***************/
            
            headerView.snp.updateConstraints { (make) in
                make.top.left.width.equalToSuperview()
                make.height.equalTo(nulo ? 200 : 240)
            }
            
            
            
            boxAvaliador.snp.updateConstraints { (make) in
                make.top.equalTo(linhaHeader.snp.bottom)
                make.left.equalToSuperview()
                make.right.equalToSuperview()
                make.height.equalTo( nulo ? 0: 40)
            }
            
            proximoAvaliadorLabel.snp.updateConstraints { (make) in
                make.centerY.equalToSuperview()
                make.left.equalTo(nulo ? 0 : 20)
            }
            
            proximoAvaliador.snp.updateConstraints { (make) in
                make.centerY.equalToSuperview()
                make.left.equalTo(proximoAvaliadorLabel.snp.right).offset(10)
            }
            
            
            
            
            
            
            
        }
    }
    
    
    
    
    
    func setupViews() {
        
        //barra de navegacao
        if let barranavegacao = self.navigationController?.navigationBar {
            barranavegacao.isTranslucent = false
            barranavegacao.barTintColor = UIColor(r: 128, g: 30, b: 72)//UIColor(r: 54, g: 142, b: 58)
            barranavegacao.tintColor = UIColor.white
            self.navigationItem.titleView = tituloView
            let backItem = UIBarButtonItem(customView: backButton)
            self.navigationItem.setLeftBarButton(backItem, animated: true)
            backButton.addTarget(self, action: #selector(back), for: .touchUpInside)
        }
        view.backgroundColor = UIColor.white
        
        //Adiciona subviews
        view.addSubview(headerView)
        headerView.addSubview(boxTitulo)
        boxTitulo.addSubview(boxStatus)
        boxStatus.addSubview(circulo)
        circulo.addSubview(flatSwitch)
        boxTitulo.addSubview(nomeCliente)
        boxTitulo.addSubview(nomeCampanha)
        
        headerView.addSubview(linhaHeader)
        
        
        headerView.addSubview(boxAvaliador)
        boxAvaliador.addSubview(proximoAvaliadorLabel)
        boxAvaliador.addSubview(proximoAvaliador)
        
        
        headerView.addSubview(boxHeader1)
        boxHeader1.addSubview(numeroLabel)
        boxHeader1.addSubview(numero)
        boxHeader1.addSubview(vendedor)
        
        
        
        headerView.addSubview(boxHeader2)
        boxHeader2.addSubview(dataDoPedidoLabel)
        boxHeader2.addSubview(dataDoPedido)
        boxHeader2.addSubview(campanhaLabel)
        boxHeader2.addSubview(campanha)
        boxHeader2.addSubview(dataDeVencimentoLabel)
        boxHeader2.addSubview(dataDeVencimento)
        
        boxHeader2.addSubview(tipoLabel)
        boxHeader2.addSubview(tipo)
        boxHeader2.addSubview(totalGeral)
        
        
        view.addSubview(barratitulo)
        barratitulo.addSubview(valorUnitarioLabel)
        barratitulo.addSubview(descontoLabel)
        barratitulo.addSubview(totalLabel)
        view.addSubview(linha)
        view.addSubview(tabela)
        view.addSubview(floaty)
        
        /******************* headerview **************************/
        
        //decide se aparece a box proximoAvaliador
        
        let pedidos = self.pedidofiltrado(pedidoId: self.pedidoId!)
        let nulo = (pedidos?.proximoAvaliador?.nome == "") ? true : false
        
        headerView.snp.makeConstraints { (make) in
            make.top.left.width.equalToSuperview()
            make.height.equalTo(nulo ? 200 : 240)
        }
        
        boxTitulo.snp.makeConstraints { (make) in
            make.left.top.width.equalToSuperview()
            make.height.equalTo(39)
        }
        
        boxStatus.snp.makeConstraints { (make) in
            make.left.top.bottom.equalToSuperview()
            make.width.equalTo(70)
        }
        
        circulo.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.equalTo(20)
            make.right.equalTo(-24)
            make.height.width.equalTo(26)
        }
        
        nomeCliente.snp.makeConstraints { (make) in
            make.top.height.equalToSuperview()
            make.left.equalTo(boxStatus.snp.right)
        }
        
        nomeCampanha.snp.makeConstraints { (make) in
            make.top.right.height.equalToSuperview()
            make.left.equalTo(nomeCliente.snp.right)
        }
        
        
        
        linhaHeader.snp.makeConstraints { (make) in
            make.top.equalTo(boxTitulo.snp.bottom)
            make.left.width.equalToSuperview()
            make.height.equalTo(1)
        }
        
        /*********** box avaliador ***************/
        
        boxAvaliador.snp.makeConstraints { (make) in
            make.top.equalTo(linhaHeader.snp.bottom)
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalTo( nulo ? 0: 40)
        }
        
        proximoAvaliadorLabel.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.equalTo(nulo ? 0 : 20)
        }
        
        proximoAvaliador.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.equalTo(proximoAvaliadorLabel.snp.right).offset(10)
        }
        
        /************* boxHeader1 ************/
        
        boxHeader1.snp.makeConstraints { (make) in
            make.top.equalTo(boxAvaliador.snp.bottom)
            make.left.width.equalToSuperview()
            make.height.equalTo(60)
        }
        
        numeroLabel.snp.makeConstraints { (make) in
            make.top.equalTo(8)
            make.left.equalTo(20)
            make.width.equalToSuperview().multipliedBy(0.4)
        }
        
        numero.snp.makeConstraints { (make) in
            make.top.equalTo(numeroLabel.snp.bottom)
            make.left.equalTo(numeroLabel.snp.left)
            make.width.equalToSuperview().dividedBy(2)
        }
        
        vendedor.snp.makeConstraints { (make) in
            make.top.equalTo(8)
            make.left.equalTo(numeroLabel.snp.right).offset(10)
            make.right.equalToSuperview()
        }
        
        /************* boxHeader2 ****************/
        
        boxHeader2.snp.makeConstraints { (make) in
            make.top.equalTo(boxHeader1.snp.bottom)
            make.left.width.equalToSuperview()
            make.height.equalTo(80)
        }
        
        dataDoPedidoLabel.snp.makeConstraints { (make) in
            make.top.equalTo(8)
            make.left.equalTo(20)
        }
        
        dataDoPedido.snp.makeConstraints { (make) in
            make.top.equalTo(dataDoPedidoLabel.snp.bottom)
            make.left.equalTo(dataDoPedidoLabel.snp.left)
        }
        
        campanhaLabel.snp.makeConstraints { (make) in
            make.top.equalTo(dataDoPedido.snp.bottom).offset(8)
            make.left.equalTo(20)
        }
        
        campanha.snp.makeConstraints { (make) in
            make.top.equalTo(campanhaLabel.snp.bottom)
            make.left.equalTo(campanhaLabel.snp.left)
            make.bottom.equalTo(headerView.snp.bottom).offset(-8)
        }
        
        tipoLabel.snp.makeConstraints { (make) in
            make.top.equalTo(campanhaLabel.snp.top)
            make.left.equalTo(dataDeVencimentoLabel.snp.left)
            
        }
        
        tipo.snp.makeConstraints { (make) in
            make.top.equalTo(tipoLabel.snp.bottom)
            make.left.equalTo(tipoLabel.snp.left)
            make.bottom.equalTo(headerView.snp.bottom).offset(-8)
        }
        
        dataDeVencimentoLabel.snp.makeConstraints { (make) in
            make.top.equalTo(8)
            make.left.equalTo(vendedor.snp.left)
        }
        
        dataDeVencimento.snp.makeConstraints { (make) in
            make.top.equalTo(dataDeVencimentoLabel.snp.bottom)
            make.left.equalTo(dataDeVencimentoLabel.snp.left)
        }
        
        totalGeral.snp.makeConstraints { (make) in
            make.centerY.equalTo(tipoLabel.snp.bottom)
            make.left.equalTo(tipo.snp.right)
            make.right.equalToSuperview()
        }
        
        
        /************** barra titulo ***************/
        
        barratitulo.snp.makeConstraints { (make) in
            make.top.equalTo(headerView.snp.bottom)
            make.left.equalToSuperview()
            make.width.equalToSuperview()
            make.height.equalTo(28)
        }
        
        valorUnitarioLabel.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(8)
            make.width.equalTo(100)
            make.height.lessThanOrEqualToSuperview()
        }
        
        totalLabel.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.right.equalToSuperview().offset(8)
            make.width.equalTo(100)
            make.height.lessThanOrEqualToSuperview()
        }
        
        descontoLabel.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.equalTo(valorUnitarioLabel.snp.right)
            make.right.equalTo(totalLabel.snp.left)
            make.height.lessThanOrEqualToSuperview()
        }
        
        linha.snp.makeConstraints { (make) in
            make.top.equalTo(barratitulo.snp.bottom)
            make.left.width.equalToSuperview()
            make.height.equalTo(1)
        }
        
        
        
        tabela.snp.makeConstraints { (make) in
            make.top.equalTo(linha.snp.bottom)
            make.left.bottom.width.equalToSuperview()
            
            let dadosLogin = lerBancoDeDados(self.name.dadosLogin) as! [DadosLogin]
            
            if dadosLogin.count > 0 {
                if let authToken = dadosLogin[0].authToken {
                    
                    floaty.addItem(title: "bloquear") { (item) in
                        
                        
                        let descricao = ""
                        
                        self.preencherObservacao("Bloquear", descricao: descricao, placeHolder: "observação") { (observacao) in
                            
                            self.motivoBloqueio({ (motivoBloqueioId) in
                                
                                
                                self.bloquear(self.urlBase, id: self.pedidoId!, indexPath: self.indexPathPedido!, authToken: authToken, observacao: observacao, motivoBloqueioId: motivoBloqueioId, {_ in
                                    
                                    self.notificacao(self.tipoRequisicao.bloquear, pedidoId: self.pedidoId!)
                                })
                                
                                
                            })}
                        
                        
                        
                        
                        }.buttonColor = UIColor(r: 178, g:   0, b: 0)
                    
                    floaty.addItem(title: "Pedir Revisão") { (item) in
                        let descricao = ""
                        self.preencherObservacao("Enviar para Revisão", descricao: descricao, placeHolder: "observação") { (observacao) in
                            
                            self.pedirRevisao(self.urlBase, id: self.pedidoId!, indexPath: self.indexPathPedido!, authToken: authToken, observacao: observacao, {_ in
                                
                                self.notificacao(self.tipoRequisicao.pedirRevisao, pedidoId: self.pedidoId!)
                            })
                            
                            
                        }
                        }.buttonColor = UIColor(r: 178, g: 87, b: 9)//UIColor(r: 255, g: 215, b: 0)
                    
                    floaty.addItem(title: "Liberar") { (item) in
                        
                        
                        let descricao = ""
                        self.preencherObservacao("Liberar", descricao: descricao, placeHolder: "justificativa") { (observacao) in
                            
                            self.liberar(self.urlBase, id: self.pedidoId!, indexPath: self.indexPathPedido!, authToken: authToken, observacao: observacao, {_ in
                                
                                self.notificacao(self.tipoRequisicao.liberar, pedidoId: self.pedidoId!)
                            })
                            
                            
                        }
                        
                        }.buttonColor = UIColor(r: 54, g: 142, b: 58)
                }
            }
            
        }
    }
    
    
    func notificacao(_ tipoRequisicao: String, pedidoId: String) {
        
        
        self.removeAllOverlays()
        SwiftOverlays.removeAllBlockingOverlays()
        
        if let pedido = self.pedidofiltrado(pedidoId: pedidoId) {
            if pedido.cd_isSync {
                
                if pedido.cd_requisicaoDeuCerto {
                    
                    var title: String
                    var subtitle: String
                    
                    switch tipoRequisicao {
                    case self.tipoRequisicao.liberar:
                        title  = "Liberar"
                        subtitle = "Pedido Liberado"
                    case self.tipoRequisicao.pedirRevisao:
                        title  = "Enviar para revisão"
                        subtitle = "Pedido enviado para revisão"
                    case self.tipoRequisicao.bloquear:
                        title  = "Bloquear"
                        subtitle = "Pedido bloqueado"
                    default:
                        title  = ""
                        subtitle = ""
                    }
                    
                    let banner = NotificationBanner(title: title, subtitle: subtitle, style: .success)
                    banner.show()
                    
                }
            }
            self.setupPedido()
            
        }
        
        
    }
    
    func back() {
        DatabaseController.saveContext()
        self.dismiss(animated: true)
        
    }
    
    override func didMove(toParentViewController parent: UIViewController?) {
        
        if let feed = self.navigationController?.parent as? Feed {
            feed.tabela.reloadData()
            
        }
    }
    
    
    // MARK: - Table view data source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let pedidos = self.pedidofiltrado(pedidoId: self.pedidoId!)
        let itens = pedidos?.itens?.allObjects
        return (itens?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: detailCellIdentifier, for: indexPath) as! DetailCell
        
        //condig the cell
        
        if let pedidos = self.pedidofiltrado(pedidoId: self.pedidoId!) {
            cell.config(pedido: pedidos, indexPath: indexPath)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200 // also UITableViewAutomaticDimension can be used
    }
    
    func tableView(_ tableView: UITableView, canFocusRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    
}
