//
//  MenuCell.swift
//  agroshowiOS
//
//  Created by Lucas de Brito Reis on 19/06/17.
//  Copyright © 2017 Minerva Aplicativos LTDA ME. All rights reserved.
//

import UIKit
import SnapKit

class MenuCell: UITableViewCell {
    
    
    let boxIcone: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }()
    
    let icone: UIImageView = {
        let imageView = UIImageView()
        return imageView
    }()
    
    let texto: UILabel = {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 100, height: 50))
        label.textColor = UIColor.black
        label.textAlignment = NSTextAlignment.center
        label.font = UIFont.boldSystemFont(ofSize: 16)
        return label
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        
        // Adiciona subviews
        self.addSubview(boxIcone)
        boxIcone.addSubview(icone)
        self.addSubview(texto)

        boxIcone.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.equalToSuperview()
            make.height.equalTo(50)
            make.width.equalTo(50)
        }
        
        icone.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(20)
            make.centerY.equalToSuperview()
            make.height.width.equalTo(30)
        }
        
        texto.snp.makeConstraints { (make) in
            make.left.equalTo(icone.snp.right).offset(15)
            make.centerY.equalToSuperview()
        }
    }
    
    
    //Configura o icone e o texto da celula
    func config(imageName: [String], labelName: [String], indexPath: IndexPath) {
        icone.image = UIImage(named: imageName[indexPath.row])
        texto.text = labelName[indexPath.row]
    }
    
}
