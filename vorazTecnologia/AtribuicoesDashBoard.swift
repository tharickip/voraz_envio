//
//  AtribuicoesDashBoard.swift
//  vorazTecnologia
//
//  Created by Lucas de Brito Reis on 16/08/17.
//  Copyright © 2017 Minerva Aplicativos LTDA ME. All rights reserved.
//

import UIKit
import CoreData
import SwiftyJSON


extension BaseViewController {
    
    func atribuiCampanhaVendas(_ dicionario: JSON) {
        
        let campanhasVendas = NSEntityDescription.insertNewObject(forEntityName: name.campanhaVendas, into: DatabaseController.getContext()) as! CampanhaVendas
    
        campanhasVendas.id                  = dicionario["id"].stringValue
        campanhasVendas.dataInclusao        = dicionario["dataInclusao"].stringValue
        campanhasVendas.dataAlteracao       = dicionario["dataAlteracao"].stringValue
        campanhasVendas.usuarioInclusao     = dicionario["usuarioInclusao"].stringValue
        campanhasVendas.usuarioAlteracao    = dicionario["usuarioAlteracao"].stringValue
        campanhasVendas.idImportado         = dicionario["idImportado"].stringValue
        campanhasVendas.idImportado         = dicionario["idImportado"].stringValue
        campanhasVendas.estado              = dicionario["estado"].stringValue
        campanhasVendas.organizacaoId       = dicionario["organizacaoId"].stringValue
        campanhasVendas.nome                = dicionario["nome"].stringValue
        campanhasVendas.inicio              = dicionario["inicio"].stringValue
        campanhasVendas.fim                 = dicionario["fim"].stringValue
        campanhasVendas.ativo               = dicionario["ativo"].boolValue
        
        
        campanhasVendas.cd_vendasContadoresOK   = false
        campanhasVendas.cd_entregasEvolucaoOK   = false
        campanhasVendas.cd_vendasForecastOK     = false
        campanhasVendas.cd_vendasEspecieOK      = false
        
        DatabaseController.saveContext()
    }
    
    
    func atribuiVendasContadores(_ dicionario: JSON)->VendasContadores {
        
        let vendasContadores = NSEntityDescription.insertNewObject(forEntityName: name.VendasContadores, into: DatabaseController.getContext()) as! VendasContadores
        
        vendasContadores.valorCarteira  = dicionario["valorCarteira"].doubleValue
        vendasContadores.valorEntregue  = dicionario["valorEntregue"].doubleValue
        vendasContadores.valorVenda     = dicionario["valorVenda"].doubleValue
        
        DatabaseController.saveContext()
        return vendasContadores
    }
    
    
    func atribuiVendasEspecies(_ dicionario: JSON)->VendasEspecies {
        
        let vendasEspecies = NSEntityDescription.insertNewObject(forEntityName: name.VendasEspecies, into: DatabaseController.getContext()) as! VendasEspecies
        
        vendasEspecies.nome    = dicionario["nome"].stringValue
        vendasEspecies.valor   = dicionario["valor"].doubleValue
        vendasEspecies.volume  = dicionario["volume"].doubleValue
        
        DatabaseController.saveContext()
        return vendasEspecies
    }
    
    
    func atribuiEntregasEvolucao(_ dicionario: JSON)->EntregasEvolucao {
        
        let entregasEvolucao = NSEntityDescription.insertNewObject(forEntityName: name.EntregasEvolucao, into: DatabaseController.getContext()) as! EntregasEvolucao
        
        entregasEvolucao.nome             = dicionario["nome"].stringValue
        entregasEvolucao.volumeEntregue   = dicionario["volumeEntregue"].doubleValue
        entregasEvolucao.volumeCarteira   = dicionario["volumeCarteira"].doubleValue
        
        DatabaseController.saveContext()
        return entregasEvolucao
    }
    
    func atribuiVendasForecast(_ dicionario: JSON)->VendasForecast {
        
        let vendasForecast = NSEntityDescription.insertNewObject(forEntityName: name.VendasForecast, into: DatabaseController.getContext()) as! VendasForecast
        
        vendasForecast.nome                 = dicionario["nome"].stringValue
        vendasForecast.volumeVendaRealizado = dicionario["volumeVendaRealizado"].doubleValue
        vendasForecast.volumeVendaARealizar = dicionario["volumeVendaARealizar"].doubleValue
        
        DatabaseController.saveContext()
        return vendasForecast
    }
    
    func atribuiComparaCampanhas(_ dicionario: JSON) {
        
        let comparaCampanhas = NSEntityDescription.insertNewObject(forEntityName: name.ComparaCampanhas, into: DatabaseController.getContext()) as! ComparaCampanhas
        
        comparaCampanhas.nome                   = dicionario["nome"].stringValue
        comparaCampanhas.volume                 = dicionario["volume"].doubleValue
        
        DatabaseController.saveContext()
    }
    
    func lerVendasContadores() -> [CampanhaVendas] {
        
        var entidade = [CampanhaVendas]()
        
        let request  = NSFetchRequest<NSFetchRequestResult>(entityName: self.name.campanhaVendas)
        request.returnsObjectsAsFaults = false
        
        request.predicate = NSPredicate(format: "\(self.name.cd_vendasContadoresOK) == %@", true as CVarArg)
        
        do {
            entidade = try(DatabaseController.getContext().fetch(request)) as! [CampanhaVendas]
        } catch let erro {
            print("erro ao carregar \(self.name.campanhaVendas): \(erro)")
        }
        
        return entidade
    }
    
    
    func lerEntregasEvolucao() -> [CampanhaVendas] {
        
        var entidade = [CampanhaVendas]()
        
        let request  = NSFetchRequest<NSFetchRequestResult>(entityName: self.name.campanhaVendas)
        request.returnsObjectsAsFaults = false
        
        request.predicate = NSPredicate(format: "\(self.name.cd_entregasEvolucaoOK) == %@", true as CVarArg)
                
        do {
            entidade = try(DatabaseController.getContext().fetch(request)) as! [CampanhaVendas]
        } catch let erro {
            print("erro ao carregar \(self.name.campanhaVendas): \(erro)")
        }
        
        return entidade
    }
    
    func lerVendasForecast() -> [CampanhaVendas] {
        
        var entidade = [CampanhaVendas]()
        
        let request  = NSFetchRequest<NSFetchRequestResult>(entityName: self.name.campanhaVendas)
        request.returnsObjectsAsFaults = false
        
        request.predicate = NSPredicate(format: "\(self.name.cd_vendasForecastOK) == %@", true as CVarArg)
        
        do {
            entidade = try(DatabaseController.getContext().fetch(request)) as! [CampanhaVendas]
        } catch let erro {
            print("erro ao carregar \(self.name.campanhaVendas): \(erro)")
        }
        
        return entidade
    }
    
    func lerVendasEspecies() -> [CampanhaVendas] {
        
        var entidade = [CampanhaVendas]()
        
        let request  = NSFetchRequest<NSFetchRequestResult>(entityName: self.name.campanhaVendas)
        request.returnsObjectsAsFaults = false
        
        request.predicate = NSPredicate(format: "\(self.name.cd_vendasEspecieOK) == %@", true as CVarArg)
        
        do {
            entidade = try(DatabaseController.getContext().fetch(request)) as! [CampanhaVendas]
        } catch let erro {
            print("erro ao carregar \(self.name.campanhaVendas): \(erro)")
        }
        
        return entidade
    }

}
